/***********************************************************
  pkg_socket.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#endif

#include "../luka.h"

/** socket **/
static voidp luka_socket_socket (Luka *luka, voidp *p, size_t n) {
    int sock = 0;

    if (n < 3 || !luka_is_int(p[0]) || !luka_is_int(p[1]) || !luka_is_int(p[2]))
        return luka_null(luka);

    sock = socket(
        (int)luka_get_int(luka, p[0]), 
        (int)luka_get_int(luka, p[1]), 
        (int)luka_get_int(luka, p[2])
    );
    return luka_put_int(luka, sock);
}

/** close **/
static voidp luka_socket_close (Luka *luka, voidp *p, size_t n) {
    int sock = 0;

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    sock = (int)luka_get_int(luka, p[0]);
    if (sock == 0)
        return luka_null(luka);

#ifdef _WIN32
    closesocket(sock);
#else
    close(sock);
#endif
    return luka_null(luka);
}

/** htonl **/
static voidp luka_socket_htonl (Luka *luka, voidp *p, size_t n) {
    u_long val = 0;

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    val = (u_long)luka_get_int(luka, p[0]);
    val = htonl(val);
    return luka_put_int(luka, (long)val);
}

/** ntohl **/
static voidp luka_socket_ntohl (Luka *luka, voidp *p, size_t n) {
    u_long val = 0;

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    val = (u_long)luka_get_int(luka, p[0]);
    val = ntohl(val);
    return luka_put_int(luka, (long)val);
}

/** htons **/
static voidp luka_socket_htons (Luka *luka, voidp *p, size_t n) {
    u_short val = 0;

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    val = (u_short)luka_get_int(luka, p[0]);
    val = htons(val);
    return luka_put_int(luka, (long)val);
}

/** ntohs **/
static voidp luka_socket_ntohs (Luka *luka, voidp *p, size_t n) {
    u_short val = 0;

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    val = (u_short)luka_get_int(luka, p[0]);
    val = ntohs(val);
    return luka_put_int(luka, (long)val);
}

/** inet_addr **/
static voidp luka_socket_inet_addr (Luka *luka, voidp *p, size_t n) {
    const char *strptr = NULL;

    if (n < 1 || !luka_is_string(p[0]))
        return luka_null(luka);

    strptr = luka_get_string(luka, p[0]);
    return luka_put_int(luka, (long)inet_addr(strptr));
}

/** connect **/
static voidp luka_socket_connect (Luka *luka, voidp *p, size_t n) {
    int sock = 0;
    LukaObject *addr = NULL;

    voidp bufp = NULL;
    struct sockaddr_in paddr;

    if (n < 2 || !luka_is_int(p[0]) || !luka_is_object(p[1]))
        return luka_null(luka);

    sock = luka_get_int(luka, p[0]);
    addr = luka_get_object(luka, p[1]);

    if ((bufp = luka_object_get(luka, addr, "sin_family")) != NULL && luka_is_int(bufp)) {
        paddr.sin_family = luka_get_int(luka, bufp);
    }

    if ((bufp = luka_object_get(luka, addr, "sin_port")) != NULL && luka_is_int(bufp)) {
        paddr.sin_port = (u_short)luka_get_int(luka, bufp);
    }

    if ((bufp = luka_object_get(luka, addr, "sin_addr")) != NULL && luka_is_object(bufp)) {
        LukaObject *sin_addr = luka_get_object(luka, bufp);
        if ((bufp = luka_object_get(luka, sin_addr, "s_addr")) != NULL && luka_is_int(bufp)) {
            paddr.sin_addr.s_addr = luka_get_int(luka, bufp);
        }
    }

    return luka_put_int(luka, connect(sock, (struct sockaddr *)&paddr, sizeof(struct sockaddr)));
}

/** send **/
static voidp luka_socket_send (Luka *luka, voidp *p, size_t n) {
    int sock = 0, len = 0, flag = 0;
    const char *buffer = NULL;

    if (n < 4 || !luka_is_int(p[0]) || (!luka_is_string(p[1]) && !luka_is_byte(p[1])) || !luka_is_int(p[2]) || !luka_is_int(p[3]))
        return luka_null(luka);

    sock = (int)luka_get_int(luka, p[0]);
    len = (int)luka_get_int(luka, p[2]);
    flag = (int)luka_get_int(luka, p[3]);
    if (luka_is_string(p[1])) {
        buffer = luka_get_string(luka, p[1]);
    } else {
        buffer = (const char *)luka_get_byte(luka, p[1], NULL);
    }
    return luka_put_int(luka, send(sock, buffer, len, flag));
}

/** recv **/
static voidp luka_socket_recv (Luka *luka, voidp *p, size_t n) {
    int sock = 0, len = 0, flag = 0;
    unsigned char *data = NULL;

    if (n < 3 || !luka_is_int(p[0]) || !luka_is_int(p[1]) || !luka_is_int(p[2]))
        return luka_null(luka);

    sock = (int)luka_get_int(luka, p[0]);
    len = (int)luka_get_int(luka, p[1]);
    flag = (int)luka_get_int(luka, p[2]);
    data = (unsigned char *)luka_malloc(luka, len);
    if ((len = recv(sock, data, len, flag)) <= 0) {
        luka_free(luka, data);
        return luka_null(luka);
    }
    return luka_put_byte(luka, data, len);
}

/** bind **/
static voidp luka_socket_bind (Luka *luka, voidp *p, size_t n) {
    int sock = 0;
    LukaObject *addr = NULL;

    voidp bufp = NULL;
    struct sockaddr_in paddr;

    if (n < 2 || !luka_is_int(p[0]) || !luka_is_object(p[1]))
        return luka_null(luka);

    sock = luka_get_int(luka, p[0]);
    addr = luka_get_object(luka, p[1]);

    if ((bufp = luka_object_get(luka, addr, "sin_family")) != NULL && luka_is_int(bufp)) {
        paddr.sin_family = luka_get_int(luka, bufp);
    }

    if ((bufp = luka_object_get(luka, addr, "sin_port")) != NULL && luka_is_int(bufp)) {
        paddr.sin_port = (u_short)luka_get_int(luka, bufp);
    }

    if ((bufp = luka_object_get(luka, addr, "sin_addr")) != NULL && luka_is_object(bufp)) {
        LukaObject *sin_addr = luka_get_object(luka, bufp);
        if ((bufp = luka_object_get(luka, sin_addr, "s_addr")) != NULL && luka_is_int(bufp)) {
            paddr.sin_addr.s_addr = luka_get_int(luka, bufp);
        }
    }

    return luka_put_int(luka, bind(sock, (struct sockaddr *)&paddr, sizeof(struct sockaddr_in)));
}

/** listen **/
static voidp luka_socket_listen (Luka *luka, voidp *p, size_t n) {
    int sock = 0, num = 0;

    if (n < 2 || !luka_is_int(p[0]) || !luka_is_int(p[1]))
        return luka_null(luka);

    sock = luka_get_int(luka, p[0]);
    num = luka_get_int(luka, p[1]);
    return luka_put_int(luka, listen(sock, num));
}

/** accept **/
static voidp luka_socket_accept (Luka *luka, voidp *p, size_t n) {
    int sock = 0, struct_len = sizeof(struct sockaddr_in);
    struct sockaddr_in addr = {0};

    if (n < 1 || !luka_is_int(p[0]))
        return luka_null(luka);

    sock = luka_get_int(luka, p[0]);
    return luka_put_int(luka, accept(sock, (struct sockaddr *)&addr, &struct_len));
}

/***********************************************************
  PackageSocket(SOCKET����) 
***********************************************************/

void luka_socket_regs (Luka *luka) {
#ifdef _WIN32
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 0), &wsaData);
#endif

    luka_reg(luka, "socket",    luka_socket_socket);
    luka_reg(luka, "close",     luka_socket_close);
    luka_reg(luka, "htonl",     luka_socket_htonl);
    luka_reg(luka, "ntohl",     luka_socket_ntohl);
    luka_reg(luka, "htons",     luka_socket_htons);
    luka_reg(luka, "ntohs",     luka_socket_ntohs);
    luka_reg(luka, "inet_addr", luka_socket_inet_addr);
    luka_reg(luka, "connect",   luka_socket_connect);
    luka_reg(luka, "send",      luka_socket_send);
    luka_reg(luka, "recv",      luka_socket_recv);
    luka_reg(luka, "bind",      luka_socket_bind);
    luka_reg(luka, "listen",    luka_socket_listen);
    luka_reg(luka, "accept",    luka_socket_accept);
}

#define SREG(name,value) \
    luka_reg_var(luka, name,     luka_put_int(luka, value));

void luka_socket_vars (Luka *luka) {
    SREG("IPPROTO_IP",       0);
    SREG("IPPROTO_ICMP",     1);
    SREG("IPPROTO_IGMP",     2);
    SREG("IPPROTO_GGP",      3);
    SREG("IPPROTO_TCP",      6);
    SREG("IPPROTO_PUP",      12);
    SREG("IPPROTO_UDP",      17);
    SREG("IPPROTO_IDP",      22);
    SREG("IPPROTO_ND",       77);
    SREG("IPPROTO_RAW",      255);
    SREG("IPPROTO_MAX",      256);

    SREG("IPPROTO_HOPOPTS",  0);
    SREG("IPPROTO_IPV6",     41);
    SREG("IPPROTO_ROUTING",  43);
    SREG("IPPROTO_FRAGMENT", 44);
    SREG("IPPROTO_ESP",      50);
    SREG("IPPROTO_AH",       51);
    SREG("IPPROTO_ICMPV6",   58);
    SREG("IPPROTO_NONE",     59);
    SREG("IPPROTO_DSTOPTS",  60);

    SREG("AF_UNIX",          1);
    SREG("AF_INET",          2);
    SREG("AF_INET6",         23);

    SREG("SOCK_STREAM",      1);
    SREG("SOCK_DGRAM",       2);
    SREG("SOCK_RAW",         3);
    SREG("SOCK_SEQPACKET",   5);

    SREG("INADDR_ANY",       0);
}

