/***********************************************************
  pkg_windows.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <windows.h>
#include <windowsx.h>
#endif

#include "../luka.h"

#ifdef _WIN32

/** SetConsoleTitle **/
static voidp luka_windows_SetConsoleTitle (Luka *luka, voidp *p, size_t n) {
	const char *title = NULL;

    if (n < 1 || !luka_is_string(p[0]))
        return luka_null(luka);

	title = luka_get_string(luka, p[0]);
	SetConsoleTitle(title);
    return luka_true(luka);
}

#endif

/***********************************************************
  PackageWindows  
***********************************************************/

void luka_windows_regs (Luka *luka) {
#ifdef _WIN32
    luka_reg(luka, "SetConsoleTitle", luka_windows_SetConsoleTitle);
#endif
}

void luka_windows_vars (Luka *luka) {
#ifdef _WIN32
#endif
}

