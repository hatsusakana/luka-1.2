// +------------------------------------------------
// | csock.h 
// | hatsusakana@gmail.com 
// +------------------------------------------------

#ifndef C_SOCK_H
#define C_SOCK_H

//#define USE_OPENSSL      //使用OpenSSL库，不使用就注释掉

#ifndef SN
#define SN(x)  (!x || strlen(x) == 0)
#endif

#ifndef FREE
#define FREE(x)  if (x) { free(x); x = NULL; } 
#endif

#ifdef __cplusplus
extern "C" {
#endif
    
extern void cSOCK_Init ();

/** 域名转IP **/
extern int cSOCK_GetHostByName (const char *doname, char *s, size_t n);

/** 手动优化dns解析速度(从文件读取) **/
/** 例: www.baidu.com 61.135.169.121 **/
extern int cSOCK_DNS (const char *dnspath, const char *doname, char *s, size_t n);

/** tcp客户端(设置超时时间) **/
extern int cSOCK_Connect (const char *ip, int port, int timeout);

/** 设置发送接受超时 **/
extern void cSOCK_SetTimeout (int s, int sec);

/** 发送 **/
extern size_t cSOCK_Send (int s, const char *data, size_t size);

/** 发送文本 **/
extern size_t cSOCK_SendTXT (int s, const char *data);

/** 接收 **/
extern size_t cSOCK_Recv (int s, char *data, size_t size);

/** 读取一行 **/
extern int cSOCK_ReadLine (int s, char *line, size_t size);

/** 关闭套接字 **/
extern void cSOCK_Close (int s);

/** 关闭套接字(立刻) **/
extern void cSOCK_CloseNow (int s);

// +-------------------------------------------------------
// | USE OpenSSL 
// +-------------------------------------------------------

#ifdef USE_OPENSSL

typedef struct SSLSocket SSLSocket;

/** OpenSSL握手 **/
extern SSLSocket *cSOCK_SSL_Connect (int s);

/** OpenSSL发送 **/
extern size_t cSOCK_SSL_Send (SSLSocket *ssls, const char *data, size_t size);

/** OpenSSL接收 */
extern size_t cSOCK_SSL_Recv (SSLSocket *ssls, char *data, size_t size);

/** OpenSSL读取一行 **/
extern int cSOCK_SSL_ReadLine (SSLSocket *ssls, char *line, size_t size);

/** OpenSSL关闭 **/
extern void cSOCK_SSL_Close (SSLSocket **ssls);

#endif

/** 创建一个TCP服务器 **/
int csockserver (int port, void *p, void (*cb)(void *, int));
    
#ifdef __cplusplus
}
#endif

#endif
