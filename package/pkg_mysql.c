/***********************************************************
  pkg_mysql.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <winsock2.h>
#else
#endif

#include <mysql.h>

#include "../luka.h"

/** mysql_connect **/
static voidp luka_mysql_mysql_connect (Luka *luka, voidp *p, size_t n) {
    const char *host = NULL, *user = NULL, *pwd = NULL, *db = NULL;
    int port = 0;
    MYSQL *mysql = NULL;

    if (n < 5 || !luka_is_string(p[0]) || !luka_is_string(p[1]) || !luka_is_string(p[2]) || !luka_is_int(p[3]) || !luka_is_string(p[4]))
        return luka_null(luka);

    host = luka_get_string(luka, p[0]);
    user = luka_get_string(luka, p[1]);
    pwd = luka_get_string(luka, p[2]);
    port = luka_get_int(luka, p[3]);
    db = luka_get_string(luka, p[4]);

    if (*host == 0 || *user == 0 || *pwd == 0 || port < 0 || *db == 0)
        return luka_null(luka);

    mysql = (MYSQL *)luka_malloc(luka, sizeof(MYSQL));
    mysql_init(mysql);
    if (!mysql_real_connect(mysql, host, user, pwd, db, port, NULL, 0)) {
        luka_free(luka, mysql);
        return luka_null(luka);
    }

    return luka_put_voidp(luka, mysql);
}

/** mysql_close **/
static voidp luka_mysql_mysql_close (Luka *luka, voidp *p, size_t n) {
    MYSQL *mysql = NULL;

    if (n < 1 || !luka_is_voidp(p[0]))
        return luka_null(luka);

    mysql = (MYSQL *)luka_get_voidp(luka, p[0]);
    if (mysql) {
        mysql_close(mysql);
        luka_free(luka, mysql);
    }
    return luka_true(luka);
}

/** mysql_query **/
static voidp luka_mysql_mysql_query (Luka *luka, voidp *p, size_t n) {
    MYSQL *mysql = NULL;
    const char *sql = NULL;
    int ret = 0;

    if (n < 2 || !luka_is_voidp(p[0]) || !luka_is_string(p[1]))
        return luka_null(luka);

    mysql = (MYSQL *)luka_get_voidp(luka, p[0]);
    sql = luka_get_string(luka, p[1]);
    if (!mysql || *sql == 0)
        return luka_null(luka);

    ret = mysql_query(mysql, sql);
    return luka_put_int(luka, ret);
}

/** mysql_set_character_set **/
static voidp luka_mysql_mysql_set_character_set (Luka *luka, voidp *p, size_t n) {
    MYSQL *mysql = NULL;
    const char *code = NULL;
    int ret = 0;

    if (n < 2 || !luka_is_voidp(p[0]) || !luka_is_string(p[1]))
        return luka_null(luka);

    mysql = (MYSQL *)luka_get_voidp(luka, p[0]);
    code = luka_get_string(luka, p[1]);
    if (!mysql || *code == 0)
        return luka_null(luka);

    ret = mysql_set_character_set(mysql, code);
    return luka_put_int(luka, ret);
}

/** mysql_select **/
static voidp luka_mysql_mysql_select (Luka *luka, voidp *p, size_t n) {
    MYSQL *mysql = NULL;
    const char *sql = NULL;
    int ret = 0;

    int index = 0;
    LukaArray *fields = NULL;
    LukaArray *fieldsType = NULL;

    MYSQL_RES   *result = NULL;
    MYSQL_FIELD *field = NULL, *field_mov = NULL;
    MYSQL_ROW    row = NULL;

    voidp arrayP = luka_null(luka), objectP = luka_null(luka);
    LukaArray *array = NULL;
    LukaObject *object = NULL;

    if (n < 2 || !luka_is_voidp(p[0]) || !luka_is_string(p[1]))
        return luka_null(luka);

    mysql = (MYSQL *)luka_get_voidp(luka, p[0]);
    sql = luka_get_string(luka, p[1]);
    if (!mysql || *sql == 0)
        return luka_null(luka);

    ret = mysql_query(mysql, sql);
    if (ret != 0)
        return luka_null(luka);

    result = mysql_store_result(mysql);
    if (!result)
        return luka_null(luka);

    //获得字段
    fields = luka_array_create(luka);
    while (field = mysql_fetch_field(result)) {
        luka_array_push(luka, fields, field->name);
    }

    //读取数据
    arrayP = luka_put_array(luka);
    array = luka_get_array(luka, arrayP);
    while (row = mysql_fetch_row(result)) {
        objectP = luka_put_object(luka);
        object = luka_get_object(luka, objectP);

        for (index = 0; index < luka_array_length(luka, fields); index++) {
            voidp input_data_p = luka_put_string(luka, luka_strdup(luka, row[index]));
            luka_data_up(luka, input_data_p);
            luka_object_put(luka, object, (const char *)luka_array_get(luka, fields, index), input_data_p);
        }

        luka_data_up(luka, objectP);
        luka_array_push(luka, array, objectP);
    }
    mysql_free_result(result);
    luka_array_destroy(luka, fields);
    return arrayP;
}

/** mysql_find **/
static voidp luka_mysql_mysql_find (Luka *luka, voidp *p, size_t n) {
    MYSQL *mysql = NULL;
    const char *sql = NULL;
    int ret = 0;

    int index = 0;
    LukaArray *fields = NULL;
    LukaArray *fieldsType = NULL;

    MYSQL_RES   *result = NULL;
    MYSQL_FIELD *field = NULL, *field_mov = NULL;
    MYSQL_ROW    row = NULL;

    voidp objectP = luka_null(luka);
    LukaObject *object = NULL;

    if (n < 2 || !luka_is_voidp(p[0]) || !luka_is_string(p[1]))
        return luka_null(luka);

    mysql = (MYSQL *)luka_get_voidp(luka, p[0]);
    sql = luka_get_string(luka, p[1]);
    if (!mysql || *sql == 0)
        return luka_null(luka);

    ret = mysql_query(mysql, sql);
    if (ret != 0)
        return luka_null(luka);

    result = mysql_store_result(mysql);
    if (!result)
        return luka_null(luka);

    //获得字段
    fields = luka_array_create(luka);
    while (field = mysql_fetch_field(result)) {
        luka_array_push(luka, fields, field->name);
    }

    //读取数据
    if (row = mysql_fetch_row(result)) {
        objectP = luka_put_object(luka);
        object = luka_get_object(luka, objectP);

        for (index = 0; index < luka_array_length(luka, fields); index++) {
            voidp input_data_p = luka_put_string(luka, luka_strdup(luka, row[index]));
            luka_data_up(luka, input_data_p);
            luka_object_put(luka, object, (const char *)luka_array_get(luka, fields, index), input_data_p);
        }
    }
    mysql_free_result(result);
    luka_array_destroy(luka, fields);
    return objectP;
}

/***********************************************************
  PackageMYSQL  
***********************************************************/

void luka_mysql_regs (Luka *luka) {
    luka_reg(luka, "mysql_connect",           luka_mysql_mysql_connect);
    luka_reg(luka, "mysql_close",             luka_mysql_mysql_close);
    luka_reg(luka, "mysql_query",             luka_mysql_mysql_query);
    luka_reg(luka, "mysql_set_character_set", luka_mysql_mysql_set_character_set);
    luka_reg(luka, "mysql_select",            luka_mysql_mysql_select);
    luka_reg(luka, "mysql_find",              luka_mysql_mysql_find);
}

void luka_mysql_vars (Luka *luka) {
}

