/*
 * test.c
 *
 *  Created on: 2018-8-21
 *  Author:        hatsusakana@gmail.com
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "qrencode.h"

int main (int argc, char *argv[]) {
    QRcode* pQRC = NULL;

    if (pQRC = QRcode_encodeString("https://www.baidu.com/", 0, QR_ECLEVEL_H, QR_MODE_8, 1)) {
        QRcode_NewBGR24(pQRC, 0x00, 0x00, 0x00);
        QRcode_SaveBMP(pQRC, "1.bmp");
    }
    QRcode_free(pQRC);
    return 1;
}
