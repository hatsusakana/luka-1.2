/***********************************************************
  pkg_csv.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../luka.h"

static char *csv_substr (Luka *luka, const char *left, const char *right) {
    size_t size = right - left;
    char *str = NULL;

    str = (char *)luka_malloc(luka, size + 2);
    if (size != 0)
        memcpy(str, left, size);
    return str;
}

/** 剔除首尾 [空格,\t,\r,\n,\x0B] **/
static char *_trim (char *s) {
    char *p = NULL;

    if (s == NULL || strlen(s) == 0) return s;

    p = s + strlen(s) - 1;
    while (*p == ' ' || *p == '\t' || *p == '\r' || *p == '\n' || *p == '\x0B')
        *p-- = '\0';

    p = s;
    while (*p == ' ' || *p == '\t' || *p == '\r' || *p == '\n' || *p == '\x0B')
        p++;

    if (p != s) {
        memmove(s, p, strlen(p) + 1);
    }

    return s;
}

/** 处理转义 **/
static void csv_convert (char *p) {
    _trim(p);

    if (*p != '\"')
        return;
    
    memmove(p, p + 1, strlen(p + 1) + 1);
    if (p[strlen(p) - 1] == '\"')
        p[strlen(p) - 1] = 0;

    while (*p != 0) {
        if (*p == '\"') {
            memmove(p, p + 1, strlen(p + 1) + 1);
        }
        p++;
    }
}

/** 解析列 **/
static voidp csv_decode_column (Luka *luka, const char *line) {
    int convert_flag = 0;
    const char *left = line, *right = line;
    char *strbuf = NULL;
    voidp pbuf = NULL;

    LukaArray *csv_array = NULL;
    voidp csv_array_p = NULL;

    csv_array_p = luka_put_array(luka);
    csv_array = luka_get_array(luka, csv_array_p);

    while (*right != 0) {
        if (convert_flag == 0) {
            if (*right == '"') {
                convert_flag = 1;
            } else if (*right == ',') {
                strbuf = csv_substr(luka, left, right);
                csv_convert(strbuf);
                left = right + 1;
                pbuf = luka_put_string(luka, strbuf);
                luka_array_push(luka, csv_array, pbuf);
                luka_data_up(luka, pbuf);
            }
        } else {
            if (*right == '\"' && *(right + 1) == '\"') {
                right++;
            } else if (*right == '\"') {
                convert_flag = 0;
            }
        }
        right++;
    }

    strbuf = luka_strdup(luka, left);
    csv_convert(strbuf);
    pbuf = luka_put_string(luka, strbuf);
    luka_array_push(luka, csv_array, pbuf);
    luka_data_up(luka, pbuf);
    return csv_array_p;
}

/** 解析行 **/
static void csv_decode_line (Luka *luka, const char *s, LukaArray *csv_array) {
    int convert_flag = 0;
    const char *left = s, *right = s;
    char *strbuf = NULL;
    voidp pbuf = NULL;

    while (*right != 0) {
        if (convert_flag == 0) {
            if (*right == '"') {
                convert_flag = 1;
            } else if (*right == '\n') {
                strbuf = csv_substr(luka, left, right);
                left = right + 1;
                pbuf = csv_decode_column(luka, strbuf);
                luka_array_push(luka, csv_array, pbuf);
                luka_data_up(luka, pbuf);
            }
        } else {
            if (*right == '\"' && *(right + 1) == '\"') {
                right++;
            } else if (*right == '\"') {
                convert_flag = 0;
            }
        }
        right++;
    }

    strbuf = luka_strdup(luka, left);
    pbuf = csv_decode_column(luka, strbuf);
    luka_array_push(luka, csv_array, pbuf);
    luka_data_up(luka, pbuf);
}

/** csv_decode **/
static voidp luka_csv_csv_decode (Luka *luka, voidp *p, size_t n) {
    LukaArray *csv_array = NULL;
    voidp csv_array_p = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    csv_array_p = luka_put_array(luka);
    csv_array = luka_get_array(luka, csv_array_p);
    csv_decode_line(luka, luka_get_string(luka, p[0]), csv_array);
    return csv_array_p;
}

/***********************************************************
  PackageCSV(.csv文件读写) 
***********************************************************/

void luka_csv_regs (Luka *luka) {
    luka_reg(luka, "csv_decode", luka_csv_csv_decode);
}

