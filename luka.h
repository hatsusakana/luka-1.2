#ifndef LUKA_H
#define LUKA_H

//启用main函数
#define CONFIG_MAIN

typedef void* voidp;
typedef struct LukaStack  LukaStack;
typedef struct LukaObject LukaObject;
typedef struct LukaArray  LukaArray;
typedef struct Luka Luka;

#ifndef CONFIG_MAIN
int         luka_main                (int argc, char *argv[]);
#endif

/** 内存管理 **/
void        luka_exception           (Luka *luka, int code);
void       *luka_malloc              (Luka *luka, size_t n);
char       *luka_strdup              (Luka *luka, const char *s);
void        luka_free                (Luka *luka, void *p);

/** 注册C函数 **/
void        luka_reg                 (Luka *luka, const char *func_name, 
                                        voidp (*func_p)(Luka *, voidp *, size_t));
void        luka_reg_var             (Luka *luka, const char *name, voidp p);

/** 调用函数 **/
voidp       luka_call_func           (Luka *luka, const char *func_name, voidp *p, size_t n);

/** 上调计数器 **/
void        luka_data_up             (Luka *luka, voidp p);

/** 添加数据 **/
voidp       luka_null                (Luka *luka);
voidp       luka_true                (Luka *luka);
voidp       luka_false               (Luka *luka);
voidp       luka_put_int             (Luka *luka, long i);
voidp       luka_put_double          (Luka *luka, double d);
voidp       luka_put_string          (Luka *luka, char *s);
voidp       luka_put_byte            (Luka *luka, unsigned char *datap, size_t size);
voidp       luka_put_object          (Luka *luka);
voidp       luka_put_array           (Luka *luka);
voidp       luka_put_voidp           (Luka *luka, void *p);

/** 获得数据 **/
long        luka_get_int             (Luka *luka, voidp p);
double      luka_get_double          (Luka *luka, voidp p);
const char *luka_get_string          (Luka *luka, voidp p);
unsigned char *luka_get_byte         (Luka *luka, voidp p, size_t *size);
LukaObject *luka_get_object          (Luka *luka, voidp p);
LukaArray  *luka_get_array           (Luka *luka, voidp p);
void       *luka_get_voidp           (Luka *luka, voidp p);

/** 对比数据类型 **/
int         luka_is_null             (voidp p);
int         luka_is_true             (voidp p);
int         luka_is_false            (voidp p);
int         luka_is_int              (voidp p);
int         luka_is_double           (voidp p);
int         luka_is_string           (voidp p);
int         luka_is_byte             (voidp p);
int         luka_is_object           (voidp p);
int         luka_is_array            (voidp p);
int         luka_is_voidp            (voidp p);

/** LukaStack **/
LukaStack  *luka_stack_create        (Luka *luka);
void        luka_stack_push          (Luka *luka, LukaStack *stack, void *p);
void       *luka_stack_top           (Luka *luka, LukaStack *stack);
void        luka_stack_pop           (Luka *luka, LukaStack *stack);
void        luka_stack_destroy       (Luka *luka, LukaStack *stack);

/** LukaObject **/
LukaObject *luka_object_create       (Luka *luka);
void        luka_object_setcb        (Luka *luka, LukaObject *object, 
                                        void (*in_proc)(Luka *luka, voidp p), 
                                        void (*out_proc)(Luka *luka, voidp p));
void        luka_object_put          (Luka *luka, LukaObject *object, const char *key, voidp p);
voidp       luka_object_get          (Luka *luka, LukaObject *object, const char *key);
void        luka_object_rmv          (Luka *luka, LukaObject *object, const char *key);
void        luka_object_each         (Luka *luka, LukaObject *object, void *p, 
                                        void (*cb)(Luka *, LukaObject *, void *, const char *, voidp));
void        luka_object_destroy      (Luka *luka, LukaObject *object);

/** LukaArray **/
LukaArray  *luka_array_create        (Luka *luka);
void        luka_array_setcb         (Luka *luka, LukaArray *array, 
                                        void (*in_proc)(Luka *luka, voidp p), 
                                        void (*out_proc)(Luka *luka, voidp p));
void        luka_array_push          (Luka *luka, LukaArray *array, voidp p);
void        luka_array_put           (Luka *luka, LukaArray *array, size_t i, voidp p);
size_t      luka_array_length        (Luka *luka, LukaArray *array);
voidp       luka_array_get           (Luka *luka, LukaArray *array, size_t i);
void        luka_array_destroy       (Luka *luka, LukaArray *array);

#endif

