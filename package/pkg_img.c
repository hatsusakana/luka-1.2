/***********************************************************
  pkg_img.c 图像处理 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "../luka.h"

typedef struct LukaPkgImgData {
    unsigned char *data;
    size_t size, w, h;
} LukaPkgImgData;

/** luka数据转c数据 **/
static int decode_data (Luka *luka, voidp p, LukaPkgImgData *data) {
    voidp       bufp    = NULL;
    LukaObject *bmp_obj = NULL;

    bmp_obj = luka_get_object(luka, p);

    if ((bufp = luka_object_get(luka, bmp_obj, "data")) == NULL || !luka_is_byte(bufp))
        return 0;

    data->data = luka_get_byte(luka, bufp, &data->size);
    if (!data)
        return 0;

    if ((bufp = luka_object_get(luka, bmp_obj, "w")) == NULL || !luka_is_int(bufp))
        return 0;
    data->w = (size_t)(abs((int)luka_get_int(luka, bufp)));

    if ((bufp = luka_object_get(luka, bmp_obj, "h")) == NULL || !luka_is_int(bufp))
        return 0;
    data->h = (size_t)(abs((int)luka_get_int(luka, bufp)));
    return 1;
}

/** c数据转luka数据 **/
static voidp make_data (Luka *luka, LukaPkgImgData *data) {
    LukaObject *bmp_obj = NULL;
    voidp bmp_obj_p = NULL, buf_p = NULL;

    bmp_obj_p = luka_put_object(luka);
    bmp_obj = luka_get_object(luka, bmp_obj_p);

    buf_p = luka_put_byte(luka, data->data, data->size);
    luka_object_put(luka, bmp_obj, "data", buf_p);
    luka_data_up(luka, buf_p);

    buf_p = luka_put_int(luka, data->w);
    luka_object_put(luka, bmp_obj, "w", buf_p);
    luka_data_up(luka, buf_p);

    buf_p = luka_put_int(luka, data->h);
    luka_object_put(luka, bmp_obj, "h", buf_p);
    luka_data_up(luka, buf_p);
    return bmp_obj_p;
}

/** 复制s1一样大小的空间 **/
static void copy_space (Luka *luka, LukaPkgImgData *s1, LukaPkgImgData *s2) {
    s2->w = s1->w;
    s2->h = s1->h;
    s2->size = s1->size;
    s2->data = (unsigned char *)luka_malloc(luka, s2->size);
}

/***********************************************************
  BMP文件  
***********************************************************/

typedef unsigned short WORD;
typedef unsigned int DWORD;
 
#pragma pack(1)
typedef struct {
    WORD  bfType;
    DWORD bfSize;
    WORD  bfReserved1;
    WORD  bfReserved2;
    DWORD bfOffBits;
} BITMAP_FILE_HEADER;
 
typedef struct BITMAP_INFO_HEADER {
    DWORD biSize;
    int   biWidth;
    int   biHeight;
    WORD  biPlanes;
    WORD  biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    int   biXPelsPerMeter;
    int   biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
} BITMAP_INFO_HEADER;
#pragma pack()

/**
 * 32->24
 */
static void bmp_remove_aplpha (unsigned char *data, unsigned char *buf, size_t *size, size_t w, size_t h) {
    size_t i = 0, j = 0, dst_fix = 0;
 
    dst_fix = 4 - ((w * 24)>>3) & 3;
    *size = ((((w * 24) + 31) >> 5) << 2) * h;
 
    for (j = 0; j < h; j++) {
        for (i = 0; i < w; i++) {
            memcpy(buf, data, 3);
            data += 4;
            buf += 3;
        }
        buf += dst_fix;
    }
}
 
/**
 * 倒转图像
 */
static void bmp_reverse (unsigned char *data, unsigned char *buf, size_t size, size_t w, size_t h) {
    unsigned char *src = NULL;
    int i = 0, j = 0, preline = 0;
     
    preline = size / h;
    for (i = 0, j = h - 1; j >= 0; i++, j--) {
        src = data + j * preline;
        memcpy(buf + i * preline, src, preline);
    }
}
 
/**
 * bmp_read
 *
 * 读取一个BMP文件
 * 只支持24/32位，32位会转为24位，倒转的图像会摆正
 */
static unsigned char *bmp_read (Luka *luka, const char *path, size_t *size, size_t *w, size_t *h) {
    FILE *fp = NULL;
    BITMAP_FILE_HEADER file = {0};
    BITMAP_INFO_HEADER info = {0};
    unsigned char *data = NULL, *buf = NULL;
     
    if (!path || *path == 0 || !size || !w || !h)
        return NULL;
     
    if (!(fp = fopen(path, "rb")))
        return NULL;
 
    if (fread(&file, 1, sizeof(BITMAP_FILE_HEADER), fp) != sizeof(BITMAP_FILE_HEADER)) {
        fclose(fp);
        return NULL;
    }
 
    if (fread(&info, 1, sizeof(BITMAP_INFO_HEADER), fp) != sizeof(BITMAP_INFO_HEADER)) {
        fclose(fp);
        return NULL;
    }
 
    //非BM开头
    if (file.bfType != 0x4d42) {
        fclose(fp);
        return NULL;
    }
 
    //暂时只能支持24与32位
    if (info.biBitCount != 24 && info.biBitCount != 32) {
        fclose(fp);
        return NULL;
    }
 
    if (info.biSizeImage == 0) {
        info.biSizeImage = info.biWidth * abs(info.biHeight) * (info.biBitCount == 24 ? 3 : 4);
    }
    
    data = (unsigned char *)luka_malloc(luka, sizeof(unsigned char) * info.biSizeImage);
    buf = (unsigned char *)luka_malloc(luka, sizeof(unsigned char) * info.biSizeImage);
     
    if (fread(data, 1, info.biSizeImage, fp) != info.biSizeImage) {
        luka_free(luka, data);
        luka_free(luka, buf);
        fclose(fp);
        return NULL;
    }
     
    fclose(fp);
     
    *size = info.biSizeImage;
    *w = info.biWidth;
    *h = abs(info.biHeight);
     
    //32转24
    if (info.biBitCount == 32) {
        bmp_remove_aplpha(data, buf, size, *w, *h);
        memcpy(data, buf, *size);
    }
     
    //图像倒转
    if (info.biHeight > 0) {
        bmp_reverse(data, buf, *size, *w, *h);
        memcpy(data, buf, *size);
    }
 
    luka_free(luka, buf);
    return data;
}

/**
 * bmp_write
 */
static int bmp_write (unsigned char *data, size_t size, size_t w, size_t h, const char *path) {
    FILE *fp = NULL;
    BITMAP_FILE_HEADER file = {0};
    BITMAP_INFO_HEADER info = {0};
    unsigned char *src = NULL;
    int hsrc = 0, preline = 0;
     
    if (!data || size == 0 || w == 0 || h == 0 || !path || *path == 0)
        return 0;
     
    if (!(fp = fopen(path, "wb+")))
        return 0;
 
    file.bfType = 0x4d42;
    file.bfSize = sizeof(BITMAP_FILE_HEADER) + sizeof(BITMAP_INFO_HEADER) + size;
    file.bfReserved1 = 0;
    file.bfReserved2 = 0;
    file.bfOffBits = sizeof(BITMAP_FILE_HEADER) + sizeof(BITMAP_INFO_HEADER);
     
    info.biSize = sizeof(BITMAP_INFO_HEADER);
    info.biWidth = w;
    info.biHeight = h;
    info.biPlanes = 1;
    info.biBitCount = 24;
    info.biCompression = 0L;
    info.biSizeImage = size;
    info.biXPelsPerMeter = 0;
    info.biYPelsPerMeter = 0;
    info.biClrUsed = 0;
    info.biClrImportant = 0;
     
    fwrite(&file, 1, sizeof(BITMAP_FILE_HEADER), fp);
    fwrite(&info, 1, sizeof(BITMAP_INFO_HEADER), fp);
     
    //倒转写入图像数据
    preline = size / h;
    for (hsrc = h - 1; hsrc >= 0; hsrc--) {
        src = data + hsrc * preline;
        fwrite(src, 1, preline, fp);
    }
     
    fclose(fp);
    return 1;
}

/** img_read_bmp **/
static voidp luka_img_img_read_bmp (Luka *luka, voidp *p, size_t n) {
    const char *filepath = NULL;

    unsigned char *data = NULL;
    size_t size = 0, w = 0, h = 0;

    LukaObject *bmp_obj = NULL;
    voidp bmp_obj_p = NULL, buf_p = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    filepath = luka_get_string(luka, p[0]);
    if (*filepath == 0)
        return luka_null(luka);

    data = bmp_read(luka, filepath, &size, &w, &h);
    if (!data)
        return luka_null(luka);

    bmp_obj_p = luka_put_object(luka);
    bmp_obj = luka_get_object(luka, bmp_obj_p);

    buf_p = luka_put_byte(luka, data, size);
    luka_object_put(luka, bmp_obj, "data", buf_p);
    luka_data_up(luka, buf_p);

    buf_p = luka_put_int(luka, w);
    luka_object_put(luka, bmp_obj, "w", buf_p);
    luka_data_up(luka, buf_p);

    buf_p = luka_put_int(luka, h);
    luka_object_put(luka, bmp_obj, "h", buf_p);
    luka_data_up(luka, buf_p);
    return bmp_obj_p;
}

/** img_write_bmp **/
static voidp luka_img_img_write_bmp (Luka *luka, voidp *p, size_t n) {
    const char *filepath = NULL;
    LukaPkgImgData img_data = {0};

	if (n < 2 || !luka_is_object(p[0]) || !luka_is_string(p[1]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    filepath = luka_get_string(luka, p[1]);
    if (*filepath == 0)
        return luka_null(luka);

    return bmp_write(img_data.data, img_data.size, img_data.w, img_data.h, filepath) != -1 ? luka_true(luka) : luka_false(luka);
}

/***********************************************************
  图像处理(BGR数据)  
***********************************************************/

static int rgb (int value) {
    if (value < 0) return 0;
    if (value > 255) return 255;
    return value;
}

/** img_rect **/
static voidp luka_img_img_rect (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int left = 0, top = 0, right = 0, bottom = 0;
    int src_w = 0, src_h = 0, dst_w = 0, dst_h = 0;
    int src_speed = 0, dst_speed = 0;
    int src_perline = 0, dst_perline = 0;

	if (n < 5 || !luka_is_object(p[0]) || !luka_is_int(p[1]) || !luka_is_int(p[2]) || !luka_is_int(p[3]) || !luka_is_int(p[4]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    left = luka_get_int(luka, p[1]);
    top = luka_get_int(luka, p[2]);
    right = luka_get_int(luka, p[3]);
    bottom = luka_get_int(luka, p[4]);
    if (left > right || bottom < top)
        return luka_null(luka);

    if (left < 0) left = 0;
    if (top < 0) top = 0;
    if (right > img_data.w) right = img_data.w;
    if (bottom > img_data.h) bottom = img_data.h;

    new_data.w = right - left + 1;
    new_data.h = bottom - top + 1;
    new_data.size = new_data.h * ((new_data.w * 24 + 31) / 32 * 4);
    new_data.data = (unsigned char *)luka_malloc(luka, new_data.size);

    src_perline = ((img_data.w * 24 + 31) / 32 * 4);
    dst_perline = ((new_data.w * 24 + 31) / 32 * 4);
    for (src_h = top, dst_h = 0; src_h <= bottom; src_h++, dst_h++) {
        for (src_w = left, dst_w = 0; src_w <= right; src_w++, dst_w++) {
            src_speed = src_h * src_perline + src_w * 3;
            dst_speed = dst_h * dst_perline + dst_w * 3;
            new_data.data[dst_speed + 0] = img_data.data[src_speed + 0];
            new_data.data[dst_speed + 1] = img_data.data[src_speed + 1];
            new_data.data[dst_speed + 2] = img_data.data[src_speed + 2];
        }
    }
    return make_data(luka, &new_data);
}

/** img_reverse(倒转图像) **/
static voidp luka_img_img_reverse (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int src_w = 0, src_h = 0, dst_w = 0, dst_h = 0, preline = 0;
    unsigned char *src = NULL, *dst = NULL;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    preline = (img_data.w * 24 + 31) / 32 * 4;
    copy_space(luka, &img_data, &new_data);
    for (src_h = 0, dst_h = new_data.h - 1; src_h < (int)img_data.h; src_h++, dst_h--) {
        src = img_data.data + src_h * preline;
        dst = new_data.data + dst_h * preline;
        memcpy(dst, src, preline);
    }
    return make_data(luka, &new_data);
}

/** img_horflip(水平翻转图像) **/
static voidp luka_img_img_horflip (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int src_w = 0, src_h = 0, dst_w = 0, dst_h = 0, preline = 0;
    int src_speed = 0, dst_speed = 0;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    copy_space(luka, &img_data, &new_data);
    preline = (img_data.w * 24 + 31) / 32 * 4;
    for (src_w = 0, dst_w = new_data.w - 1; src_w < (int)img_data.w; src_w++, dst_w--) {
        for (src_h = 0; src_h < (int)img_data.h; src_h++) {
            src_speed = src_h * preline + src_w * 3;
            dst_speed = src_h * preline + dst_w * 3;
            new_data.data[dst_speed + 0] = img_data.data[src_speed + 0];
            new_data.data[dst_speed + 1] = img_data.data[src_speed + 1];
            new_data.data[dst_speed + 2] = img_data.data[src_speed + 2];
        }
    }
    return make_data(luka, &new_data);
}

/** img_histogram(直方图数据) **/
static voidp luka_img_img_histogram (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0};
    int mov_w = 0, mov_h = 0, mov_speed = 0, mov_preline = 0;

    //返回obj
    LukaObject *his = NULL;
    voidp his_p = NULL;

    //直方图数据
    LukaArray *r = NULL, *g = NULL, *b = NULL;
    voidp r_p = NULL, g_p = NULL, b_p = NULL;

    //临时变量
    voidp buf_p = NULL;

    //数据
    int *histogram[3] = {NULL, NULL, NULL};

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    his_p = luka_put_object(luka);
    his = luka_get_object(luka, his_p);
    b_p = luka_put_array(luka);
    b = luka_get_array(luka, b_p);
    g_p = luka_put_array(luka);
    g = luka_get_array(luka, g_p);
    r_p = luka_put_array(luka);
    r = luka_get_array(luka, r_p);

    luka_object_put(luka, his, "b", b_p);
    luka_object_put(luka, his, "g", g_p);
    luka_object_put(luka, his, "r", r_p);
    luka_data_up(luka, b_p);
    luka_data_up(luka, g_p);
    luka_data_up(luka, r_p);

    mov_preline = (img_data.w * 24 + 31) / 32 * 4;
    histogram[0] = (int *)luka_malloc(luka, sizeof(int) * 256);
    histogram[1] = (int *)luka_malloc(luka, sizeof(int) * 256);
    histogram[2] = (int *)luka_malloc(luka, sizeof(int) * 256);

    for (mov_h = 0; mov_h < (int)img_data.h; mov_h++) {
        for (mov_w = 0; mov_w < (int)img_data.w; mov_w++) {
            mov_speed = mov_preline * mov_h + mov_w * 3;
            histogram[0][rgb(img_data.data[mov_speed + 0])]++;
            histogram[1][rgb(img_data.data[mov_speed + 1])]++;
            histogram[2][rgb(img_data.data[mov_speed + 2])]++;
        }
    }

    for (mov_h = 0; mov_h < 256; mov_h++) {
        buf_p = luka_put_int(luka, histogram[0][mov_h]);
        luka_array_put(luka, b, mov_h, buf_p);
        luka_data_up(luka, buf_p);
        
        buf_p = luka_put_int(luka, histogram[1][mov_h]);
        luka_array_put(luka, g, mov_h, buf_p);
        luka_data_up(luka, buf_p);
        
        buf_p = luka_put_int(luka, histogram[2][mov_h]);
        luka_array_put(luka, r, mov_h, buf_p);
        luka_data_up(luka, buf_p);
    }

    luka_free(luka, histogram[0]);
    luka_free(luka, histogram[1]);
    luka_free(luka, histogram[2]);
    return his_p;
}

/** img_convgray(灰度图) **/
static voidp luka_img_img_convgray (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int mov_w = 0, mov_h = 0, mov_speed = 0, mov_preline = 0;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    copy_space(luka, &img_data, &new_data);
    mov_preline = (img_data.w * 24 + 31) / 32 * 4;
    for (mov_h = 0; mov_h < (int)img_data.h; mov_h++) {
        for (mov_w = 0; mov_w < (int)img_data.w; mov_w++) {
            int garyval = 0;
            mov_speed = mov_preline * mov_h + mov_w * 3;
            garyval = (int)(img_data.data[mov_speed + 2] * 0.3 + img_data.data[mov_speed + 1] * 0.59 + img_data.data[mov_speed] * 0.11);
            new_data.data[mov_speed + 0] = garyval;
            new_data.data[mov_speed + 1] = garyval;
            new_data.data[mov_speed + 2] = garyval;
        }
    }
    return make_data(luka, &new_data);
}

/** img_binary(二值化) **/
static voidp luka_img_img_binary (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int mov_w = 0, mov_h = 0, mov_speed = 0, mov_preline = 0, k = 0, avg = 0;

	if (n < 2 || !luka_is_object(p[0]) || !luka_is_int(p[1]))
		return luka_null(luka);

    k = luka_get_int(luka, p[1]);
    if (k < 0 || k > 255)
        k = 125;

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    copy_space(luka, &img_data, &new_data);
    mov_preline = (img_data.w * 24 + 31) / 32 * 4;
    for (mov_h = 0; mov_h < (int)img_data.h; mov_h++) {
        for (mov_w = 0; mov_w < (int)img_data.w; mov_w++) {
            mov_speed = mov_preline * mov_h + mov_w * 3;

            avg = (img_data.data[mov_speed] + img_data.data[mov_speed + 1] + img_data.data[mov_speed + 2]) / 3;
            if (avg > k) {
                new_data.data[mov_speed + 0] = new_data.data[mov_speed + 1] = new_data.data[mov_speed + 2] = 0xff;
            } else {
                new_data.data[mov_speed + 0] = new_data.data[mov_speed + 1] = new_data.data[mov_speed + 2] = 0x00;
            }
        }
    }
    return make_data(luka, &new_data);
}

/** otsu算法 **/
static int _otsu (LukaPkgImgData *data) {
    int threshold = 0, i = 0, j = 0, pixnum = 0;
    float hisportion[256] = {0};   //直方图比例
    
    //占最大比例的像素
    float maxpoint = 0;
    int maxpoint_i = 0;
    
    float w0, w1, u0tmp, u1tmp, u0, u1, u, deltaTmp, deltaMax = 0;

    pixnum = data->w * data->h;
    
    for (i = 0; i < 256; i++) {
        hisportion[i] = (float)data->data[i] / pixnum;
        if (hisportion[i] > maxpoint) {
            maxpoint = hisportion[i];
            maxpoint_i = i;
        }
    }
    
    //遍历灰度级[0,255]
    for (i = 0; i < 256; i++) {    //i作为阈值
        w0 = w1 = u0tmp = u1tmp = u0 = u1 = u = deltaTmp = 0;
        for (j = 0; j < 256; j++) {
            if (j <= i) {   //背景
                w0 += hisportion[j];
                u0tmp += j * hisportion[j];
            } else {        //前景
                w1 += hisportion[j];
                u1tmp += j * hisportion[j];
            }
        }
        u0 = u0tmp / w0;
        u1 = u1tmp / w1;
        u = u0tmp + u1tmp;
        deltaTmp = (float)(w0 * pow((u0 - u), 2) + w1 * pow((u1 - u), 2));
        if (deltaTmp > deltaMax) {
            deltaMax = deltaTmp;
            threshold = i;
        }
    }
    return threshold;
}

/** img_otsu(二值化算法) **/
static voidp luka_img_img_otsu (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int mov_w = 0, mov_h = 0, mov_speed = 0, mov_preline = 0, k;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    //转灰度图
    copy_space(luka, &img_data, &new_data);
    mov_preline = (img_data.w * 24 + 31) / 32 * 4;
    for (mov_h = 0; mov_h < (int)img_data.h; mov_h++) {
        for (mov_w = 0; mov_w < (int)img_data.w; mov_w++) {
            int garyval = 0;
            mov_speed = mov_preline * mov_h + mov_w * 3;
            garyval = (int)(img_data.data[mov_speed + 2] * 0.3 + img_data.data[mov_speed + 1] * 0.59 + img_data.data[mov_speed] * 0.11);
            new_data.data[mov_speed + 0] = garyval;
            new_data.data[mov_speed + 1] = garyval;
            new_data.data[mov_speed + 2] = garyval;
        }
    }

    k = _otsu(&new_data);
    luka_free(luka, new_data.data);
    return luka_put_int(luka, k);
}

/** img_boxfilter(方框滤波) **/
static voidp luka_img_img_boxfilter (Luka *luka, voidp *p, size_t n) {
    LukaPkgImgData img_data = {0}, new_data = {0};
    int mov_w = 0, mov_h = 0, mov_speed = 0, mov_preline = 0;
    int box = 0, avg = 0;

	if (n < 2 || !luka_is_object(p[0]) || !luka_is_int(p[1]))
		return luka_null(luka);

    if (!decode_data(luka, p[0], &img_data))
        return luka_null(luka);

    box = luka_get_int(luka, p[1]);
    if (box < 0 || box > img_data.w || box > img_data.h)
        return luka_null(luka);

    copy_space(luka, &img_data, &new_data);
    mov_preline = (img_data.w * 24 + 31) / 32 * 4;
    for (mov_h = 0; mov_h < (int)img_data.h; mov_h++) {
        for (mov_w = 0; mov_w < (int)img_data.w; mov_w++) {
            mov_speed = mov_preline * mov_h + mov_w * 3;
        }
    }
    return make_data(luka, &new_data);
}

/***********************************************************
  PackageImg  
***********************************************************/

void luka_img_regs (Luka *luka) {
    /** BMP **/
    luka_reg(luka, "img_read_bmp",   luka_img_img_read_bmp);
    luka_reg(luka, "img_write_bmp",  luka_img_img_write_bmp);

    /** 图像处理(BGR数据) **/
    luka_reg(luka, "img_rect",       luka_img_img_rect);
    luka_reg(luka, "img_reverse",    luka_img_img_reverse);
    luka_reg(luka, "img_horflip",    luka_img_img_horflip);
    luka_reg(luka, "img_histogram",  luka_img_img_histogram);
    luka_reg(luka, "img_convgray",   luka_img_img_convgray);
    luka_reg(luka, "img_binary",     luka_img_img_binary);
    luka_reg(luka, "img_otsu",       luka_img_img_otsu);
    luka_reg(luka, "img_boxfilter",  luka_img_img_boxfilter);
}

void luka_img_vars (Luka *luka) {
}

