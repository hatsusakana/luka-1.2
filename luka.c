/***********************************************************
  luka-1.2
  
  luka, 一个由C语言实现的动态语言。

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>
#include <ctype.h>

#ifdef _WIN32
#include <direct.h>
#else
#include <unistd.h>
#endif

#ifdef _WIN32
#define getcwd   _getcwd
#define snprintf _snprintf
#endif

#include "luka.h"

/** 程序版本 **/
#define LUKA_VERSION      "luka-1.2.20200529"

/** 数组最大长度 **/
#define LUKA_ARRAY_MAX    2000000000

/** 路径最大长度 **/
#define LUKA_PATH_MAX     256

/** 虚拟主函数 **/
#define LUKA_MAIN         "__MAIN__" 

/** luka主函数 **/
#define LUKA_MAIN2        "luka_main"

/***********************************************************
  宏定义 
***********************************************************/

/** 错误码 **/
#define LUKA_OK                 0       //成功
#define LUKA_OOM                100     //内存不足
#define LUKA_FILE_ERR           101     //文件错误
#define LUKA_PARSE_ERR          102     //解析失败
#define LUKA_INCLUDE_ERR        103     //不可以重复include
#define LUKA_PACKAGE_ERR        104     //加载包失败
#define LUKA_CFUNC_ERR          105     //注册C函数失败

/** 数据类型 **/
#define LUKA_NULL               200
#define LUKA_TRUE               201
#define LUKA_FALSE              202
#define LUKA_INT                203
#define LUKA_DOUBLE             204
#define LUKA_STRING             205
#define LUKA_BYTE               206
#define LUKA_OBJECT             207
#define LUKA_ARRAY              208
#define LUKA_VOIDP              209

/** 语句类型 **/
#define LUKA_IF                 300
#define LUKA_ELSEIF             301
#define LUKA_ELSE               302
#define LUKA_WHILE              303
#define LUKA_FOR                304
#define LUKA_END                305
#define LUKA_EXPRESS            306
#define LUKA_RETURN             307
#define LUKA_FUNC               308
#define LUKA_BREAK              309
#define LUKA_CONTINUE           310

/** 解释器状态/语句类型 **/
#define LUKA_GCC_NULL           400     //空闲
#define LUKA_GCC_NOTE1          401     //# //
#define LUKA_GCC_NOTE2          402     // /* */
#define LUKA_GCC_INCLUDE        403     //include
#define LUKA_GCC_WHILE          404     //while
#define LUKA_GCC_IF             405     //if
#define LUKA_GCC_ELSE           406     //else
#define LUKA_GCC_EXPRESS        407     //express
#define LUKA_GCC_FOR            408     //for
#define LUKA_GCC_FUNC           409     //function
#define LUKA_GCC_RETURN         410     //return
#define LUKA_GCC_PACKAGE        411	    //package
#define LUKA_GCC_BREAK          412	    //break
#define LUKA_GCC_CONTINUE       413	    //continue
#define LUKA_GCC_END            414	    //end
#define LUKA_GCC_ELSEIF         415     //elseif

/** 表达式节点类型 **/
#define LUKA_EXP_DATA           500     //数据(代码中的静态数据)
#define LUKA_EXP_VAR            501     //变量
#define LUKA_EXP_FUNC           502     //函数调用
#define LUKA_EXP_OPER           503     //运算符
#define LUKA_EXP_OBJECT         504     //object
#define LUKA_EXP_ARRAY          505     //array

/** 运算符 **/
#define LUKA_OPER_OBJECT        600     //->
#define LUKA_OPER_ARRAY_LEFT    601     //[
#define LUKA_OPER_ARRAY_RIGHT   602     //]
#define LUKA_OPER_SMA_BRACL     603     //(
#define LUKA_OPER_SMA_BRACR     604     //)
#define LUKA_OPER_DIVIDE        605     //'/'
#define LUKA_OPER_TIMES         606     //*
#define LUKA_OPER_REMAINDER     607     //%
#define LUKA_OPER_PLUS          608     //+
#define LUKA_OPER_MINUS         609     //-
#define LUKA_OPER_MOREEQU       610     //>=
#define LUKA_OPER_MORE          611     //>
#define LUKA_OPER_LESSEQU       612     //<=
#define LUKA_OPER_LESS          613     //<
#define LUKA_OPER_EQU2          614     //==
#define LUKA_OPER_NOTQEU        615     //!=
#define LUKA_OPER_AND           616     //&&
#define LUKA_OPER_OR            617     //||
#define LUKA_OPER_EQU           618     //=
#define LUKA_OPER_DIVIDEQU      619     //'/='
#define LUKA_OPER_TIMESEQU      620     //*=
#define LUKA_OPER_REMAINEQU     621     //%=
#define LUKA_OPER_PLUSEQU       622     //+=
#define LUKA_OPER_MINUSEQU      623     //-=

/***********************************************************
  数据结构 
***********************************************************/

enum RB_COLOR {BLACK, RED};

/** RBTreeP **/
typedef struct RBTreePNode {
    enum RB_COLOR color;
    void *p;
    struct RBTreePNode *left, *right, *parent;
} RBTreePNode;

typedef struct RBTreeP {
    RBTreePNode *root, *nil;
} RBTreeP;

/** RBTreeC **/
typedef struct RBTreeCNode {
    enum RB_COLOR color;
    char *p;
    voidp value;
    struct RBTreeCNode *left, *right, *parent;
} RBTreeCNode;

typedef struct RBTreeC {
    RBTreeCNode *root, *nil;
} RBTreeC;

/** LukaByte **/
typedef struct Byte {
    unsigned char *data;
    size_t size;
} Byte;

/** LukaStack **/
typedef struct LukaStackNode {
    void *p;
    struct LukaStackNode *next;
} LukaStackNode;

struct LukaStack {
    LukaStackNode *head;
};

/** LukaStringList **/
typedef struct LukaStringList {
    char *s;
    struct LukaStringList *next;
} LukaStringList;

/** LukaObject **/
struct LukaObject {
	RBTreeC *object;

	void (*in_proc)(Luka *luka, voidp p);
	void (*out_proc)(Luka *luka, voidp p);
};

/** LukaArray **/
struct LukaArray {
	RBTreeC *array;
	size_t   length;

	void (*in_proc)(Luka *luka, voidp p);
	void (*out_proc)(Luka *luka, voidp p);
};

/** LukaData **/
typedef struct LukaData {
    int         type;
    int         index;

    //也许可以用union优化
    long        data_i;
    double      data_d;
    char        data_c;
    char       *data_str;
    Byte        data_byte;
    LukaObject *data_object;
    LukaArray  *data_array;
    void       *data_p;
} LukaData;

/** LukaStaMen **/
typedef struct LukaStaMen {
    const char *file;
    size_t line;

    int   type;
    char *express;
    char *a, *b, *c;
    char  *func_name;
    char **func_param;
    size_t func_len;

    struct LukaStaMen *next, *jump;
} LukaStaMen;

/** LukaExpress **/
typedef struct LukaExpress LukaExpress;
typedef struct LukaExpressNode {
    int           type;

    voidp         data_p;           //数据
    char         *data_var;         //变量
    char         *data_func;        //函数调用
    LukaExpress **data_param;
    size_t        data_len;
    int           data_oper;        //运算符
    char         *data_objkey;
    long          data_arrindex;

    struct LukaExpressNode *last, *next;
} LukaExpressNode;

struct LukaExpress {
    LukaExpressNode *queue, *queue_tail;
    LukaExpressNode *RPN, *RPN_tail;
    LukaExpressNode *stack;
};

/** LukaCode **/
typedef struct LukaCode {
    int          type;
    LukaExpress *express, *a, *b, *c;
    struct LukaCode *last, *next, *jump;
} LukaCode;

/** LukaFunc **/
typedef struct LukaFunc {
    char **func_param;
    size_t func_len;
    LukaCode *codes_head, *codes_tail;
} LukaFunc;

typedef struct LukaIWF {
    LukaCode *p;
    int flag;
} LukaIWF;

/** Luka **/
struct Luka {
    int             ret;
    jmp_buf         jump;
    RBTreeP        *memery;
    LukaData       *ntf[3];
    LukaObject     *cs;
    LukaStaMen     *stame, *stame_end;
    LukaStringList *files;
    LukaObject     *funcs;
	LukaStack      *queue;
    LukaObject     *mainvars;       //全局变量
    char           *mainpath;       //脚本路径
};

/***********************************************************
  RBTreeP 
***********************************************************/

static RBTreeP *rbtreep_create () {
    RBTreeP *tree = NULL;

    tree = (RBTreeP *)calloc(1, sizeof(RBTreeP));
    if (!tree)
        return NULL;
    
    tree->nil = (RBTreePNode *)calloc(1, sizeof(RBTreePNode));
    if (!tree->nil) {
        free(tree);
        return NULL;
    }

    tree->nil->color = BLACK;
    tree->nil->left = NULL;
    tree->nil->right = NULL;
    tree->nil->parent = NULL;
    tree->root = tree->nil;
    return tree;
}

static RBTreePNode *rbtreep_search (RBTreeP *tree, void *p) {
    RBTreePNode *x = tree->root;
    while (x != tree->nil) {
        if (p > x->p) x = x->right;
        else if(p < x->p) x = x->left;
        else if(p == x->p) return x;
    }
    return NULL;
}

static void rbtreep_left_rotate (RBTreeP *tree, RBTreePNode *x) {
    RBTreePNode *y = x->right;
    x->right = y->left;

    if (y->left != tree->nil)
        y->left->parent = x;
    if (x != tree->nil)
        y->parent = x->parent;
    if(x->parent == tree->nil) 
        tree->root = y; 
    else {
        if (x->parent->left == x)
            x->parent->left = y;
        else
            x->parent->right = y;
    }
    y->left = x;
    if (x != tree->nil)
        x->parent = y;
}

static void rbtreep_right_rotate (RBTreeP *tree, RBTreePNode *x) {
    RBTreePNode *y = x->left;
    x->left = y->right;

    if (y->right != tree->nil)
        y->right->parent = x;
    if (x != tree->nil)
        y->parent = x->parent;
    if (x->parent == tree->nil)
        tree->root = y;
    else {
        if(x->parent->left == x)
            x->parent->left = y;
        else
            x->parent->right = y;
    }
    y->right = x;
    if(x != tree->nil)
        x->parent = y;
}

static void rbtreep_insert_fixup (RBTreeP *tree, RBTreePNode *z) {
    RBTreePNode *y = NULL;
    while ((z->parent != NULL) && (z->parent->color == RED)) {
        if (z->parent == z->parent->parent->left) { 
            y = z->parent->parent->right; 
            if ((y != NULL) && (y->color == RED)) {
                z->parent->color = BLACK; 
                y->color = BLACK;
                z->parent->parent->color = RED; 
                z = z->parent->parent; 
            } else {
                if (z == z->parent->right) { 
                    z = z->parent;
                    rbtreep_left_rotate(tree, z);
                }
                z->parent->color = BLACK; 
                z->parent->parent->color = RED;
                rbtreep_right_rotate(tree, z->parent->parent);
            }        
        } else { 
            y = z->parent->parent->left;
            if ((y != NULL) && (y->color == RED)) {
                z->parent->color = BLACK;
                y->color = BLACK;
                z->parent->parent->color = RED;
                z = z->parent->parent;
            } else {
                if (z == z->parent->left) {
                    z = z->parent;
                    rbtreep_right_rotate(tree, z);
                }
                z->parent->color = BLACK;
                z->parent->parent->color = RED;
                rbtreep_left_rotate(tree, z->parent->parent);
            }
        }
    }
    tree->root->color = BLACK;
    tree->nil->color = BLACK;
}

static int rbtreep_insert (RBTreeP *tree, void *p) {
    RBTreePNode *x = tree->root, *y = tree->nil, *z;

    z = (RBTreePNode *)calloc(1, sizeof(RBTreePNode));
    if (!z)
        return 0;
    z->color = RED;
    z->p = p;
    z->parent = NULL;
    z->left = NULL;
    z->right = NULL;
    while (x != tree->nil) {
        y = x;
        if (z->p < x->p)
            x = x->left;
        else if (z->p > x->p)
            x = x->right;
        else if (z->p == x->p) {
            free(z);
            return 0;
        }
    }
    z->parent = y;
    if (y != tree->nil) {
        if(z->p > y->p)
            y->right = z;
        else if (z->p<y->p)
            y->left = z;
    } else {
        tree->root = z; 
    }
    z->left = tree->nil;
    z->right = tree->nil;
    rbtreep_insert_fixup(tree, z);
    return 1;
}

static void rbtreep_delete_fixup (RBTreeP *tree, RBTreePNode *x) {
    RBTreePNode *w = NULL;

    while ((x != tree->root) && (x->color == BLACK)) {
        if (x == x->parent->left) {
            w = x->parent->right;
            if (w->color == RED) {
                w->color = BLACK;
                x->parent->color = RED;
                rbtreep_left_rotate(tree, x->parent);
                w = x->parent->right;
            }
            if (w->left->color == BLACK && w->right->color == BLACK) {
                w->color = RED;
                x = x->parent;
            } else {
                if(w->right->color == BLACK) {
                    w->left->color = BLACK;
                    w->color = RED;
                    rbtreep_right_rotate(tree, w);
                    w = x->parent->right;
                } 
                w->color = w->parent->color;
                x->parent->color = BLACK;
                w->right->color = BLACK;
                rbtreep_left_rotate(tree, x->parent);
                x = tree->root;
            }
        } else {
            w = x->parent->left;
            if (w->color == RED) {
                w->color = BLACK;
                x->parent->color = RED;
                rbtreep_right_rotate(tree, x->parent);
                w = x->parent->left;
            }
            if (w->right->color == BLACK && w->left->color == BLACK) {
                w->color = RED;
                x = x->parent;
            } else {
                if (w->left->color == BLACK) {
                    w->right->color = BLACK;
                    w->color = RED;
                    rbtreep_left_rotate(tree, w);
                    w = x->parent->left;
                }
                w->color = w->parent->color;
                x->parent->color = BLACK;
                w->left->color = BLACK;
                rbtreep_right_rotate(tree, x->parent);
                x = tree->root;
            }
        }
    }
    x->color = BLACK;
}

static void rbtreep_delete (RBTreeP *tree, RBTreePNode *z) {
    RBTreePNode *y = NULL, *x = NULL;

    if (z->left == tree->nil || z->right == tree->nil) y=z;
    else {
        y = z->right;
        while (y->left != tree->nil) y = y->left;
    }
    if (y->left != tree->nil) x = y->left;
    else x = y->right;
    x->parent = y->parent;
    if (y->parent == tree->nil)
        tree->root = x;
    else {
        if (y->parent->left == y) y->parent->left = x;
        else y->parent->right = x;
    }
    if (y != z) z->p = y->p;
    if (y->color == BLACK) rbtreep_delete_fixup(tree, x);
    free(y);
}

static void *rbtreep_alloc (RBTreeP *tree, size_t n) {
    void *p = malloc(n);
    if (!p)
        return NULL;

    memset(p, 0, n);
    if (!rbtreep_insert(tree, p)) {
        free(p);
        return NULL;
    }
    return p;
}

static void rbtreep_free (RBTreeP *tree, void *p) {
    rbtreep_delete(tree, rbtreep_search(tree, p));
    free(p);
}

static void rbtreep_destroy_ex (RBTreeP *tree, RBTreePNode *node) {
    if (node == tree->nil)
        return;
    
    free(node->p);
    rbtreep_destroy_ex(tree, node->left);
    rbtreep_destroy_ex(tree, node->right);
    free(node);
}

static void rbtreep_destroy (RBTreeP *tree) {
    rbtreep_destroy_ex(tree, tree->root);
    free(tree->nil);
    free(tree);
}

/***********************************************************
  RBTreeP 
***********************************************************/

static RBTreeC *rbtreec_create (Luka *luka) {
    RBTreeC *tree = (RBTreeC *)luka_malloc(luka, sizeof(RBTreeC));
    tree->nil = (RBTreeCNode *)luka_malloc(luka, sizeof(RBTreeCNode));
    tree->nil->color = BLACK;
    tree->nil->left = NULL;
    tree->nil->right = NULL;
    tree->nil->parent = NULL;
    tree->root = tree->nil;
    return tree;
}

static RBTreeCNode *rbtreec_search (RBTreeC *tree, const char *s) {
    int ret = 0;
    RBTreeCNode *x = tree->root;
    while (x != tree->nil) {
        ret = strcmp(s, x->p);
        if (ret > 0) x = x->right;
        else if (ret < 0) x = x->left;
        else return x;
    }
    return NULL;
}

static void rbtreec_left_rotate (RBTreeC *tree, RBTreeCNode *x) {
    RBTreeCNode *y = x->right;
    x->right = y->left;

    if (y->left != tree->nil)
        y->left->parent = x;
    if (x != tree->nil)
        y->parent = x->parent;
    if(x->parent == tree->nil) 
        tree->root = y; 
    else {
        if (x->parent->left == x)
            x->parent->left = y;
        else
            x->parent->right = y;
    }
    y->left = x;
    if (x != tree->nil)
        x->parent = y;
}

static void rbtreec_right_rotate (RBTreeC *tree, RBTreeCNode *x) {
    RBTreeCNode *y = x->left;
    x->left = y->right;

    if (y->right != tree->nil)
        y->right->parent = x;
    if (x != tree->nil)
        y->parent = x->parent;
    if (x->parent == tree->nil)
        tree->root = y;
    else {
        if(x->parent->left == x)
            x->parent->left = y;
        else
            x->parent->right = y;
    }
    y->right = x;
    if(x != tree->nil)
        x->parent = y;
}

static void rbtreec_insert_fixup (RBTreeC *tree, RBTreeCNode *z) {
    RBTreeCNode *y = NULL;
    while ((z->parent != NULL) && (z->parent->color == RED)) {
        if (z->parent == z->parent->parent->left) { 
            y = z->parent->parent->right; 
            if ((y != NULL) && (y->color == RED)) {
                z->parent->color = BLACK; 
                y->color = BLACK;
                z->parent->parent->color = RED; 
                z = z->parent->parent; 
            } else {
                if (z == z->parent->right) { 
                    z = z->parent;
                    rbtreec_left_rotate(tree, z);
                }
                z->parent->color = BLACK; 
                z->parent->parent->color = RED;
                rbtreec_right_rotate(tree, z->parent->parent);
            }        
        } else { 
            y = z->parent->parent->left;
            if ((y != NULL) && (y->color == RED)) {
                z->parent->color = BLACK;
                y->color = BLACK;
                z->parent->parent->color = RED;
                z = z->parent->parent;
            } else {
                if (z == z->parent->left) {
                    z = z->parent;
                    rbtreec_right_rotate(tree, z);
                }
                z->parent->color = BLACK;
                z->parent->parent->color = RED;
                rbtreec_left_rotate(tree, z->parent->parent);
            }
        }
    }
    tree->root->color = BLACK;
    tree->nil->color = BLACK;
}

static void rbtreec_insert (Luka *luka, RBTreeC *tree, const char *s, void *value) {
    int ret = 0;
    RBTreeCNode *x = tree->root, *y = tree->nil, *z;

    z = (RBTreeCNode *)luka_malloc(luka, sizeof(RBTreeCNode));
    z->color = RED;
    z->p = luka_strdup(luka, s);
    z->value = value;
    z->parent = NULL;
    z->left = NULL;
    z->right = NULL;
    while (x != tree->nil) {
        y = x;
        ret = strcmp(z->p, x->p);
        if (ret < 0)
            x = x->left;
        else if (ret > 0)
            x = x->right;
        else {
            luka_free(luka, z->p);
            luka_free(luka, z);
            x->value = value;
            return;
        }
    }
    z->parent = y;
    if (y != tree->nil) {
        ret = strcmp(z->p, y->p);
        if(ret > 0)
            y->right = z;
        else
            y->left = z;
    } else {
        tree->root = z; 
    }
    z->left = tree->nil;
    z->right = tree->nil;
    rbtreec_insert_fixup(tree, z);
}

static void rbtreec_delete_fixup (RBTreeC *tree, RBTreeCNode *x) {
    RBTreeCNode *w = NULL;

    while ((x != tree->root) && (x->color == BLACK)) {
        if (x == x->parent->left) {
            w = x->parent->right;
            if (w->color == RED) {
                w->color = BLACK;
                x->parent->color = RED;
                rbtreec_left_rotate(tree, x->parent);
                w = x->parent->right;
            }
            if (w->left->color == BLACK && w->right->color == BLACK) {
                w->color = RED;
                x = x->parent;
            } else {
                if(w->right->color == BLACK) {
                    w->left->color = BLACK;
                    w->color = RED;
                    rbtreec_right_rotate(tree, w);
                    w = x->parent->right;
                } 
                w->color = w->parent->color;
                x->parent->color = BLACK;
                w->right->color = BLACK;
                rbtreec_left_rotate(tree, x->parent);
                x = tree->root;
            }
        } else {
            w = x->parent->left;
            if (w->color == RED) {
                w->color = BLACK;
                x->parent->color = RED;
                rbtreec_right_rotate(tree, x->parent);
                w = x->parent->left;
            }
            if (w->right->color == BLACK && w->left->color == BLACK) {
                w->color = RED;
                x = x->parent;
            } else {
                if (w->left->color == BLACK) {
                    w->right->color = BLACK;
                    w->color = RED;
                    rbtreec_left_rotate(tree, w);
                    w = x->parent->left;
                }
                w->color = w->parent->color;
                x->parent->color = BLACK;
                w->left->color = BLACK;
                rbtreec_right_rotate(tree, x->parent);
                x = tree->root;
            }
        }
    }
    x->color = BLACK;
}

static void rbtreec_delete (Luka *luka, RBTreeC *tree, RBTreeCNode *z) {
    RBTreeCNode *y = NULL, *x = NULL;

    luka_free(luka, z->p);
    if (z->left == tree->nil || z->right == tree->nil) y=z;
    else {
        y = z->right;
        while (y->left != tree->nil) y = y->left;
    }
    if (y->left != tree->nil) x = y->left;
    else x = y->right;
    x->parent = y->parent;
    if (y->parent == tree->nil)
        tree->root = x;
    else {
        if (y->parent->left == y) y->parent->left = x;
        else y->parent->right = x;
    }
    if (y != z) z->p = y->p;
    if (y->color == BLACK) rbtreec_delete_fixup(tree, x);
    luka_free(luka, y);
}

static void rbtreec_put (Luka *luka, RBTreeC *tree, const char *s, void *value) {
    rbtreec_insert(luka, tree, s, value);
}

static void *rbtreec_get (Luka *luka, RBTreeC *tree, const char *s) {
    RBTreeCNode *node = rbtreec_search(tree, s);
    return node ? node->value : NULL;
}

static void rbtreec_rmv (Luka *luka, RBTreeC *tree, const char *s) {
    RBTreeCNode *node = rbtreec_search(tree, s);
    if (node) rbtreec_delete(luka, tree, node);
}

static void rbtreec_each_ex (Luka *luka, RBTreeC *tree, RBTreeCNode *node, void *k, void (*cb)(Luka *, void *k, const char *p, void *value)) {
    if (node == tree->nil)
        return;

    rbtreec_each_ex(luka, tree, node->left, k, cb);
    cb(luka, k, node->p, node->value);
    rbtreec_each_ex(luka, tree, node->right, k, cb);
}

static void rbtreec_each (Luka *luka, RBTreeC *tree, void *k, void (*cb)(Luka *, void *k, const char *p, void *value)) {
    rbtreec_each_ex(luka, tree, tree->root, k, cb);
}

static void rbtreec_destroy_ex (Luka *luka, RBTreeC *tree, RBTreeCNode *node) {
    if (node == tree->nil)
        return;

    luka_free(luka, node->p);
    rbtreec_destroy_ex(luka, tree, node->left);
    rbtreec_destroy_ex(luka, tree, node->right);
    luka_free(luka, node);
}

void rbtreec_destroy (Luka *luka, RBTreeC *tree) {
    rbtreec_destroy_ex(luka, tree, tree->root);
    luka_free(luka, tree->nil);
    luka_free(luka, tree);
}

/***********************************************************
  LukaStack 
***********************************************************/

LukaStack *luka_stack_create (Luka *luka) {
    return (LukaStack *)luka_malloc(luka, sizeof(LukaStack));
}

void luka_stack_push (Luka *luka, LukaStack *stack, void *p) {
    LukaStackNode *node = (LukaStackNode *)luka_malloc(luka, sizeof(LukaStackNode));
    node->p = p;
    node->next = stack->head;
    stack->head = node;
}

void *luka_stack_top (Luka *luka, LukaStack *stack) {
    return stack->head ? stack->head->p : NULL;
}

void luka_stack_pop (Luka *luka, LukaStack *stack) {
    LukaStackNode *node = stack->head;
    stack->head = node->next;
    luka_free(luka, node);
}

int luka_stack_exist (Luka *luka, LukaStack *stack, void *p) {
	LukaStackNode *mov = stack->head;
	while (mov) {
		if (mov->p == p) return 1;
		mov = mov->next;
	}
	return 0;
}

void luka_stack_destroy (Luka *luka, LukaStack *stack) {
    LukaStackNode *mov = NULL, *buf = NULL;

    mov = stack->head;
    while (mov) {
        buf = mov;
        mov = mov->next;
        luka_free(luka, buf);
    }
    luka_free(luka, stack);
}

/***********************************************************
  LukaObject 
***********************************************************/

typedef struct LukaObjectRouter {
    LukaObject *object;
    void (*cb)(Luka *, LukaObject *, void *, const char *, voidp);
    void *p;
} LukaObjectRouter;

LukaObject *luka_object_create (Luka *luka) {
	LukaObject *obj = NULL;

	obj = (LukaObject *)luka_malloc(luka, sizeof(LukaObject));
	obj->object = rbtreec_create(luka);
	return obj;
}

void luka_object_setcb (Luka *luka, LukaObject *object, void (*in_proc)(Luka *luka, voidp p), void (*out_proc)(Luka *luka, voidp p)) {
	object->in_proc = in_proc;
	object->out_proc = out_proc;
}

void luka_object_put (Luka *luka, LukaObject *object, const char *key, voidp p) {
	voidp data = NULL;

	if (!key || *key == 0)
		return;

	if (object->out_proc) {
		data = luka_object_get(luka, object, key);
		if (data)
			object->out_proc(luka, data);
	}

	rbtreec_put(luka, object->object, key, p);

	if (object->in_proc) {
		object->in_proc(luka, p);
	}
}

voidp luka_object_get (Luka *luka, LukaObject *object, const char *key) {
	if (!key || *key == 0)
		return NULL;

	return rbtreec_get(luka, object->object, key);
}

void luka_object_rmv (Luka *luka, LukaObject *object, const char *key) {
	voidp data = NULL;

	if (!key || *key == 0)
		return;

	data = luka_object_get(luka, object, key);
	if (!data)
		return;

	rbtreec_rmv(luka, object->object, key);

	if (object->out_proc) {
		object->out_proc(luka, data);
	}
}

static void luka_object_each_proc (Luka *luka, void *k, const char *p, void *value) {
    LukaObjectRouter *router = (LukaObjectRouter *)k;
    router->cb(luka, router->object, router->p, p, value);
}

void luka_object_each (Luka *luka, LukaObject *object, void *p, void (*cb)(Luka *, LukaObject *, void *, const char *, voidp)) {
    LukaObjectRouter router = {object, cb, p};
	rbtreec_each(luka, object->object, (void *)&router, luka_object_each_proc);
}

static void luka_object_destroy_proc (Luka *luka, void *k, const char *p, void *value) {
	LukaObject *object = (LukaObject *)k;

	if (object->out_proc) {
		object->out_proc(luka, value);
	}
}

void luka_object_destroy (Luka *luka, LukaObject *object) {
	rbtreec_each(luka, object->object, object, luka_object_destroy_proc);
	rbtreec_destroy(luka, object->object);
	luka_free(luka, object);
}

/***********************************************************
  LukaArray 
***********************************************************/

LukaArray *luka_array_create (Luka *luka) {
	LukaArray *array = NULL;

	array = (LukaArray *)luka_malloc(luka, sizeof(LukaArray));
	array->array = rbtreec_create(luka);
	return array;
}

void luka_array_setcb (Luka *luka, LukaArray *array, void (*in_proc)(Luka *luka, voidp p), void (*out_proc)(Luka *luka, voidp p)) {
	array->in_proc = in_proc;
	array->out_proc = out_proc;
}

void luka_array_push (Luka *luka, LukaArray *array, voidp p) {
	char temp[12] = {0};

	if (array->length == LUKA_ARRAY_MAX)
		return;

	sprintf(temp, "%d", array->length);
	rbtreec_put(luka, array->array, temp, p);

	if (array->in_proc) {
		array->in_proc(luka, p);
	}

	array->length++;
}

void luka_array_put (Luka *luka, LukaArray *array, size_t i, voidp p) {
	size_t j = 0;
	voidp pold = NULL;

	if (i >= LUKA_ARRAY_MAX)
		return;

	if ((pold = luka_array_get(luka, array, i)) != NULL) {
        char temp[12] = {0};
        sprintf(temp, "%d", i);
        rbtreec_put(luka, array->array, temp, p);
        if (array->out_proc)
            array->out_proc(luka, pold);
        return;
	}

	for (j = array->length; j < i; j++) {
		luka_array_push(luka, array, luka_null(luka));
	}

	luka_array_push(luka, array, p);
}

size_t luka_array_length (Luka *luka, LukaArray *array) {
	return array->length;
}

voidp luka_array_get (Luka *luka, LukaArray *array, size_t i) {
	char temp[12] = {0};
	void *p = NULL;

	if (i >= array->length)
		return NULL;

	sprintf(temp, "%d", i);
	p = rbtreec_get(luka, array->array, temp);
	return p;
}

static void luka_array_destroy_proc (Luka *luka, void *k, const char *p, void *value) {
	LukaArray *array = (LukaArray *)k;

	if (array->out_proc) {
		array->out_proc(luka, value);
	}
}

void luka_array_destroy (Luka *luka, LukaArray *array) {
	rbtreec_each(luka, array->array, array, luka_array_destroy_proc);
	rbtreec_destroy(luka, array->array);
	luka_free(luka, array);
}

/***********************************************************
  LukaStringList 
***********************************************************/

static void luka_string_list_push (Luka *luka, LukaStringList **list, char *s) {
    LukaStringList *node = NULL, *mov = NULL;

    node = (LukaStringList *)luka_malloc(luka, sizeof(LukaStringList));
    node->s = s;
    node->next = NULL;
    if (*list == NULL) {
        *list = node;
    } else {
        mov = *list;
        while (mov->next) mov = mov->next;
        mov->next = node;
    }
}

static int luka_string_list_exist (Luka *luka, LukaStringList *list, const char *s) {
    LukaStringList *mov = list;
    while (mov) {
        if (strcmp(mov->s, s) == 0) return 1;
        mov = mov->next;
    }
    return 0;
}

/***********************************************************
  内存管理 
***********************************************************/

void luka_exception (Luka *luka, int code) {
    longjmp(luka->jump, code);
}

void *luka_malloc (Luka *luka, size_t n) {
    void *p = NULL;

    if (n == 0) n = 1;
    p = rbtreep_alloc(luka->memery, n);
    if (!p) luka_exception(luka, LUKA_OOM);
    return p;
}

char *luka_strdup (Luka *luka, const char *s) {
    size_t n = strlen(s);
    char *r = luka_malloc(luka, n + 1);
    memcpy(r, s, n);
    return r;
}

void luka_free (Luka *luka, void *p) {
    rbtreep_free(luka->memery, p);
}

/***********************************************************
  内部函数声明 
***********************************************************/

static int          _isname                 (const char *s);

static void         luka_parse_main         (Luka *luka, const char *full_path);
static void         luka_package            (Luka *luka, const char *package);
static void         luka_parse_express_main (Luka *luka);
static void         luka_parse_check_main   (Luka *luka);
static void         luka_parse_carding_main (Luka *luka);

static void         luka_luka_regs          (Luka *luka);
static LukaExpress *luka_express_parse      (Luka *luka, const char *s);
static int          luka_express_end        (const char *s);
static int          luka_express_oper       (Luka *luka, const char **s);

static void         luka_data_down          (Luka *luka, voidp p);
static void         luka_data_free          (Luka *luka, LukaData *data);

/***********************************************************
  Luka 
***********************************************************/

static void luka_create (Luka *luka) {
    luka->memery = rbtreep_create();
    if (!luka->memery) {
        luka_exception(luka, LUKA_OOM);
    }

    //null,true,false数据
    luka->ntf[0] = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    luka->ntf[0]->type = LUKA_NULL;
    luka->ntf[1] = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    luka->ntf[1]->type = LUKA_TRUE;
    luka->ntf[2] = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    luka->ntf[2]->type = LUKA_FALSE;

    //初始化cs/funcs
    luka->cs = luka_object_create(luka);
    luka->funcs = luka_object_create(luka);

    //载入luka/c相关函数
    luka_package(luka, "luka");
    luka_package(luka, "c");
    luka_package(luka, "c2");

	//初始化queue
	luka->queue = luka_stack_create(luka);
}

static void luka_destroy (Luka *luka) {
    if (luka->memery) {
        rbtreep_destroy(luka->memery);
        luka->memery = NULL;
    }
}

/** 注册C语言函数 **/
void luka_reg (Luka *luka, const char *func_name, voidp (*func_p)(Luka *, voidp *, size_t)) {
    if (!func_name || *func_name == 0 || !func_p || !_isname(func_name))
        luka_exception(luka, LUKA_CFUNC_ERR);

    if (luka_object_get(luka, luka->cs, func_name)) {
        luka_exception(luka, LUKA_CFUNC_ERR);
    }

    luka_object_put(luka, luka->cs, func_name, func_p);
}

/** 注册系统变量 **/
void luka_reg_var (Luka *luka, const char *name, voidp p) {
    luka_object_put(luka, luka->mainvars, name, p);
    luka_data_up(luka, p);
}

/** 表达式数据存入队列 **/
static void luka_queue_add (Luka *luka, voidp p) {
	if (p == luka_null(luka) || p == luka_true(luka) || p == luka_false(luka))
		return;

	if (!luka_stack_exist(luka, luka->queue, p)) {
		luka_stack_push(luka, luka->queue, p);
	}
}

/** 释放无效数据 **/
static void luka_queue_check (Luka *luka) {
	LukaStackNode *buf = NULL;
	LukaData *data = NULL;

	while ((buf = luka_stack_top(luka, luka->queue)) != NULL) {
		data = (LukaData *)buf;
		luka_stack_pop(luka, luka->queue);

		if (data->index == 0) {
			luka_data_free(luka, data);
		}
	}
}

/***********************************************************
  LukaData 
***********************************************************/

voidp luka_null (Luka*luka) {
    return (voidp)luka->ntf[0];
}

voidp luka_true (Luka*luka) {
    return (voidp)luka->ntf[1];
}

voidp luka_false (Luka*luka) {
    return (voidp)luka->ntf[2];
}

voidp luka_put_int (Luka *luka, long i) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_INT;
    data->data_i = i;
    return (voidp)data;
}

voidp luka_put_double (Luka *luka, double d) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_DOUBLE;
    data->data_d = d;
    return (voidp)data;
}

voidp luka_put_string (Luka *luka, char *s) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_STRING;
    data->data_str = s;
    return data;
}

voidp luka_put_byte (Luka *luka, unsigned char *datap, size_t size) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_BYTE;
    data->data_byte.data = datap;
    data->data_byte.size = size;
    return data;
}

voidp luka_put_object (Luka *luka) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_OBJECT;
    data->data_object = luka_object_create(luka);
    luka_object_setcb(luka, data->data_object, NULL, luka_data_down);
    return data;
}

voidp luka_put_array (Luka *luka) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_ARRAY;
    data->data_array = luka_array_create(luka);
    luka_array_setcb(luka, data->data_array, NULL, luka_data_down);
    return data;
}

voidp luka_put_voidp (Luka *luka, void *p) {
    LukaData *data = (LukaData *)luka_malloc(luka, sizeof(LukaData));
    data->type = LUKA_VOIDP;
    data->data_p = p;
    return data;
}

/*********************************************************************/

long luka_get_int (Luka *luka, voidp p) {
    LukaData *pi = (LukaData *)p;
    if (pi->type == LUKA_INT) return pi->data_i;
    if (pi->type == LUKA_DOUBLE) return (long)pi->data_d;
    return 0;
}

double luka_get_double (Luka *luka, voidp p) {
    LukaData *pi = (LukaData *)p;
    if (pi->type == LUKA_DOUBLE) return pi->data_d;
    if (pi->type == LUKA_INT) return (double)pi->data_i;
    return 0;
}

const char *luka_get_string (Luka *luka, voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_STRING ? pi->data_str : "";
}

unsigned char *luka_get_byte (Luka *luka, voidp p, size_t *size) {
	LukaData *pi = (LukaData *)p;
	if (pi->type != LUKA_BYTE) return NULL;

    if (size)
	    *size = pi->data_byte.size;
	return pi->data_byte.data;
}

LukaObject *luka_get_object (Luka *luka, voidp p) {
	LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_OBJECT ? pi->data_object : NULL;
}

LukaArray *luka_get_array (Luka *luka, voidp p) {
	LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_ARRAY ? pi->data_array : NULL;
}

void *luka_get_voidp (Luka *luka, voidp p) {
	LukaData *pi = (LukaData *)p;
	return pi->type == LUKA_VOIDP ? pi->data_p : NULL;
}

/*********************************************************************/

int luka_is_null (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_NULL;
}

int luka_is_true (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_TRUE;
}

int luka_is_false (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_FALSE;
}

int luka_is_int (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_INT;
}

int luka_is_double (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_DOUBLE;
}

int luka_is_string (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_STRING;
}

int luka_is_byte (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_BYTE;
}

int luka_is_object (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_OBJECT;
}

int luka_is_array (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_ARRAY;
}

int luka_is_voidp (voidp p) {
    LukaData *pi = (LukaData *)p;
    return pi->type == LUKA_VOIDP;
}

/*********************************************************************/

void luka_data_up (Luka *luka, voidp p) {
	LukaData *pi = (LukaData *)p;

    if (p == luka_null(luka) || p == luka_true(luka) || p == luka_false(luka))
        return;

	pi->index++;
}

static void luka_data_down (Luka *luka, voidp p) {
	LukaData *pi = (LukaData *)p;

    if (p == luka_null(luka) || p == luka_true(luka) || p == luka_false(luka))
        return;

	pi->index--;

    if (pi->index <= 0) {
        luka_queue_add(luka, p);
    }
}

static void luka_data_free (Luka *luka, LukaData *data) {
	if (data->type == LUKA_STRING) {
		luka_free(luka, data->data_str);
	} else if (data->type == LUKA_BYTE) {
        if (data->data_byte.data)
    		luka_free(luka, data->data_byte.data);
	} else if (data->type == LUKA_OBJECT) {
		luka_object_destroy(luka, data->data_object);
	} else if (data->type == LUKA_ARRAY) {
		luka_array_destroy(luka, data->data_array);
	}
	luka_free(luka, data);
}

/***********************************************************
  LukaFunc 
***********************************************************/

/** 添加luka函数 **/
static void luka_func_add (Luka *luka, const char *func_name, char **func_param, size_t func_len) {
    LukaFunc *func = NULL;

    if (luka_object_get(luka, luka->cs, func_name) || luka_object_get(luka, luka->funcs, func_name)) {
        fprintf(stderr, "luka_func_add failed, function '%s' exist\n", func_name);
        luka_exception(luka, LUKA_PARSE_ERR);
    }

    func = (LukaFunc *)luka_malloc(luka, sizeof(LukaFunc));
    func->func_param = func_param;
    func->func_len = func_len;
    luka_object_put(luka, luka->funcs, func_name, func);
}

/** 添加'一句'代码 **/
static void luka_func_push (Luka *luka, LukaFunc *func, int type, LukaExpress *express, LukaExpress *a, LukaExpress *b, LukaExpress *c) {
    LukaCode *code = NULL;

    code = (LukaCode *)luka_malloc(luka, sizeof(LukaCode));
    code->type = type;
    code->express = express;
    code->a = a;
    code->b = b;
    code->c = c;
    if (func->codes_head == NULL) {
        func->codes_head = func->codes_tail = code;
    } else {
        func->codes_tail->next = code;
        code->last = func->codes_tail;
        func->codes_tail = code;
    }
}

/***********************************************************
  内部函数封装(解析语句时用) 
***********************************************************/

/** 目录符 **/
static char _get_dir_c () {
#ifdef _WIN32
    return '\\';
#else
    return '/';
#endif
}

/** 从s中剔除c **/
static void _str_rmv_c (char *s, char c) {
    char *p = s;
    while (*p != '\0') {
        if (*p != c) *s++ = *p;
        p++;
    }
    *s = '\0';
}

/** 剔除一部分字符串 **/
static void _str_rmv (char *s, size_t start, size_t len) {
	size_t s_len = 0;

	if (!s || *s == 0)
		return;

	s_len = strlen(s);
	if (start > s_len || len == 0)
		return;

	s_len = strlen(s + start + len);
	len = len > s_len ? s_len : len;

	memset(s + start, 0, len);
    memmove(s + start, s + start + len, s_len + 1);
}

/** 移除./和../ **/
static void _clear_path (char *full_path) {
	char *p = NULL, *p2 = NULL;

	while ((p = strstr(full_path, "../")) != NULL || (p = strstr(full_path, "..\\")) != NULL) {
		p2 = p - 2;  //退回2个字符
		while (p2 != full_path && *p2 != '/' && *p2 != '\\')
			p2--;

		if (p2 == full_path)
			return;

		_str_rmv(p2, 1, p + 2 - p2);
	}

	while ((p = strstr(full_path, "./")) != NULL || (p = strstr(full_path, ".\\")) != NULL) {
		_str_rmv(p, 0, 2);
	}
}

/** 剔除前后制表符 **/
static char *_trim (char *s) {
    char *p = s + strlen(s) - 1;
    while (p >= s && *p <= 32)
        *p-- = '\0';
    p = s;

    while (*p <= 32 && *p != 0)
        p++;
    if (p != s)
        memmove(s, p, strlen(p) + 1);
    return s;
}

/** 读取脚本文件 **/
static char *_read_script_file (Luka *luka, const char *path) {
    FILE *fp = NULL;
    char *data = NULL;
    long  size = 0;

    if ((fp = fopen(path, "rb")) == NULL) {
        fprintf(stderr, "can't open '%s'\n", path);
        luka_exception(luka, LUKA_FILE_ERR);
    }

    fseek(fp, 0, SEEK_END);
    size = ftell(fp);
    fclose(fp);

    if (size < 0) {
        fprintf(stderr, "file size error %ld\n", size);
        luka_exception(luka, LUKA_FILE_ERR);
    }

    if ((fp = fopen(path, "rb")) == NULL) {
        fprintf(stderr, "can't open '%s'\n", path);
        luka_exception(luka, LUKA_FILE_ERR);
    }

    data = (char *)luka_malloc(luka, size + 10);
    fread(data, 1, size, fp);
    fclose(fp);
    return data;
}

/** 获得之前连续几个\的数量 **/
static size_t _count_trans_num (const char *s) {
    size_t count = 0;
    while (*s == '\\') {
        count++; s--;
    }
    return count;
}

/** 扫描至c(需处理字符串转义问题) **/
static const char *_strstr_code (const char *s, char c) {
    char instr = 0;
    size_t left = 0;

    while (*s != 0) {
        if (instr == 0) {
            if (*s == '\'' || *s == '\"') {
                instr = *s;
            } else if (*s == c && left == 0)
                return s;

            if (*s == '(') left++;
            else if (*s == ')') left--;
        } else {
            if (*s == instr && _count_trans_num(s - 1) % 2 == 0) {
                instr = 0;
            }
        }
        s++;
    }
    return NULL;
}

static char *_strdup_code (Luka *luka, const char *left, const char *right) {
    size_t len = 0;
    char  *ret = NULL;

    len = right - left + 1;
    ret = (char *)luka_malloc(luka, len + 10);
    memcpy(ret, left, len);
    return ret;
}

static void _link_check (const char *s, size_t *line) {
    while (*s != '\0') {
        if (*s == '\n')
            *line = (*line) + 1;
        s++;
    }
}

static char **_splite_code (Luka *luka, const char *s, char c, size_t *n) {
    const char *left = s, *right = NULL;
    char **ret = NULL;
    size_t i = 0, number = 1;

    while ((right = _strstr_code(left, c)) != NULL) {
        number++;
        left = right + 1;
    }

    ret = (char **)luka_malloc(luka, sizeof(char *) * number);
    left = s;
    right = NULL;
    while ((right = _strstr_code(left, c)) != NULL) {
        if (left == right) {
            ret[i++] = luka_strdup(luka, "");
        } else {
            ret[i++] = _trim(_strdup_code(luka, left, right - 1));
        }
        left = right + 1;
    }

    ret[i++] = _trim(luka_strdup(luka, left));
    *n = number;
    return ret;
}

static const char *g_Name = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_";
static const char *g_Num  = "0123456789";
static const char *g_Sys[] = {
    "include", "package", "if", "else", "while", "for", "function", "return", "continue", "break", LUKA_MAIN, LUKA_MAIN2, NULL
};

/** 是否为合法的变量名/函数名(注:luka_main也包含在此函数里) **/
static int _isname (const char *s) {
    size_t i = 0;

    if (!strchr(g_Name, *s) || strchr(g_Num, *s))
        return 0;
    s++;

    while (*s != 0) {
        if (!strchr(g_Name, *s))
            return 0;
        s++;
    }

    //检查关键字
    while (g_Sys[i]) {
        if (strcmp(g_Sys[i++], s) == 0) return 0;
    }
    return 1;
}

static const char *g_Oper[] = {
    "->", "[", "]", "(", ")", "/", "*", "%", "+", "-", ">=", ">", "<=", "<", 
    "==", "!=", "&&", "||", "=", "/=", "*=", "%=" , "+=", "-=", "++", "--"
};

/** 获得oper字符 **/
static const char *_get_oper (int type) {
    return g_Oper[type - 600];
}

static const int g_Oper_Level[] = {
    1, 1, 1, 1, 1, 3, 3, 3, 4, 4, 5, 5, 5, 5, 
    6, 6, 7, 8, 9, 9, 9, 9, 9, 9, 2, 2
};

/** 获得oper优先级 **/
static int _get_oper_level (int type) {
    return g_Oper_Level[type - 600];
}

/** 处理转义 **/
static void _trans_str (char *s) {
    char *p = s;
    while (*p != '\0') {
        if (*p == '\\') {
            *p = 1;
            if (*(p + 1) == 'n') *(p + 1) = '\n';
            else if (*(p + 1) == 'r') *(p + 1) = '\r';
            else if (*(p + 1) == 't') *(p + 1) = '\t';
            p++;
        }
        p++;
    }
    _str_rmv_c(s, 1);
}

/***********************************************************
  解析部分 
***********************************************************/

/** 解析 **/
static void luka_parse (Luka *luka, const char *luka_script_path) {
    char *full_path = NULL;
    char pwd_path[LUKA_PATH_MAX] = {0};
    size_t pwd_len = 0, name_len = 0;

#ifdef _WIN32
    if ((toupper(*luka_script_path) >= 'A' && toupper(*luka_script_path) <= 'Z') && *(luka_script_path + 1) == ':') {
#else
    if (*luka_script_path == '/') {
#endif
        full_path = luka_strdup(luka, luka_script_path);
    } else {
        getcwd(pwd_path, sizeof(pwd_path) - 1);
        pwd_len = strlen(pwd_path);
        name_len = strlen(luka_script_path);
        full_path = (char *)luka_malloc(luka, pwd_len + name_len + 10);
        sprintf(full_path, "%s%c%s", pwd_path, _get_dir_c(), luka_script_path);
    }
    _clear_path(full_path);
    luka->mainpath = luka_strdup(luka, full_path);

    //1. 初步解析,检查语法错误
    luka_parse_main(luka, full_path);

    //2. 添加虚拟主函数
    luka_func_add(luka, LUKA_MAIN, NULL, 0);

    //3. '编译'开始,解析所有表达式
    luka_parse_express_main(luka);

    //4. 梳理代码
    luka_parse_carding_main(luka);
}

/** 初步解析(一大坨) **/
static void luka_parse_main (Luka *luka, const char *full_path) {
    char *text = NULL, *p = NULL;
    size_t line = 1;
    int state = LUKA_GCC_NULL;
    const char *left = NULL, *right = NULL;
    char *express_str = NULL, *include_path = NULL;
    LukaStaMen *stamen = NULL;

    //检查文件扩展名
    if ((left = strrchr(full_path, '.')) == NULL || strcmp(left, ".luka") != 0) {
        luka_exception(luka, LUKA_FILE_ERR);
    }
    left = NULL;

    //检查是否重复include
    if (luka_string_list_exist(luka, luka->files, full_path)) {
        luka_exception(luka, LUKA_INCLUDE_ERR);
    }
    luka_string_list_push(luka, &luka->files, luka_strdup(luka, full_path));

    p = text = _read_script_file(luka, full_path);
    while (*p != 0) {
        //空闲状态
        if (state == LUKA_GCC_NULL) {
            //跳过制表符
            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }
            if (*p == 0) break;

            //单行注释
            if (*p == '#') {
                state = LUKA_GCC_NOTE1;
                p++;
            }

            //单行注释
            else if (strncmp(p, "//", 2) == 0) {
                state = LUKA_GCC_NOTE1;
                p += 2;
            }

            //多行注释
            else if (strncmp(p, "/*", 2) == 0) {
                state = LUKA_GCC_NOTE2;
                p += 2;
            }

            //include
            else if (strncmp(p, "include", 7) == 0 && (*(p + 7) == '(' || *(p + 7) <= 32)) {
                state = LUKA_GCC_INCLUDE;
                p += 7;
            }

            //package
            else if (strncmp(p, "package", 7) == 0 && (*(p + 7) == '(' || *(p + 7) <= 32)) {
                state = LUKA_GCC_PACKAGE;
                p += 7;
            }

            //if
            else if (strncmp(p, "if", 2) == 0 && (*(p + 2) == '(' || *(p + 2) <= 32)) {
                state = LUKA_GCC_IF;
                p += 2;
            }

            //else
            else if (strncmp(p, "else", 4) == 0 && (*(p + 4) == '(' || *(p + 4) <= 32)) {
                state = LUKA_GCC_ELSE;
                p += 4;
            }

            //while
            else if (strncmp(p, "while", 5) == 0 && (*(p + 5) == '(' || *(p + 5) <= 32)) {
                state = LUKA_GCC_WHILE;
                p += 5;
            }

            //for
            else if (strncmp(p, "for", 3) == 0 && (*(p + 3) == '(' || *(p + 3) <= 32)) {
                state = LUKA_GCC_FOR;
                p += 3;
            }

            //function
            else if (strncmp(p, "function", 8) == 0 && (*(p + 8) == '(' || *(p + 8) <= 32)) {
                state = LUKA_GCC_FUNC;
                p += 8;
            }

            //return
            else if (strncmp(p, "return", 6) == 0 && (*(p + 6) == ';' || *(p + 6) <= 32)) {
                state = LUKA_GCC_RETURN;
                p += 6;
            }

            //break
            else if (strncmp(p, "break", 5) == 0 && (*(p + 5) == ';' || *(p + 5) <= 32)) {
                p += 5;
                
                while (*p <= 32 && *p != 0) {
                    if (*p == '\n') line++;
                    p++;
                }

                if (*p != ';') {
                    fprintf(stderr, "can't parse %s:%d, break error\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                } else {
                    p++;

                    //添加break语句
                    stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                    stamen->type = LUKA_GCC_BREAK;
                    stamen->file = full_path;
                    stamen->line = line;
                    if (luka->stame_end) {
                        luka->stame_end->next = stamen;
                        luka->stame_end = stamen;
                    } else {
                        luka->stame = luka->stame_end = stamen;
                    }
                    stamen = NULL;
                }
            }

            //continue
            else if (strncmp(p, "continue", 8) == 0 && (*(p + 8) == ';' || *(p + 8) <= 32)) {
                p += 8;
                
                while (*p <= 32 && *p != 0) {
                    if (*p == '\n') line++;
                    p++;
                }

                if (*p != ';') {
                    fprintf(stderr, "can't parse %s:%d, break error\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                } else {
                    p++;

                    //添加continue语句
                    stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                    stamen->type = LUKA_GCC_CONTINUE;
                    stamen->file = full_path;
                    stamen->line = line;
                    if (luka->stame_end) {
                        luka->stame_end->next = stamen;
                        luka->stame_end = stamen;
                    } else {
                        luka->stame = luka->stame_end = stamen;
                    }
                    stamen = NULL;
                }
            }

            //null express
            else if (*p == ';') {
                p++;
            }

            //}
            else if (*p == '}') {
                p++;

                //添加end语句
                stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                stamen->type = LUKA_GCC_END;
                stamen->file = full_path;
                stamen->line = line;
                if (luka->stame_end) {
                    luka->stame_end->next = stamen;
                    luka->stame_end = stamen;
                } else {
                    luka->stame = luka->stame_end = stamen;
                }
                stamen = NULL;
            }

            //普通表达式
            else {
                left = p;
                right = _strstr_code(left, ';');
                if (!right) {
                    fprintf(stderr, "[express] can't parse %s:%d '%.12s...'\n", full_path, line, p);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }

                p = (char *)right + 1;
                express_str = _strdup_code(luka, left, right - 1);
                _link_check(express_str, &line);
                _trim(express_str);
                left = right = NULL;
                state = LUKA_GCC_EXPRESS;
            }
        }

        //单行注释
        else if (state == LUKA_GCC_NOTE1) {
            if (*p == '\0') state = LUKA_GCC_NULL;
            if (*p == '\n') {
                line++;
                state = LUKA_GCC_NULL;
            }
            p++;
        }

        //多行注释
        else if (state == LUKA_GCC_NOTE2) {
            if (*p == '\0')
                state = LUKA_GCC_NULL;
            if (*p == '\n')
                line++;
            if (strncmp(p, "*/", 2) == 0) {
                p += 2;
                state = LUKA_GCC_NULL;
            } else {
                p++;
            }
        }

        //include
        else if (state == LUKA_GCC_INCLUDE) {
            const char *full_path_p = NULL;

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '(') {
                fprintf(stderr, "include error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            left = p;
            right = _strstr_code(p, ')');
            if (!right || left == right) {
                fprintf(stderr, "include error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;
            state = LUKA_GCC_NULL;

            if (strlen(express_str) == 0) {
                fprintf(stderr, "include error '%s':%d, errcode 3\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != ';') {
                fprintf(stderr, "include error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            //include路径组合
            full_path_p = strrchr(full_path, '\\');
            if (full_path_p == NULL) full_path_p = strrchr(full_path, '/');
            include_path = (char *)luka_malloc(luka, strlen(full_path) + strlen(express_str) + 10);
            memcpy(include_path, full_path, full_path_p - full_path);
            sprintf(include_path + (full_path_p - full_path), "%c%s", _get_dir_c(), express_str);
            luka_free(luka, express_str);
            express_str = NULL;

            //include
            luka_parse_main(luka, include_path);
            include_path = NULL;
        }

        //package
        else if (state == LUKA_GCC_PACKAGE) {
            const char *full_path_p = NULL;

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '(') {
                fprintf(stderr, "include error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            left = p;
            right = _strstr_code(p, ')');
            if (!right || left == right) {
                fprintf(stderr, "include error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;
            state = LUKA_GCC_NULL;

            if (strlen(express_str) == 0) {
                fprintf(stderr, "include error '%s':%d, errcode 3\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != ';') {
                fprintf(stderr, "include error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            //处理package
            luka_package(luka, express_str);
            express_str = NULL;
        }

        //if
        else if (state == LUKA_GCC_IF) {
            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '(') {
                fprintf(stderr, "if error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            left = p;
            right = _strstr_code(p, ')');
            if (!right || left == right) {
                fprintf(stderr, "if error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;
            state = LUKA_GCC_NULL;

            if (strlen(express_str) == 0) {
                fprintf(stderr, "if error '%s':%d, errcode 3\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '{') {
                fprintf(stderr, "if error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            //添加一个if语句
            stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
            stamen->type = LUKA_GCC_IF;
            stamen->express = express_str;
            stamen->file = full_path;
            stamen->line = line;
            if (luka->stame_end) {
                luka->stame_end->next = stamen;
                luka->stame_end = stamen;
            } else {
                luka->stame = luka->stame_end = stamen;
            }
            stamen = NULL;
        }

        //else
        else if (state == LUKA_GCC_ELSE) {
            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            //else
            if (*p == '{') {
                p++;

                //添加一个else语句
                stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                stamen->type = LUKA_GCC_ELSE;
                stamen->file = full_path;
                stamen->line = line;
                if (luka->stame_end) {
                    luka->stame_end->next = stamen;
                    luka->stame_end = stamen;
                } else {
                    luka->stame = luka->stame_end = stamen;
                }
                stamen = NULL;
                state = LUKA_GCC_NULL;
            }

            //else if
            else if (strncmp(p, "if", 2) == 0) {
                p += 2;

                while (*p <= 32 && *p != 0) {
                    if (*p++ == '\n') line++;
                }

                if (*p++ != '(') {
                    fprintf(stderr, "else if error '%s':%d, errcode 1\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }

                left = p;
                right = _strstr_code(p, ')');
                if (!right || left == right) {
                    fprintf(stderr, "else if error '%s':%d, errcode 2\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }
                p = (char *)right + 1;

                express_str = _strdup_code(luka, left, right - 1);
                _link_check(express_str, &line);
                _trim(express_str);
                left = right = NULL;
                state = LUKA_GCC_NULL;

                if (strlen(express_str) == 0) {
                    fprintf(stderr, "else if error '%s':%d, errcode 3\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }

                while (*p <= 32 && *p != 0) {
                    if (*p++ == '\n') line++;
                }

                if (*p++ != '{') {
                    fprintf(stderr, "else if error '%s':%d, errcode 4\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }

                //添加一个else if 语句
                stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                stamen->type = LUKA_GCC_ELSEIF;
                stamen->express = express_str;
                stamen->file = full_path;
                stamen->line = line;
                if (luka->stame_end) {
                    luka->stame_end->next = stamen;
                    luka->stame_end = stamen;
                } else {
                    luka->stame = luka->stame_end = stamen;
                }
                stamen = NULL;
            }

            else {
                fprintf(stderr, "else error '%s':%d, errcode 5\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
        }

        //while
        else if (state == LUKA_GCC_WHILE) {
            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '(') {
                fprintf(stderr, "while error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            left = p;
            right = _strstr_code(p, ')');
            if (!right || left == right) {
                fprintf(stderr, "while error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;
            state = LUKA_GCC_NULL;

            if (strlen(express_str) == 0) {
                fprintf(stderr, "while error '%s':%d, errcode 3\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '{') {
                fprintf(stderr, "while error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            //添加一个while语句
            stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
            stamen->type = LUKA_GCC_WHILE;
            stamen->express = express_str;
            stamen->file = full_path;
            stamen->line = line;
            if (luka->stame_end) {
                luka->stame_end->next = stamen;
                luka->stame_end = stamen;
            } else {
                luka->stame = luka->stame_end = stamen;
            }
            stamen = NULL;
        }

        //for
        else if (state == LUKA_GCC_FOR) {
            char **for_ret = NULL;
            size_t for_len = 0;

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '(') {
                fprintf(stderr, "for error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            left = p;
            right = _strstr_code(p, ')');
            if (!right || left == right) {
                fprintf(stderr, "for error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;

            if (strlen(express_str) == 0) {
                fprintf(stderr, "for error '%s':%d, errcode 3\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '{') {
                fprintf(stderr, "for error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            for_ret = _splite_code(luka, express_str, ';', &for_len);
            if (for_len != 3) {
                fprintf(stderr, "for error '%s':%d, errcode 5\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            luka_free(luka, express_str);
            express_str = NULL;
            state = LUKA_GCC_NULL;

            //添加for语句
            stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
            stamen->type = LUKA_GCC_FOR;
            stamen->a = _trim(for_ret[0]);
            stamen->b = _trim(for_ret[1]);
            stamen->c = _trim(for_ret[2]);
            stamen->file = full_path;
            stamen->line = line;
            luka_free(luka, for_ret);
            for_ret = NULL;
            if (luka->stame_end) {
                luka->stame_end->next = stamen;
                luka->stame_end = stamen;
            } else {
                luka->stame = luka->stame_end = stamen;
            }
            stamen = NULL;
            state = LUKA_GCC_NULL;
        }

        //function
        else if (state == LUKA_GCC_FUNC) {
            char *func_name = NULL;
            char **func_param = NULL;
            size_t i = 0, func_len = 0;

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            left = p;
            right = _strstr_code(p, '(');
            if (!right || left == right) {
                fprintf(stderr, "function error '%s':%d, errcode 1\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            express_str = _strdup_code(luka, left, right - 1);
            _link_check(express_str, &line);
            _trim(express_str);
            left = right = NULL;

            //func_name
            func_name = express_str;
            express_str = NULL;

            left = p;
            right = _strstr_code(p, ')');
            if (!right) {
                fprintf(stderr, "function error '%s':%d, errcode 2\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            p = (char *)right + 1;

            if (left != right) {
                express_str = _strdup_code(luka, left, right - 1);
                _link_check(express_str, &line);
                _trim(express_str);
                left = right = NULL;

                if (strlen(express_str) != 0) {
                    func_param = _splite_code(luka, express_str, ',', &func_len);
                    for (i = 0; i < func_len; i++) {
                        if (!_isname(func_param[i])) {
                            fprintf(stderr, "function error '%s':%d, errcode 3\n", full_path, line);
                            luka_exception(luka, LUKA_PARSE_ERR);
                        }
                    }
                }

                luka_free(luka, express_str);
                express_str = NULL;
            }

            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p++ != '{') {
                fprintf(stderr, "function error '%s':%d, errcode 4\n", full_path, line);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            //添加一个函数
            stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
            stamen->type = LUKA_GCC_FUNC;
            stamen->func_name = func_name;
            stamen->func_param = func_param;
            stamen->func_len = func_len;
            stamen->file = full_path;
            stamen->line = line;
            if (luka->stame_end) {
                luka->stame_end->next = stamen;
                luka->stame_end = stamen;
            } else {
                luka->stame = luka->stame_end = stamen;
            }
            stamen = NULL;
            state = LUKA_GCC_NULL;
        }

        //return
        else if (state == LUKA_GCC_RETURN) {
            while (*p <= 32 && *p != 0) {
                if (*p++ == '\n') line++;
            }

            if (*p == ';') {
                //添加一个return;语句
                stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                stamen->type = LUKA_GCC_RETURN;
                stamen->express = luka_strdup(luka, "null");
                stamen->file = full_path;
                stamen->line = line;
                if (luka->stame_end) {
                    luka->stame_end->next = stamen;
                    luka->stame_end = stamen;
                } else {
                    luka->stame = luka->stame_end = stamen;
                }
                stamen = NULL;

                p++;
                express_str = NULL;
                state = LUKA_GCC_NULL;
            } else {
                left = p;
                right = _strstr_code(p, ';');
                if (!right) {
                    fprintf(stderr, "return error '%s':%d, errcode 1\n", full_path, line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }
                p = (char *)right + 1;

                express_str = _strdup_code(luka, left, right - 1);
                _link_check(express_str, &line);
                _trim(express_str);
                left = right = NULL;

                //添加一个return xxx;语句
                stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
                stamen->type = LUKA_GCC_RETURN;
                stamen->express = express_str;
                stamen->file = full_path;
                stamen->line = line;
                if (luka->stame_end) {
                    luka->stame_end->next = stamen;
                    luka->stame_end = stamen;
                } else {
                    luka->stame = luka->stame_end = stamen;
                }
                stamen = NULL;
                state = LUKA_GCC_NULL;
            }
        }

        //表达式
        else if (state == LUKA_GCC_EXPRESS) {
            //添加一个表达式
            stamen = (LukaStaMen *)luka_malloc(luka, sizeof(LukaStaMen));
            stamen->type = LUKA_GCC_EXPRESS;
            stamen->express = express_str;
            stamen->file = full_path;
            stamen->line = line;
            if (luka->stame_end) {
                luka->stame_end->next = stamen;
                luka->stame_end = stamen;
            } else {
                luka->stame = luka->stame_end = stamen;
            }
            stamen = NULL;
            express_str = NULL;
            state = LUKA_GCC_NULL;
        }

        else {
            fprintf(stderr, "can't parse %s:%d '%.12s...', errcode %d\n", full_path, line, p, state);
            luka_exception(luka, LUKA_PARSE_ERR);
        }
    }

    if (state != LUKA_GCC_NULL) {
        fprintf(stderr, "'%s' not end, '%d'\n", full_path, state);
        luka_exception(luka, LUKA_PARSE_ERR);
    }
}

/** 解析语句表达式 **/
static void luka_parse_express_main (Luka *luka) {
    LukaStaMen *mov = luka->stame;
    LukaFunc *luka_main = luka_object_get(luka, luka->funcs, LUKA_MAIN);
    LukaFunc *func_now = luka_main;
    const char *func_now_name = NULL;
    LukaExpress *express = NULL;

    int nesting = 0;
    
    while (mov) {
        //表达式
        if (mov->type == LUKA_GCC_EXPRESS) {
            express = luka_express_parse(luka, mov->express);
            if (express == NULL) {
                fprintf(stderr, "[LUKA_GCC_EXPRESS] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_push(luka, func_now, LUKA_EXPRESS, express, NULL, NULL, NULL);
        }

        //if
        else if (mov->type == LUKA_GCC_IF) {
            express = luka_express_parse(luka, mov->express);
            if (express == NULL) {
                fprintf(stderr, "[LUKA_GCC_IF] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_push(luka, func_now, LUKA_IF, express, NULL, NULL, NULL);
            nesting++;
        }

        //else if
        else if (mov->type == LUKA_GCC_ELSEIF) {
            express = luka_express_parse(luka, mov->express);
            if (express == NULL) {
                fprintf(stderr, "[LUKA_GCC_ELSEIF] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_push(luka, func_now, LUKA_ELSEIF, express, NULL, NULL, NULL);
            nesting++;
        }

        //else
        else if (mov->type == LUKA_GCC_ELSE) {
            luka_func_push(luka, func_now, LUKA_ELSE, express, NULL, NULL, NULL);
            nesting++;
        }

        //while
        else if (mov->type == LUKA_GCC_WHILE) {
            express = luka_express_parse(luka, mov->express);
            if (express == NULL) {
                fprintf(stderr, "[LUKA_GCC_WHILE] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_push(luka, func_now, LUKA_WHILE, express, NULL, NULL, NULL);
            nesting++;
        }

        //for
        else if (mov->type == LUKA_GCC_FOR) {
            LukaExpress *a = NULL, *b = NULL, *c = NULL;

            a = luka_express_parse(luka, mov->a);
            b = luka_express_parse(luka, mov->b);
            c = luka_express_parse(luka, mov->c);

			if ((*mov->a != 0 && !a) || (*mov->b != 0 && !b) || (*mov->c != 0 && !c)) {
                fprintf(stderr, "[LUKA_GCC_FOR] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
			}

            luka_func_push(luka, func_now, LUKA_FOR, NULL, a, b, c);
            nesting++;
        }

        //定义函数
        else if (mov->type == LUKA_GCC_FUNC) {
            if (nesting != 0 && func_now != luka_main) {
                fprintf(stderr, "[LUKA_GCC_FUNC] can't parse %s:%d, error define '%s'\n", mov->file, mov->line, mov->func_name);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_add(luka, mov->func_name, mov->func_param, mov->func_len);
            func_now = luka_object_get(luka, luka->funcs, mov->func_name);
            func_now_name = mov->func_name;
        }

        //}
        else if (mov->type == LUKA_GCC_END) {
            if (nesting == 0) {
                if (func_now != luka_main) {
                    func_now = luka_main;
                    func_now_name = NULL;
                } else {
                    fprintf(stderr, "[LUKA_GCC_END] can't parse %s:%d, error '}'\n", mov->file, mov->line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }
            } else {
                luka_func_push(luka, func_now, LUKA_END, NULL, NULL, NULL, NULL);
                nesting--;

                if (nesting < 0) {
                    fprintf(stderr, "[LUKA_GCC_END] can't parse %s:%d, error '}'\n", mov->file, mov->line);
                    luka_exception(luka, LUKA_PARSE_ERR);
                }
            }
        }

        //return
        else if (mov->type == LUKA_GCC_RETURN) {
            express = luka_express_parse(luka, mov->express);
            if (express == NULL) {
                fprintf(stderr, "[LUKA_GCC_RETURN] can't parse %s:%d '%.12s...'\n", mov->file, mov->line, mov->express);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            luka_func_push(luka, func_now, LUKA_RETURN, express, NULL, NULL, NULL);
        }

        //break
        else if (mov->type == LUKA_GCC_BREAK) {
            luka_func_push(luka, func_now, LUKA_BREAK, NULL, NULL, NULL, NULL);
        }

        //continue
        else if (mov->type == LUKA_GCC_CONTINUE) {
            luka_func_push(luka, func_now, LUKA_CONTINUE, NULL, NULL, NULL, NULL);
        }

        mov = mov->next;
    }

    //检查定义函数是否正常结束
    if (func_now != luka_main) {
        fprintf(stderr, "can't parse '%s' is not over\n", func_now_name);
        luka_exception(luka, LUKA_PARSE_ERR);
    }
}

/** 检查表达式中的函数调用 **/
static void luka_express_func_check (Luka *luka, LukaExpress *express) {
    size_t i = 0;
    LukaExpressNode *mov = express->RPN;

    while (mov) {
        if (mov->type == LUKA_EXP_FUNC) {
            if (strcmp(mov->data_func, LUKA_MAIN) == 0 || strcmp(mov->data_func, LUKA_MAIN2) == 0) {
                fprintf(stderr, "can't call '%s'\n", mov->data_func);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            if (!luka_object_get(luka, luka->cs, mov->data_func) && !luka_object_get(luka, luka->funcs, mov->data_func)) {
                fprintf(stderr, "'%s' not define\n", mov->data_func);
                luka_exception(luka, LUKA_PARSE_ERR);
            }

            if (mov->data_param) {
                for (i = 0; i < mov->data_len; i++) {
                    luka_express_func_check(luka, mov->data_param[i]);
                }
            }
        }

        mov = mov->next;
    }
}

static void luka_parse_carding_main_proc (Luka *luka, LukaObject *cs, void *p, const char *key, voidp value) {
    LukaFunc *func = (LukaFunc *)value;
    LukaCode *code_mov = NULL, *code_buf = NULL;
    LukaStack *stack = NULL;

    if (!func->codes_head)
        return;

    //if/while/for 与 end对应
    stack = luka_stack_create(luka);
    code_mov = func->codes_head;
    while (code_mov) {
        if (code_mov->type == LUKA_FOR || code_mov->type == LUKA_IF || code_mov->type == LUKA_ELSEIF || code_mov->type == LUKA_ELSE || code_mov->type == LUKA_WHILE) {
            luka_stack_push(luka, stack, code_mov);
        } else if (code_mov->type == LUKA_END) {
            code_buf = luka_stack_top(luka, stack);
            if (!code_buf) {
                fprintf(stderr, "'%s' can't match '{}'\n", key);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            code_buf->jump = code_mov;
            code_mov->jump = code_buf;
            luka_stack_pop(luka, stack);
        }
        code_mov = code_mov->next;
    }

    if (luka_stack_top(luka, stack)) {
        fprintf(stderr, "'%s' can't match '{}'\n", key);
        luka_exception(luka, LUKA_PARSE_ERR);
    }
    luka_stack_destroy(luka, stack);
    stack = NULL;

    //if/else if/else 对应合法检查
    code_mov = func->codes_head;
    while (code_mov) {
        if (code_mov->type == LUKA_ELSEIF || code_mov->type == LUKA_ELSE) {
            if (!code_buf || code_buf->type != LUKA_END) {
                fprintf(stderr, "'%s' else/else if error1\n", key);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
            if (code_buf->jump->type != LUKA_IF && code_buf->jump->type != LUKA_ELSEIF) {
                fprintf(stderr, "'%s' else/else if error2\n", key);
                luka_exception(luka, LUKA_PARSE_ERR);
            }
        }
        code_buf = code_mov;
        code_mov = code_mov->next;
    }

    //continue/break检查
    stack = luka_stack_create(luka);
    code_mov = func->codes_head;
    while (code_mov) {
        if (code_mov->type == LUKA_FOR || code_mov->type == LUKA_WHILE) {
            luka_stack_push(luka, stack, code_mov);
        } else if (code_mov->type == LUKA_END && (code_mov->jump->type == LUKA_FOR || code_mov->jump->type == LUKA_WHILE)) {
            luka_stack_pop(luka, stack);
        }

        if (code_mov->type == LUKA_BREAK || code_mov->type == LUKA_CONTINUE) {
            code_buf = luka_stack_top(luka, stack);
            if (!code_buf) {
                if (code_mov->type == LUKA_BREAK)
                    fprintf(stderr, "can't use break in here\n");
                else
                    fprintf(stderr, "can't use continue in here\n");
                luka_exception(luka, LUKA_PARSE_ERR);
            }
        }

        code_mov = code_mov->next;
    }

    luka_stack_destroy(luka, stack);
    stack = NULL;

    //调用函数检查
    code_mov = func->codes_head;
    while (code_mov) {
        if (code_mov->express) {
            luka_express_func_check(luka, code_mov->express);
        }
        code_mov = code_mov->next;
    }
}

/** 梳理所有循环if/else的嵌套 **/
static void luka_parse_carding_main (Luka *luka) {
    luka_object_each(luka, luka->funcs, NULL, luka_parse_carding_main_proc);
}

/***********************************************************
  LukaExpress 
***********************************************************/

static void luka_express_add   (Luka *luka, LukaExpress *express, LukaExpressNode *node);
static int  luka_express_fix1  (Luka *luka, LukaExpress *express);
static int  luka_express_fix2  (Luka *luka, LukaExpress *express);
static int  luka_express_rpn   (Luka *luka, LukaExpress *express);
static int  luka_express_test  (Luka *luka, LukaExpress *express);

/** 是否为节点分割 **/
static int luka_express_end (const char *s) {
    int i = 0;
    const char *buf = NULL;

    if (*s <= 32)
        return 1;

    for (i = LUKA_OPER_OBJECT; i <= LUKA_OPER_MINUSEQU; i++) {
        buf = _get_oper(i);
        if (strncmp(s, buf, strlen(buf)) == 0) {
            return 1;
        }
    }
    return 0;
}

/** 匹配oper **/
static int luka_express_oper (Luka *luka, const char **s) {
    int i = 0;
    const char *p = *s;
    const char *buf = NULL;

    if (strncmp(p, "/=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_DIVIDEQU;
    }

    else if (strncmp(p, "*=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_TIMESEQU;
    }

    else if (strncmp(p, "+=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_PLUSEQU;
    }

    else if (strncmp(p, "-=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_REMAINEQU;
    }

    else if (strncmp(p, "*=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_MINUSEQU;
    }

    else if (strncmp(p, ">=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_MOREEQU;
    }

    else if (strncmp(p, ">", 1) == 0) {
        *s = p + 2;
        return LUKA_OPER_MORE;
    }

    else if (strncmp(p, "<=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_LESSEQU;
    }

    else if (strncmp(p, "<", 1) == 0) {
        *s = p + 2;
        return LUKA_OPER_LESS;
    }

    else if (strncmp(p, "==", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_EQU2;
    }

    else if (strncmp(p, "!=", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_NOTQEU;
    }

    else if (strncmp(p, "&&", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_AND;
    }

    else if (strncmp(p, "||", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_OR;
    }

    else if (strncmp(p, "->", 2) == 0) {
        *s = p + 2;
        return LUKA_OPER_OBJECT;
    }

    else if (*p == '[') {
        *s = p + 1;
        return LUKA_OPER_ARRAY_LEFT;
    }

    else if (*p == ']') {
        *s = p + 1;
        return LUKA_OPER_ARRAY_RIGHT;
    }

    else if (*p == '(') {
        *s = p + 1;
        return LUKA_OPER_SMA_BRACL;
    }

    else if (*p == ')') {
        *s = p + 1;
        return LUKA_OPER_SMA_BRACR;
    }

    else if (*p == '/') {
        *s = p + 1;
        return LUKA_OPER_DIVIDE;
    }

    else if (*p == '*') {
        *s = p + 1;
        return LUKA_OPER_TIMES;
    }

    else if (*p == '%') {
        *s = p + 1;
        return LUKA_OPER_REMAINDER;
    }

    else if (*p == '+') {
        *s = p + 1;
        return LUKA_OPER_PLUS;
    }

    else if (*p == '-') {
        *s = p + 1;
        return LUKA_OPER_MINUS;
    }

    else if (*p == '=') {
        *s = p + 1;
        return LUKA_OPER_EQU;
    }
    return -1;
}

/** 匹配int **/
static int luka_express_int (const char **s, long *i) {
    const char *p = *s;

    if (*p == '-')
        p++;

    if (*p < '0' || *p > '9')
        return 0;

    while (*p >= '0' && *p <= '9')
        p++;

    if (*p == '.')
        return 0;

    if (luka_express_end(p) == 0)
        return 0;
    
    *i = atol(*s);
    *s = p;
    return 1;
}

/** 匹配double **/
static int luka_express_double (const char **s, double *d) {
    const char *p = *s;

    if (*p == '-')
        p++;

    if ((*p < '0' || *p > '9') && *p != '.')
        return 0;

    while ((*p >= '0' && *p <= '9') || *p == '.') {
        p++;
    }

    if (luka_express_end(p) == 0)
        return 0;

    *d = (double)atof(*s);
    *s = p;
    return 1;
}

/** 匹配string **/
static int luka_express_string (Luka *luka, const char **s, char **str) {
    const char *p = *s, *p_end = NULL;
    char c = *p;

    if (*p != '\'' && *p != '\"')
        return 0;

    p_end = p + 1;
    while (*p_end != '\0' && ((p_end = strchr(p_end, c)) != NULL) && _count_trans_num(p_end - 1) % 2 != 0) {
        p_end++;
    }

    if (!p_end || *p_end == '\0')
        return 0;

    //空字符串
    if (p + 1 == p_end) {
        *str = luka_strdup(luka, "");
    } else {
        *str = _strdup_code(luka, p + 1, p_end - 1);
        _trans_str(*str);
    }

    *s = p_end + 1;
    return 1;
}

/** 匹配一个变量 **/
static int luka_express_var (Luka *luka, const char **s, char **var_name) {
    const char *p = *s;
    const char *right = NULL;

    if (!strchr(g_Name, *p) || strchr(g_Num, *p))
        return 0;
    right = p++;

    while (*p != 0 && strchr(g_Name, *p)) {
        right = p++;
    }

    //检查有无(
    while (*p <= 32 && *p != 0) {
        p++;
    }
    if (*p == '(') {
        return 0;
    }

    *var_name = _strdup_code(luka, *s, right);
    *s = right + 1;
    return 1;
}

/** 调用函数 **/
static int luka_express_func (Luka *luka, const char **s, char **func_name, LukaExpress ***func_param, size_t *func_len) {
    const char *p = *s;
    const char *left = NULL, *right = NULL;
    char *str = NULL, **strs = NULL;
    size_t i = 0, strs_len = 0;

    if (!strchr(g_Name, *p) || strchr(g_Num, *p))
        return 0;
    right = p++;

    while (*p != 0 && strchr(g_Name, *p)) {
        right = p++;
    }

    while (*p <= 32 && *p != 0) {
        p++;
    }

    //检查有无(
    if (*p++ != '(')
        return 0;

    *func_name = _strdup_code(luka, *s, right);

    left = p;
    right = _strstr_code(p, ')');
    if (!right) {
        *func_name = NULL;
        return 0;
    }

    if (left == right) {
        *s = right + 1;
        return 1;
    }

    p = right + 1;
    str = _trim(_strdup_code(luka, left, right - 1));
    if (strlen(str) != 0) {
        strs = _splite_code(luka, str, ',', &strs_len);
    }
    *s = p;

    *func_param = (LukaExpress **)luka_malloc(luka, sizeof(LukaExpress *) * strs_len);
    *func_len = strs_len;

    for (i = 0; i < strs_len; i++) {
        if (strlen(_trim(strs[i])) == 0)
            return -1;

        (*func_param)[i] = luka_express_parse(luka, strs[i]);
        if ((*func_param)[i] == NULL) {
            return -1;
        }
    }
    return 1;
}

/** 解析表达式 **/
static LukaExpress *luka_express_parse (Luka *luka, const char *s) {
    int ret = 0;
    LukaExpress *express = NULL;
    LukaExpressNode *expnode = NULL;
    char *s_cp = NULL;

    //数据
    int    data_oper  = 0;
    long   data_i     = 0;
    double data_d     = 0;
    char  *data_str   = NULL;
    char  *data_var   = NULL;
    char  *func_name  = NULL;
    LukaExpress **func_param = NULL;
    size_t func_len   = 0;

    express = (LukaExpress *)luka_malloc(luka, sizeof(LukaExpress));
    if (strlen(_trim((s_cp = luka_strdup(luka, s)))) == 0) {
        return NULL;
    }

    //解析开始
    while (*s != 0) {
        if (*s <= 32) {
            s++;
        }

        //null
        else if (strncmp(s, "null", 4) == 0 && luka_express_end(s + 4)) {
            s += 4;

            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_null(luka);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //true
        else if (strncmp(s, "true", 4) == 0 && luka_express_end(s + 4)) {
            s += 4;

            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_true(luka);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //false
        else if (strncmp(s, "false", 5) == 0 && luka_express_end(s + 5)) {
            s += 5;

            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_false(luka);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //oper
        else if ((data_oper = luka_express_oper(luka, &s)) != -1) {
            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_OPER;
            expnode->data_oper = data_oper;
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //int
        else if (luka_express_int(&s, &data_i)) {
            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_put_int(luka, data_i);
			luka_data_up(luka, expnode->data_p);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //double
        else if (luka_express_double(&s, &data_d)) {
            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_put_double(luka, data_d);
			luka_data_up(luka, expnode->data_p);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //字符串
        else if (luka_express_string(luka, &s, &data_str)) {
            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_DATA;
            expnode->data_p = luka_put_string(luka, data_str);
			luka_data_up(luka, expnode->data_p);
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }
        
        //变量
        else if (luka_express_var(luka, &s, &data_var)) {
            if (!_isname(data_var)) {
                return NULL;
            }

            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_VAR;
            expnode->data_var = data_var;
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        //函数调用
        else if ((ret = luka_express_func(luka, &s, &func_name, &func_param, &func_len)) != 0) {
            if (ret == -1) {
                return NULL;
            }

            expnode = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
            expnode->type = LUKA_EXP_FUNC;
            expnode->data_func = func_name;
            expnode->data_param = func_param;
            expnode->data_len = func_len;
            luka_express_add(luka, express, expnode);
            expnode = NULL;
        }

        else {
            fprintf(stderr, "can't parse '%.12s'\n", s);
            return NULL;
        }
    }

    //处理->与[]问题
    if (!luka_express_fix1(luka, express)) {
        fprintf(stderr, "can't fix '->/[]'\n");
        return NULL;
    }

    //处理负数
    if (!luka_express_fix2(luka, express)) {
        fprintf(stderr, "can't fix '-'\n");
        return NULL;
    }

    //转逆波兰表达式
    if (!luka_express_rpn(luka, express)) {
        fprintf(stderr, "rpn express failed\n");
        return NULL;
    }

    //测试运行表达式
    if (!luka_express_test(luka, express)) {
        fprintf(stderr, "test express failed\n");
        return NULL;
    }
    return express;
}

/***********************************************************
  LukaExpress 
***********************************************************/

static void luka_express_destroy       (Luka *luka, LukaExpress *express);
static void luka_express_node_destroy2 (Luka *luka, LukaExpressNode *node);

static void luka_express_node_destroy (Luka *luka, LukaExpressNode *node) {
    luka_express_node_destroy2(luka, node);
    luka_free(luka, node);
}

static void luka_express_node_destroy2 (Luka *luka, LukaExpressNode *node) {
    if (node->type == LUKA_EXP_VAR) {
        if (node->data_var) luka_free(luka, node->data_var);
    } else if (node->type == LUKA_EXP_FUNC) {
        size_t i = 0;
        for (i = 0; i < node->data_len; i++) {
            luka_express_destroy(luka, node->data_param[i]);
        }
        if (node->data_param)
            luka_free(luka, node->data_param);
        luka_free(luka, node->data_func);
    } else if (node->type == LUKA_EXP_OBJECT) {
        luka_free(luka, node->data_objkey);
    }
}

static void luka_express_destroy (Luka *luka, LukaExpress *express) {
    LukaExpressNode *mov = NULL, *buf = NULL;

    mov = express->RPN;
    while (mov) {
        buf = mov;
        mov = mov->next;
        luka_express_node_destroy(luka, buf);
    }

    luka_free(luka, express);
}

/***********************************************************
  LukaExpress 队列 
***********************************************************/

/** 队列 - 追加节点 **/
static void luka_express_add (Luka *luka, LukaExpress *express, LukaExpressNode *node) {
    if (!express->queue) {
        express->queue = express->queue_tail = node;
    } else {
        express->queue_tail->next = node;
        node->last = express->queue_tail;
        express->queue_tail = node;
    }
}

/** 队列 - 插入节点(p节点前增加node节点) **/
static void luka_express_insert (Luka *luka, LukaExpress *express, LukaExpressNode *p, LukaExpressNode *node) {
    LukaExpressNode *left = p->last;

    node->last = left;
    node->next = p;
    p->last = node;

    if (!left) {
        express->queue = node;
    } else {
        left->next = node;
    }
}

/** 队列 - 插入节点(p节点后增加node节点) **/
static void luka_express_insert2 (Luka *luka, LukaExpress *express, LukaExpressNode *p, LukaExpressNode *node) {
    if (p->next) {
        luka_express_insert(luka, express, p->next, node);
    } else {
        luka_express_add(luka, express, node);
    }
}

/** 队列 - 取出节点 **/
static LukaExpressNode *luka_express_pop (Luka *luka, LukaExpress *express) {
    LukaExpressNode *node = NULL;

    if (!express->queue)
        return NULL;

    node = express->queue;
    express->queue = node->next;
    if (!express->queue)
        express->queue_tail = NULL;

    return node;
}

/***********************************************************
  LukaExpress 缓存栈 
***********************************************************/

/** 缓存栈 - 入栈 **/
static void luka_express_stack_push (Luka *luka, LukaExpress *express, LukaExpressNode *buf) {
    buf->next = express->stack;
    express->stack = buf;
}

/** 缓存栈 - 栈顶 **/
static LukaExpressNode *luka_express_stack_top (Luka *luka, LukaExpress *express) {
    return express->stack;
}

/** 缓存栈 - 出栈 **/
static LukaExpressNode *luka_express_stack_pop (Luka *luka, LukaExpress *express) {
    LukaExpressNode *node = express->stack;
    express->stack = node->next;
    return node;
}

/***********************************************************
  LukaExpress 逆波兰表达式 
***********************************************************/

/** 逆波兰表达式 - 增加 **/
static void luka_express_rpn_push (Luka *luka, LukaExpress *express, LukaExpressNode *buf) {
    buf->last = NULL;
    buf->next = NULL;

    if (express->RPN == NULL) {
        express->RPN = express->RPN_tail = buf;
    } else {
        express->RPN_tail->next = buf;
        buf->last = express->RPN_tail;
        express->RPN_tail = buf;
    }
}

/** 逆波兰表达式 - 查询 **/
static LukaExpressNode *luka_express_rpn_find (LukaExpress *express) {
    LukaExpressNode *mov = express->RPN;
    while (mov) {
        if (mov->type == LUKA_EXP_OPER)
            return mov;
        mov = mov->next;
    }
    return NULL;
}

/** 逆波兰表达式 - 剔除 **/
static void luka_express_rpn_rmv (Luka *luka, LukaExpress *express, LukaExpressNode *node) {
    LukaExpressNode *left = node->last, *right = node->next;

    if (!left && right) {
        express->RPN = express->RPN->next;
        if (!express->RPN)
            express->RPN_tail = NULL;
    } else if (left && right) {
        left->next = right;
        right->last = left;
    } else if (left && !right) {
        left->next = NULL;
        express->RPN_tail = left;
    }

	luka_express_node_destroy(luka, node);
}

/** 逆波兰表达式 - 替换 **/
static void luka_express_rpn_update (Luka *luka, LukaExpress *express, LukaExpressNode *node, voidp data) {
    luka_express_node_destroy2(luka, node);
    node->type = LUKA_EXP_DATA;
    node->data_p = data;
}

/***********************************************************
  LukaExpress Copy 
***********************************************************/

/** 复制一份逆波兰表达式 **/
static LukaExpress *luka_express_copy (Luka *luka, LukaExpress *express) {
    LukaExpress *express_cp = NULL;
    LukaExpressNode *mov = NULL, *buf = NULL;

    express_cp = luka_malloc(luka, sizeof(LukaExpress));
    mov = express->RPN;
    while (mov) {
        buf = luka_malloc(luka, sizeof(LukaExpressNode));
        buf->type = mov->type;
        buf->last = buf->next = NULL;

        if (mov->type == LUKA_EXP_DATA) {
            buf->data_p = mov->data_p;
        } else if (mov->type == LUKA_EXP_VAR) {
            buf->data_var = luka_strdup(luka, mov->data_var);
        } else if (mov->type == LUKA_EXP_OPER) {
            buf->data_oper = mov->data_oper;
        } else if (mov->type == LUKA_EXP_FUNC) {
            buf->data_func = luka_strdup(luka, mov->data_func);
            if (mov->data_param) {
                size_t i = 0;
                buf->data_len = mov->data_len;
                buf->data_param = (LukaExpress **)luka_malloc(luka, sizeof(LukaExpress*) * mov->data_len);
                for (i = 0; i < mov->data_len; i++) {
                    buf->data_param[i] = luka_express_copy(luka, mov->data_param[i]);
                }
            }
        } else if (mov->type == LUKA_EXP_OBJECT) {
            buf->data_p = mov->data_p;
            buf->data_objkey = luka_strdup(luka, mov->data_objkey);
        } else if (mov->type == LUKA_EXP_ARRAY) {
            buf->data_p = mov->data_p;
            buf->data_arrindex = mov->data_arrindex;
        }

        luka_express_rpn_push(luka, express_cp, buf);
        mov = mov->next;
    }
    return express_cp;
}

/***********************************************************
  LukaExpress 修复相关结构'->[]-' 
***********************************************************/

/** 处理->与[]问题 **/
static int luka_express_fix1 (Luka *luka, LukaExpress *express) {
    int brace_index = 0;
    LukaExpressNode *mov = NULL, *buf = NULL, *buf2 = NULL;
    LukaExpressNode *leftb = NULL, *rightb = NULL, *fdata = NULL;

    mov = express->queue;
    while (mov) {
        if (mov->type == LUKA_EXP_OPER) {
            //->
            if (mov->data_oper == LUKA_OPER_OBJECT) {
                if (mov->last == NULL || mov->next == NULL)
                    return 0;

                leftb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                leftb->type = LUKA_EXP_OPER;
                leftb->data_oper = LUKA_OPER_SMA_BRACL;

                rightb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                rightb->type = LUKA_EXP_OPER;
                rightb->data_oper = LUKA_OPER_SMA_BRACR;

                buf = mov->last;
                if (buf->type == LUKA_EXP_OPER) {
                    if (buf->data_oper != LUKA_OPER_SMA_BRACR)
                        return 0;

                    brace_index = 1;
                    while (buf && brace_index > 0) {
                        if (buf->data_oper == LUKA_OPER_SMA_BRACL) {
                            brace_index--;
                        }

                        if (brace_index > 0) {
                            buf = buf->last;
                        }
                    }

                    if (brace_index > 0)
                        return 0;
                }

                luka_express_insert(luka, express, buf, leftb);
                luka_express_insert2(luka, express, mov->next, rightb);

                mov = rightb->next;
                leftb = rightb = NULL;
            }

            //]
            else if (mov->data_oper == LUKA_OPER_ARRAY_RIGHT) {
                buf = mov->last;
                brace_index = 1;

                leftb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                leftb->type = LUKA_EXP_OPER;
                leftb->data_oper = LUKA_OPER_SMA_BRACL;

                rightb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                rightb->type = LUKA_EXP_OPER;
                rightb->data_oper = LUKA_OPER_SMA_BRACR;

                //移动到[
                while (buf && brace_index > 0) {
                    if (buf->data_oper == LUKA_OPER_ARRAY_RIGHT) {
                        brace_index++;
                    } else if (buf->data_oper == LUKA_OPER_ARRAY_LEFT) {
                        brace_index--;
                    }

                    if (brace_index > 0) {
                        buf = buf->last;
                    }
                }

                if (brace_index > 0)
                    return 0;

                //处理[]这种情况
                if (buf->next == mov) {
                    fdata = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                    fdata->type = LUKA_EXP_DATA;
                    fdata->data_p = luka_put_int(luka, -1);
                    luka_data_up(luka, fdata->data_p);
                    luka_express_insert(luka, express, mov, fdata);
                }

                //在[]里增加()
                luka_express_insert2(luka, express, buf, leftb);
                luka_express_insert(luka, express, mov, rightb);

                //移动到上一个节点
                buf = buf->last;

                if (buf->type == LUKA_EXP_OPER) {
                    if (buf->data_oper != LUKA_OPER_SMA_BRACR)
                        return 0;

                    brace_index = 1;
                    while (buf && brace_index > 0) {
                        if (buf->data_oper == LUKA_OPER_SMA_BRACL) {
                            brace_index--;
                        }

                        if (brace_index > 0) {
                            buf = buf->last;
                        }
                    }

                    if (brace_index > 0)
                        return 0;
                }

                //在整个数组表达式外增加()
                leftb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                leftb->type = LUKA_EXP_OPER;
                leftb->data_oper = LUKA_OPER_SMA_BRACL;

                rightb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                rightb->type = LUKA_EXP_OPER;
                rightb->data_oper = LUKA_OPER_SMA_BRACR;

                luka_express_insert(luka, express, buf, leftb);
                luka_express_insert2(luka, express, mov, rightb);

                mov = rightb->next;
                leftb = rightb = NULL;
            }
            
            else {
                mov = mov->next;
            }
        } else {
            mov = mov->next;
        }
    }
    return 1;
}

/** 处理负号问题 **/
static int luka_express_fix2 (Luka *luka, LukaExpress *express) {
    int brace_index = 0;
    LukaExpressNode *mov = NULL, *buf = NULL, *buf2 = NULL;
    LukaExpressNode *leftb = NULL, *rightb = NULL, *zero = NULL;

    mov = express->queue;
    while (mov) {
        //-
        if (mov->type == LUKA_EXP_OPER && mov->data_oper == LUKA_OPER_MINUS) {
            //判定为负号
            if (!mov->last || (mov->last->type == LUKA_EXP_OPER && mov->data_oper != LUKA_OPER_SMA_BRACR) ) {
                leftb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                leftb->type = LUKA_EXP_OPER;
                leftb->data_oper = LUKA_OPER_SMA_BRACL;

                rightb = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                rightb->type = LUKA_EXP_OPER;
                rightb->data_oper = LUKA_OPER_SMA_BRACR;

                zero = (LukaExpressNode *)luka_malloc(luka, sizeof(LukaExpressNode));
                zero->type = LUKA_EXP_DATA;
                zero->data_p = luka_put_int(luka, 0);
                luka_data_up(luka, zero->data_p);

                //移动到后边
                buf2 = mov->next;
                if (buf2->type == LUKA_EXP_OPER) {
                    if (buf2->data_oper != LUKA_OPER_SMA_BRACL)
                        return 0;

                    brace_index = 1;
                    buf2 = buf2->next;
                    while (buf2 && brace_index > 0) {
                        if (buf2->data_oper == LUKA_OPER_SMA_BRACR) {
                            brace_index--;
                        } else if (buf2->data_oper == LUKA_OPER_SMA_BRACL) {
                            brace_index++;
                        }

                        if (brace_index > 0) {
                            buf2 = buf2->next;
                        }
                    }

                    if (brace_index > 0)
                        return 0;
                }
                
                //添加')'
                luka_express_insert2(luka, express, buf2, rightb);

                //添加'(''0'
                luka_express_insert(luka, express, mov, zero);
                luka_express_insert(luka, express, zero, leftb);

                mov = rightb->next;
                zero = leftb = rightb = NULL;
            }

            else {
                mov = mov->next;
            }
        } else {
            mov = mov->next;
        }
    }
    return 1;
}

/** 转逆波兰表达式 **/
static int luka_express_rpn (Luka *luka, LukaExpress *express) {
    LukaExpressNode *buf = NULL, *buf2 = NULL;

    if (!express->queue)
        return 0;

    while ((buf = luka_express_pop(luka, express)) != NULL) {
        if (buf->type == LUKA_EXP_OPER) {
            // '('
            if (buf->data_oper == LUKA_OPER_SMA_BRACL) {
                luka_express_stack_push(luka, express, buf);
            }

            // ')'
            else if (buf->data_oper == LUKA_OPER_SMA_BRACR) {
                while ((buf2 = luka_express_stack_top(luka, express)) && buf2->data_oper != LUKA_OPER_SMA_BRACL) {
                    buf2 = luka_express_stack_pop(luka, express);
                    luka_express_rpn_push(luka, express, buf2);
                }

                if (!buf2)
                    return 0;

                buf2 = luka_express_stack_pop(luka, express);
            }

            //其他运算符
            else {
                while ((buf2 = luka_express_stack_top(luka, express)) && 
                    _get_oper_level(buf2->data_oper) <= _get_oper_level(buf->data_oper) && 
                    buf2->data_oper != LUKA_OPER_SMA_BRACL) {

                    buf2 = luka_express_stack_pop(luka, express);
                    luka_express_rpn_push(luka, express, buf2);
                }
                luka_express_stack_push(luka, express, buf);
            }
        }
        
        //操作数
        else {
            luka_express_rpn_push(luka, express, buf);
        }
    }

    //清空栈
    while ((buf2 = luka_express_stack_top(luka, express)) != NULL) {
        if (buf2->data_oper == LUKA_OPER_SMA_BRACL)
            return 0;

        buf2 = luka_express_stack_pop(luka, express);
        luka_express_rpn_push(luka, express, buf2);
    }
    return 1;
}

/** 测试运行表达式(检查表达式是否合法) **/
static int luka_express_test (Luka *luka, LukaExpress *express) {
    size_t i = 0;
    LukaExpress *express_cp = NULL;
    LukaExpressNode *oper = NULL, *left = NULL, *leftleft = NULL;
    
    express_cp = luka_express_copy(luka, express);
    while ((oper = luka_express_rpn_find(express_cp)) != NULL) {
        left = oper->last;
        if (left) leftleft = left->last;

        switch (oper->data_oper) {
            //->
            case LUKA_OPER_OBJECT: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                leftleft->type = LUKA_EXP_VAR;
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //[
            case LUKA_OPER_ARRAY_LEFT: {
                if (!left || !leftleft || oper->next->data_oper != LUKA_OPER_ARRAY_RIGHT)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                leftleft->type = LUKA_EXP_VAR;
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper->next);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //]
            case LUKA_OPER_ARRAY_RIGHT: {
                if (!left || !leftleft || oper->next->data_oper != LUKA_OPER_ARRAY_LEFT)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                leftleft->type = LUKA_EXP_VAR;
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper->next);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //'/'
            case LUKA_OPER_DIVIDE: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //*
            case LUKA_OPER_TIMES: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //%
            case LUKA_OPER_REMAINDER: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //+
            case LUKA_OPER_PLUS: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //-
            case LUKA_OPER_MINUS: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //>=
            case LUKA_OPER_MOREEQU: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //>
            case LUKA_OPER_MORE: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //<=
            case LUKA_OPER_LESSEQU: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //<
            case LUKA_OPER_LESS: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //==
            case LUKA_OPER_EQU2: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //!=
            case LUKA_OPER_NOTQEU: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //&&
            case LUKA_OPER_AND: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //||
            case LUKA_OPER_OR: {
                if (!left || !leftleft)
                    return 0;

                if (left->type == LUKA_EXP_OPER || leftleft->type == LUKA_EXP_OPER)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //=
            case LUKA_OPER_EQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //'/='
            case LUKA_OPER_DIVIDEQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //*=
            case LUKA_OPER_TIMESEQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //%=
            case LUKA_OPER_REMAINEQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //+=
            case LUKA_OPER_PLUSEQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //-=
            case LUKA_OPER_MINUSEQU: {
                if (!left || !leftleft)
                    return 0;

                if (leftleft->type != LUKA_EXP_VAR)
                    return 0;

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;
        }
    }
    return (express_cp->RPN && !express_cp->RPN->next);
}

/***********************************************************
  Package 
***********************************************************/

void luka_c_regs       (Luka *luka);
void luka_c_vars       (Luka *luka);

void luka_socket_regs  (Luka *luka);
void luka_socket_vars  (Luka *luka);

void luka_c2_regs      (Luka *luka);
void luka_c2_vars      (Luka *luka);

void luka_img_regs     (Luka *luka);
void luka_img_vars     (Luka *luka);

#ifdef _WIN32
void luka_windows_regs (Luka *luka);
void luka_windows_vars (Luka *luka);
#endif

void luka_mysql_regs   (Luka *luka);
void luka_mysql_vars   (Luka *luka);

typedef struct Package {
	const char *pkg_name;
	void (*pkg_p)(Luka *);
	void (*pkg_p2)(Luka *);
	int pkg_load;
} Package;

static Package g_Package[] = {
	{"luka",    luka_luka_regs,    NULL,               0},
	{"c",       luka_c_regs,       luka_c_vars,        0},
	{"socket",  luka_socket_regs,  luka_socket_vars,   0},
	{"c2",      luka_c2_regs,      luka_c2_vars,       0},
	{"img",     luka_img_regs,     luka_img_vars,      0},
#ifdef _WIN32
	{"windows", luka_windows_regs, luka_windows_vars,  0},
#endif
	{"mysql",   luka_mysql_regs,   luka_mysql_vars,    0},
	{NULL,      NULL,              NULL,               0}
};

static void luka_package (Luka *luka, const char *package) {
	size_t i = 0;

    //代码中查找
	while (g_Package[i].pkg_name != NULL) {
		if (strcmp(g_Package[i].pkg_name, package) == 0) {
			if (g_Package[i].pkg_load == 0) {
				g_Package[i].pkg_p(luka);
				g_Package[i].pkg_load = 1;
			}
			return;
		}
		i++;
	}

    //没找到
	fprintf(stderr, "can't find package '%s'\n", package);
}

/***********************************************************
  PackageLuka(核心函数) 
***********************************************************/

/** luka **/
static voidp luka_c_luka (Luka *luka, voidp *p, size_t n) {
    fprintf(stdout, "%s", LUKA_VERSION);
	return luka_null(luka);
}

/** print **/
static voidp luka_c_print (Luka *luka, voidp *p, size_t n) {
	size_t i = 0;
    LukaData *data = NULL;

	for (i = 0; i < n; i++) {
        data = p[i];

        if (data->type == LUKA_INT) {
			fprintf(stdout, "%ld", luka_get_int(luka, data));
        } else if (data->type == LUKA_DOUBLE) {
			fprintf(stdout, "%g", luka_get_double(luka, data));
        } else if (data->type == LUKA_STRING) {
			fprintf(stdout, "%s", luka_get_string(luka, data));
        }
    }
	return luka_null(luka);
}

/** object **/
static voidp luka_c_object (Luka *luka, voidp *p, size_t n) {
	return luka_put_object(luka);
}

/** array **/
static voidp luka_c_array (Luka *luka, voidp *p, size_t n) {
	return luka_put_array(luka);
}

/** count **/
static voidp luka_c_count (Luka *luka, voidp *p, size_t n) {
    LukaData *data = NULL;

    if (n < 1)
		return luka_null(luka);

    data = p[0];
    if (data->type == LUKA_STRING) {
        return luka_put_int(luka, strlen(data->data_str));
    } else if (data->type == LUKA_BYTE) {
        return luka_put_int(luka, data->data_byte.size);
    } else if (data->type == LUKA_ARRAY) {
        return luka_put_int(luka, data->data_array->length);
    }
    return luka_null(luka);
}

static void luka_c_dump_ex (Luka *luka, LukaData *data);

void luka_c_dump_ex_proc (Luka *luka, LukaObject *obj, void *p, const char *key, voidp value) {
    fprintf(stdout, "\"%s\":", key);
    luka_c_dump_ex(luka, (LukaData *)value);
    fprintf(stdout, ",");
}

/** luka_c_dump_ex **/
static void luka_c_dump_ex (Luka *luka, LukaData *data) {
    size_t i = 0;

    if (data->type == LUKA_NULL) {
		fprintf(stdout, "[null]");
	} else if (data->type == LUKA_TRUE) {
		fprintf(stdout, "[true]");
	} else if (data->type == LUKA_FALSE) {
		fprintf(stdout, "[false]");
	} else if (data->type == LUKA_INT) {
		fprintf(stdout, "[int]%ld", data->data_i);
	} else if (data->type == LUKA_DOUBLE) {
		fprintf(stdout, "[double]%g", data->data_d);
	} else if (data->type == LUKA_STRING) {
		fprintf(stdout, "[string]%s", data->data_str);
	} else if (data->type == LUKA_BYTE) {
		fprintf(stdout, "[byte]%d", data->data_byte.size);
	} else if (data->type == LUKA_OBJECT) {
		fprintf(stdout, "[object]{");
        luka_object_each(luka, data->data_object, NULL, luka_c_dump_ex_proc);
		fprintf(stdout, "}");
	} else if (data->type == LUKA_ARRAY) {
		fprintf(stdout, "[array][");
        for (i = 0; i < luka_array_length(luka, data->data_array); i++) {
            fprintf(stdout, "%d:", i);
			luka_c_dump_ex(luka, luka_array_get(luka, data->data_array, i));
			if (i + 1 < luka_array_length(luka, data->data_array))
				fprintf(stdout, ",");
        }
		fprintf(stdout, "]");
	} else if (data->type == LUKA_VOIDP) {
		fprintf(stdout, "[void*]%p", data->data_p);
	}
}

/** dump **/
static voidp luka_c_dump (Luka *luka, voidp *p, size_t n) {
	size_t i = 0;

	for (i = 0; i < n; i++) {
		luka_c_dump_ex(luka, (LukaData *)p[i]);
		fprintf(stdout, "\n");
	}
	return luka_null(luka);
}

/** tostr **/
static voidp luka_c_tostr (Luka *luka, voidp *p, size_t n) {
    voidp pr = luka_null(luka);

	if (luka_is_string(p[0])) {
		pr = p[0];
	} else if (luka_is_int(p[0])) {
		char temp[25] = {0};
		memset(temp, 0, sizeof(temp));
		sprintf(temp, "%ld", luka_get_int(luka, p[0]));
		pr = luka_put_string(luka, luka_strdup(luka, temp));
	} else if (luka_is_double(p[0])) {
		char temp[25] = {0};
		memset(temp, 0, sizeof(temp));
		sprintf(temp, "%g", luka_get_double(luka, p[0]));
		pr = luka_put_string(luka, luka_strdup(luka, temp));
	} else if (luka_is_byte(p[0])) {
		char *temp2 = NULL;
		unsigned char* datap = NULL;
		size_t size = 0;
		datap = luka_get_byte(luka, p[0], &size);

		temp2 = (char *)luka_malloc(luka, size + 5);
		memcpy(temp2, datap, size);
		pr = luka_put_string(luka, temp2);
	}
	return pr;
}

/** iswin32 **/
static voidp luka_c_iswin32 (Luka *luka, voidp *p, size_t n) {
#ifdef _WIN32
    return luka_true(luka);
#else
    return luka_false(luka);
#endif
}

static void luka_luka_regs (Luka *luka) {
	luka_reg(luka, "luka",    luka_c_luka);
	luka_reg(luka, "print",   luka_c_print);
	luka_reg(luka, "object",  luka_c_object);
	luka_reg(luka, "array",   luka_c_array);
	luka_reg(luka, "count",   luka_c_count);
	luka_reg(luka, "dump",    luka_c_dump);
	luka_reg(luka, "tostr",   luka_c_tostr);
	luka_reg(luka, "iswin32", luka_c_iswin32);
}

/***********************************************************
  执行表达式(核心) 
***********************************************************/

static voidp luka_expressnode_exec (Luka *luka, LukaObject *vars, LukaExpress *express, LukaExpressNode *node);

voidp luka_express_exec (Luka *luka, LukaObject *vars, LukaExpress *express) {
    voidp dataID = luka_null(luka);
    LukaExpress *express_cp = NULL;
    LukaExpressNode *oper = NULL, *left = NULL, *leftleft = NULL;
    LukaData *leftID = NULL, *leftleftID = NULL, *newDataID = NULL;

    express_cp = luka_express_copy(luka, express);
    while ((oper = luka_express_rpn_find(express_cp)) != NULL) {
        left = oper->last;
        if (left) leftleft = left->last;

        switch (oper->data_oper) {
            //->
            case LUKA_OPER_OBJECT: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                if (leftleftID->type == LUKA_OBJECT) {
                    if (left->type == LUKA_EXP_VAR) {
                        luka_express_node_destroy2(luka, leftleft);
                        leftleft->type = LUKA_EXP_OBJECT;
                        leftleft->data_p = leftleftID;
                        leftleft->data_objkey = luka_strdup(luka, left->data_var);
                    } else {
                        leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                        if (leftID->type == LUKA_STRING && *leftID->data_str != 0) {
                            luka_express_node_destroy2(luka, leftleft);
                            leftleft->type = LUKA_EXP_OBJECT;
                            leftleft->data_p = leftleftID;
                            leftleft->data_objkey = luka_strdup(luka, leftID->data_str);
                        } else {
                            luka_express_rpn_update(luka, express_cp, leftleft, luka_null(luka));
                        }
                    }
                } else {
                    luka_express_rpn_update(luka, express_cp, leftleft, luka_null(luka));
                }

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //[
            case LUKA_OPER_ARRAY_LEFT: {
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                if (leftleftID->type == LUKA_ARRAY && leftID->type == LUKA_INT) {
                    luka_express_node_destroy2(luka, leftleft);
                    leftleft->type = LUKA_EXP_ARRAY;
                    leftleft->data_p = leftleftID;
                    leftleft->data_arrindex = leftID->data_i;
                } else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_INT) {
                    if (leftID->data_i >= 0 && leftID->data_i < strlen(leftleftID->data_str)) {
                        newDataID = luka_put_int(luka, leftleftID->data_str[leftID->data_i]);
                        luka_queue_add(luka, newDataID);
                    } else {
                        newDataID = luka_null(luka);
                    }
                    luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                } else if (leftleftID->type == LUKA_BYTE && leftID->type == LUKA_INT) {
                    if (leftID->data_i >= 0 && leftID->data_i < leftleftID->data_byte.size) {
                        newDataID = luka_put_int(luka, leftleftID->data_byte.data[leftID->data_i]);
                        luka_queue_add(luka, newDataID);
                    } else {
                        newDataID = luka_null(luka);
                    }
                } else {
                    luka_express_rpn_update(luka, express_cp, leftleft, luka_null(luka));
                }

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper->next);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //]
            case LUKA_OPER_ARRAY_RIGHT: {
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                if (leftleftID->type == LUKA_ARRAY && leftID->type == LUKA_INT) {
                    luka_express_node_destroy2(luka, leftleft);
                    leftleft->type = LUKA_EXP_ARRAY;
                    leftleft->data_p = leftleftID;
                    leftleft->data_arrindex = leftID->data_i;
                } else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_INT) {
                    if (leftID->data_i >= 0 && leftID->data_i < strlen(leftleftID->data_str)) {
                        newDataID = luka_put_int(luka, leftleftID->data_str[leftID->data_i]);
                        luka_queue_add(luka, newDataID);
                    } else {
                        newDataID = luka_null(luka);
                    }
                    luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                } else {
                    luka_express_rpn_update(luka, express_cp, leftleft, luka_null(luka));
                }

                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper->next);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //'/'
            case LUKA_OPER_DIVIDE: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (luka_get_int(luka, leftID) != 0) {
                        if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                            newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) / luka_get_int(luka, leftID));
                        } else {
                            newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) / luka_get_double(luka, leftID));
                        }
                    } else {
                        newDataID = luka_null(luka);

                    }
                } else {
                    newDataID = luka_null(luka);
                }

				luka_queue_add(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //*
            case LUKA_OPER_TIMES: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) * luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) * luka_get_double(luka, leftID));
                    }
                } else {
                    newDataID = luka_null(luka);
                }

				luka_queue_add(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //%
            case LUKA_OPER_REMAINDER: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                    newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) % luka_get_int(luka, leftID));
                } else {
                    newDataID = luka_null(luka);
                }

				luka_queue_add(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //+
            case LUKA_OPER_PLUS: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);

                //数字相加
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) + luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) + luka_get_double(luka, leftID));
                    }
                }

                //字符串相加
                else if (leftleftID->type == LUKA_STRING && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
					size_t len1 = 0, len2 = 0;
					char *new_str = NULL;
					
					len1 = strlen(leftleftID->data_str);
					len2 = 64;

					new_str = (char *)luka_malloc(luka, len1 + len2 + 10);
					
					if (leftID->type == LUKA_INT)
						sprintf(new_str, "%s%ld", leftleftID->data_str, leftID->data_i);
					else
						sprintf(new_str, "%s%g", leftleftID->data_str, leftID->data_d);
					newDataID = luka_put_string(luka, new_str);
                }
				
                //字符串相加
				else if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && leftID->type == LUKA_STRING) {
					size_t len1 = 0, len2 = 0;
					char *new_str = NULL;
					len1 = strlen(leftID->data_str);

					len2 = 64;
					new_str = (char *)luka_malloc(luka, len1 + len2 + 10);
					
					if (leftleftID->type == LUKA_INT)
						sprintf(new_str, "%ld%s", leftleftID->data_i, leftID->data_str);
					else
						sprintf(new_str, "%g%s", leftleftID->data_d, leftID->data_str);
					newDataID = luka_put_string(luka, new_str);
				}
                
				//字符串相加
                else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_STRING) {
                    char *new_str = (char *)luka_malloc(luka, strlen(leftleftID->data_str) + strlen(leftID->data_str) + 10);
                    sprintf(new_str, "%s%s", leftleftID->data_str, leftID->data_str);
                    newDataID = luka_put_string(luka, new_str);
				}
				
                //byte相加
                else if (leftleftID->type == LUKA_BYTE && leftID->type == LUKA_BYTE) {
					unsigned char *leftleftData = NULL, *leftData = NULL;
					size_t leftleftSize = 0, leftSize = 0;
					unsigned char *newByte = NULL;

					leftleftData = luka_get_byte(luka, leftleftID, &leftleftSize);
					leftData = luka_get_byte(luka, leftID, &leftSize);
					newByte = (unsigned char *)luka_malloc(luka, leftleftSize + leftSize);
					memcpy(newByte, leftleftData, leftleftSize);
					memcpy(newByte + leftleftSize, leftData, leftSize);
					newDataID = luka_put_byte(luka, newByte, leftleftSize + leftSize);
				}

				luka_queue_add(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //-
            case LUKA_OPER_MINUS: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) - luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) - luka_get_double(luka, leftID));
                    }
                } else {
                    newDataID = luka_null(luka);
                }

				luka_queue_add(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //>=
            case LUKA_OPER_MOREEQU: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) >= luka_get_int(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) >= luka_get_double(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //>
            case LUKA_OPER_MORE: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) > luka_get_int(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) > luka_get_double(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //<=
            case LUKA_OPER_LESSEQU: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) <= luka_get_int(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) <= luka_get_double(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //<
            case LUKA_OPER_LESS: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) < luka_get_int(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) < luka_get_double(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //==
            case LUKA_OPER_EQU2: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) == luka_get_int(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) == luka_get_double(luka, leftID) ? luka_true(luka) : luka_false(luka);
                    }
                } else if (leftleftID == leftID) {
                    newDataID = luka_true(luka);
                } else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_STRING) {
                    newDataID = strcmp(leftleftID->data_str, leftID->data_str) == 0 ? luka_true(luka) : luka_false(luka);
                } else {
                    newDataID = luka_false(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //!=
            case LUKA_OPER_NOTQEU: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_get_int(luka, leftleftID) == luka_get_int(luka, leftID) ? luka_false(luka) : luka_true(luka);
                    } else {
                        newDataID = luka_get_double(luka, leftleftID) == luka_get_double(luka, leftID) ? luka_false(luka) : luka_true(luka);
                    }
                } else if (leftleftID == leftID) {
                    newDataID = luka_false(luka);
                } else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_STRING) {
                    newDataID = strcmp(leftleftID->data_str, leftID->data_str) == 0 ? luka_false(luka) : luka_true(luka);
                } else if (leftleftID->type == LUKA_BYTE && leftID->type == LUKA_BYTE) {
                    newDataID = memcmp(leftleftID->data_byte.data, leftID->data_byte.data, leftleftID->data_byte.size) == 0 ? luka_false(luka) : luka_true(luka);
                } else {
                    newDataID = luka_true(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //&&
            case LUKA_OPER_AND: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if (leftleftID->type == LUKA_TRUE && leftID->type == LUKA_TRUE) {
                    newDataID = luka_true(luka);
                } else {
                    newDataID = luka_false(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //||
            case LUKA_OPER_OR: {
                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                if (leftleftID->type != LUKA_TRUE) {
                    leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                    if (leftID->type != LUKA_TRUE) {
                        newDataID = luka_false(luka);
                    } else {
                        newDataID = luka_true(luka);
                    }
                } else {
                    newDataID = luka_true(luka);
                }

                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //=
            case LUKA_OPER_EQU: {
                LukaData *bufID = NULL;

                newDataID = leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if (leftleft->type == LUKA_EXP_VAR) {                  //变量赋值
                    bufID = luka_object_get(luka, vars, leftleft->data_var);
                    if (bufID == NULL && (bufID = luka_object_get(luka, luka->mainvars, leftleft->data_var)) != NULL) {    //尝试全局变量
                        luka_object_put(luka, luka->mainvars, leftleft->data_var, newDataID);
                    } else {
                        luka_object_put(luka, vars, leftleft->data_var, newDataID);
                    }
                } else if (leftleft->type == LUKA_EXP_OBJECT) {        //object赋值
                    bufID = luka_object_get(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey);
                    luka_object_put(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey, newDataID);
                } else if (leftleft->type == LUKA_EXP_ARRAY) {         //array赋值
                    if (leftleft->data_arrindex >= 0) {
                        bufID = luka_array_get(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex);
                        luka_array_put(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex, newDataID);
                    } else {
                        luka_array_push(luka, luka_get_array(luka, leftleft->data_p), newDataID);
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                if (bufID) {
                    luka_data_down(luka, bufID);
                    luka_queue_add(luka, bufID);
                }
                luka_data_up(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //'/='
            case LUKA_OPER_DIVIDEQU: {
                LukaData *bufID = NULL;

                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (luka_get_int(luka, leftID) != 0) {
                        if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                            newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) / luka_get_int(luka, leftID));
                        } else {
                            newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) / luka_get_double(luka, leftID));
                        }

                    } else {
                        newDataID = luka_null(luka);

                     }
                } else {
                    newDataID = luka_null(luka);
                }
                

                if (leftleft->type == LUKA_EXP_VAR) {                  //变量赋值
                    bufID = luka_object_get(luka, vars, leftleft->data_var);
                    luka_object_put(luka, vars, leftleft->data_var, newDataID);
                } else if (leftleft->type == LUKA_EXP_OBJECT) {        //object赋值
                    bufID = luka_object_get(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey);
                    luka_object_put(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey, newDataID);
                } else if (leftleft->type == LUKA_EXP_ARRAY) {         //array赋值
                    if (leftleft->data_arrindex >= 0) {
                        bufID = luka_array_get(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex);
                        luka_array_put(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex, newDataID);
                    } else {
                        luka_array_push(luka, luka_get_array(luka, leftleft->data_p), newDataID);
                    }
                } else {
                    newDataID = luka_null(luka);
                }


                if (bufID) {
                    luka_data_down(luka, bufID);
                    luka_queue_add(luka, bufID);
                }
                luka_data_up(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);

            } break;

            //*=
            case LUKA_OPER_TIMESEQU: {
                LukaData *bufID = NULL;

                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) * luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) * luka_get_double(luka, leftID));
                    }
                } else {
                    newDataID = luka_null(luka);
                }

                if (leftleft->type == LUKA_EXP_VAR) {                  //变量赋值
                    bufID = luka_object_get(luka, vars, leftleft->data_var);
                    luka_object_put(luka, vars, leftleft->data_var, newDataID);
                } else if (leftleft->type == LUKA_EXP_OBJECT) {        //object赋值
                    bufID = luka_object_get(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey);
                    luka_object_put(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey, newDataID);
                } else if (leftleft->type == LUKA_EXP_ARRAY) {         //array赋值
                    if (leftleft->data_arrindex >= 0) {
                        bufID = luka_array_get(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex);
                        luka_array_put(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex, newDataID);
                    } else {
                        luka_array_push(luka, luka_get_array(luka, leftleft->data_p), newDataID);
                    }
                } else {
                    newDataID = luka_null(luka);
                }


                if (bufID) {
                    luka_data_down(luka, bufID);
                    luka_queue_add(luka, bufID);
                }
                luka_data_up(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //+=
            case LUKA_OPER_PLUSEQU: {
                LukaData *bufID = NULL;

                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);

                //数字相加
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) + luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) + luka_get_double(luka, leftID));
                    }
                }

                //字符串相加
                else if (leftleftID->type == LUKA_STRING && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
					size_t len1 = 0, len2 = 0;
					char *new_str = NULL;
					
					len1 = strlen(leftleftID->data_str);
					len2 = 64;

					new_str = (char *)luka_malloc(luka, len1 + len2 + 10);
					

					if (leftID->type == LUKA_INT)
						sprintf(new_str, "%s%ld", leftleftID->data_str, leftID->data_i);
					else

						sprintf(new_str, "%s%g", leftleftID->data_str, leftID->data_d);
					newDataID = luka_put_string(luka, new_str);
                }
				

                //字符串相加
				else if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && leftID->type == LUKA_STRING) {
					size_t len1 = 0, len2 = 0;
					char *new_str = NULL;
					len1 = strlen(leftID->data_str);

					len2 = 64;
					new_str = (char *)luka_malloc(luka, len1 + len2 + 10);
					
					if (leftleftID->type == LUKA_INT)
						sprintf(new_str, "%ld%s", leftleftID->data_i, leftID->data_str);
					else
 
						sprintf(new_str, "%g%s", leftleftID->data_d, leftID->data_str);
					newDataID = luka_put_string(luka, new_str);
				}

                
			    //字符串相加
                else if (leftleftID->type == LUKA_STRING && leftID->type == LUKA_STRING) {
                    char *new_str = (char *)luka_malloc(luka, strlen(leftleftID->data_str) + strlen(leftID->data_str) + 10);
                    sprintf(new_str, "%s%s", leftleftID->data_str, leftID->data_str);
                    newDataID = luka_put_string(luka, new_str);
				}
				
                //byte相加
                else if (leftleftID->type == LUKA_BYTE && leftID->type == LUKA_BYTE) {
					unsigned char *leftleftData = NULL, *leftData = NULL;
					size_t leftleftSize = 0, leftSize = 0;
					unsigned char *newByte = NULL;

					leftleftData = luka_get_byte(luka, leftleftID, &leftleftSize);
					leftData = luka_get_byte(luka, leftID, &leftSize);
					newByte = (unsigned char *)luka_malloc(luka, leftleftSize + leftSize);
					memcpy(newByte, leftleftData, leftleftSize);
					memcpy(newByte + leftleftSize, leftData, leftSize);
					newDataID = luka_put_byte(luka, newByte, leftleftSize + leftSize);
				}

                else {
                    newDataID = luka_null(luka);
                }
				

                if (leftleft->type == LUKA_EXP_VAR) {                  //变量赋值
                    bufID = luka_object_get(luka, vars, leftleft->data_var);
                    luka_object_put(luka, vars, leftleft->data_var, newDataID);
                } else if (leftleft->type == LUKA_EXP_OBJECT) {        //object赋值
                    bufID = luka_object_get(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey);
                    luka_object_put(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey, newDataID);
                } else if (leftleft->type == LUKA_EXP_ARRAY) {         //array赋值
                    if (leftleft->data_arrindex >= 0) {
                        bufID = luka_array_get(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex);
                        luka_array_put(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex, newDataID);
                    } else {
                        luka_array_push(luka, luka_get_array(luka, leftleft->data_p), newDataID);
                    }
                } else {
                    newDataID = luka_null(luka);
                }


                if (bufID) {
                    luka_data_down(luka, bufID);
                    luka_queue_add(luka, bufID);
                }
                luka_data_up(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;

            //-=
            case LUKA_OPER_MINUSEQU: {
                LukaData *bufID = NULL;

                leftleftID = luka_expressnode_exec(luka, vars, express_cp, leftleft);
                leftID = luka_expressnode_exec(luka, vars, express_cp, left);
                if ((leftleftID->type == LUKA_INT || leftleftID->type == LUKA_DOUBLE) && (leftID->type == LUKA_INT || leftID->type == LUKA_DOUBLE)) {
                    if (leftleftID->type == LUKA_INT && leftID->type == LUKA_INT) {
                        newDataID = luka_put_int(luka, luka_get_int(luka, leftleftID) - luka_get_int(luka, leftID));
                    } else {
                        newDataID = luka_put_double(luka, luka_get_double(luka, leftleftID) - luka_get_double(luka, leftID));
                    }
                } else {
                    newDataID = luka_null(luka);
                }
                

                if (leftleft->type == LUKA_EXP_VAR) {                  //变量赋值
                    bufID = luka_object_get(luka, vars, leftleft->data_var);
                    luka_object_put(luka, vars, leftleft->data_var, newDataID);
                } else if (leftleft->type == LUKA_EXP_OBJECT) {        //object赋值
                    bufID = luka_object_get(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey);
                    luka_object_put(luka, luka_get_object(luka, leftleft->data_p), leftleft->data_objkey, newDataID);
                } else if (leftleft->type == LUKA_EXP_ARRAY) {         //array赋值
                    if (leftleft->data_arrindex >= 0) {
                        bufID = luka_array_get(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex);
                        luka_array_put(luka, luka_get_array(luka, leftleft->data_p), leftleft->data_arrindex, newDataID);
                    } else {
                        luka_array_push(luka, luka_get_array(luka, leftleft->data_p), newDataID);
                    }
                } else {
                    newDataID = luka_null(luka);
                }


                if (bufID) {
                    luka_data_down(luka, bufID);
                    luka_queue_add(luka, bufID);
                }
                luka_data_up(luka, newDataID);
                luka_express_rpn_update(luka, express_cp, leftleft, newDataID);
                luka_express_rpn_rmv(luka, express_cp, left);
                luka_express_rpn_rmv(luka, express_cp, oper);
            } break;
        }
    }

    dataID = luka_expressnode_exec(luka, vars, express_cp, express_cp->RPN);
    luka_express_destroy(luka, express_cp);
    return dataID;
}

static voidp luka_expressnode_exec (Luka *luka, LukaObject *vars, LukaExpress *express, LukaExpressNode *node) {
    voidp dataID = luka_null(luka);

    if (node->type == LUKA_EXP_DATA) {
        dataID = node->data_p;
    } else if (node->type == LUKA_EXP_VAR) {
        dataID = luka_object_get(luka, vars, node->data_var);
        if (dataID == NULL) {  //调用全局变量
            dataID = luka_object_get(luka, luka->mainvars, node->data_var);
        }
        dataID = dataID != NULL ? dataID : luka_null(luka);
    } else if (node->type == LUKA_EXP_FUNC) {
        size_t i = 0;
        voidp *func_param = NULL;
        size_t  func_len = 0;

        if (node->data_param) {
            func_len = node->data_len;
            func_param = (voidp *)luka_malloc(luka, sizeof(voidp) * func_len);
            for (i = 0; i < node->data_len; i++) {
                func_param[i] = luka_express_exec(luka, vars, node->data_param[i]);
            }
        }

        dataID = luka_call_func(luka, node->data_func, func_param, func_len);

        if (func_param) {
			for (i = 0; i < node->data_len; i++) {
				luka_queue_add(luka, func_param[i]);
			}
        	luka_free(luka, func_param);
        }
    } else if (node->type == LUKA_EXP_OBJECT) {
        dataID = luka_object_get(luka, luka_get_object(luka, node->data_p), node->data_objkey);
        dataID = dataID != NULL ? dataID : luka_null(luka);
    } else if (node->type == LUKA_EXP_ARRAY) {
        dataID = luka_array_get(luka, luka_get_array(luka, node->data_p), node->data_arrindex);
        dataID = dataID != NULL ? dataID : luka_null(luka);
    }
    luka_queue_add(luka, dataID);
    return dataID;
}

/***********************************************************
  调用函数 
***********************************************************/

/** 检查a/b是否为同一if/else **/
static int luka_func_check_if (LukaCode *a, LukaCode *b) {
    if (a == b)
        return 1;

    while (b->type != LUKA_IF) {
        b = b->last->jump;
        if (a == b) return 1;
    }
    return 0;
}

static voidp luka_data_up2 (Luka *luka, voidp p) {
    luka_data_up(luka, p);
    return p;
}

static voidp luka_func_call (Luka *luka, const char *func_name, voidp *p, size_t n) {
    size_t i = 0;

    LukaFunc   *func   = NULL;
    LukaCode   *mov    = NULL;
    LukaObject *vars   = NULL;
    LukaStack  *stack  = NULL;
    LukaIWF    *iwf    = NULL;
    voidp       dataID = luka_null(luka);
    LukaIWF    *buf    = NULL;

    int ifwhile_flag   = 0;

    vars  = luka_object_create(luka);
    func  = luka_object_get(luka, luka->funcs, func_name);
    stack = luka_stack_create(luka);

    /** 虚拟主函数 **/
    if (strcmp(func_name, LUKA_MAIN) == 0) {
        luka->mainvars = vars;

        luka_reg_var(luka, "PATH", luka_put_string(luka, luka->mainpath));

        //注册系统全局变量
        for (i = 0; g_Package[i].pkg_name; i++) {
            if (g_Package[i].pkg_load == 1 && g_Package[i].pkg_p2) {
                g_Package[i].pkg_p2(luka);
            }
        }
    }

    if (!func)
        return luka_null(luka);

    //形参
    if (func->func_param && p) {
        for (i = 0; i < func->func_len && i < n; i++) {
             luka_object_put(luka, vars, func->func_param[i], p[i]);
             luka_data_up(luka, p[i]);
        }
    }

    mov = func->codes_head;
    while (mov) {
        //表达式
        if (mov->type == LUKA_EXPRESS) {
            dataID = luka_express_exec(luka, vars, mov->express);
			luka_queue_check(luka);
            mov = mov->next;
        }
        
        //return
        else if (mov->type == LUKA_RETURN) {
            dataID = luka_express_exec(luka, vars, mov->express);
            luka_object_destroy(luka, vars);
            luka_stack_destroy(luka, stack);
            return dataID;
        }
        
        //if
        else if (mov->type == LUKA_IF) {
            dataID = luka_express_exec(luka, vars, mov->express);
            luka_queue_check(luka);
            if (dataID == luka_true(luka)) {
                iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                iwf->p = mov;
                luka_stack_push(luka, stack, iwf);
                mov = mov->next;
            } else {
                mov = mov->jump;
            }
        }

        //else if
        else if (mov->type == LUKA_ELSEIF) {
            if ((buf = luka_stack_top(luka, stack)) != NULL && luka_func_check_if(buf->p, mov)) {
                mov = mov->jump;
            } else {
                dataID = luka_express_exec(luka, vars, mov->express);
                luka_queue_check(luka);
                if (dataID == luka_true(luka)) {
                    iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                    iwf->p = mov;
                    luka_stack_push(luka, stack, iwf);
                    mov = mov->next;
                } else {
                    mov = mov->jump;
                }
            }
        }
        
        //else
        else if (mov->type == LUKA_ELSE) {
            if ((buf = luka_stack_top(luka, stack)) != NULL && luka_func_check_if(buf->p, mov)) {
                mov = mov->jump;
            } else {
                iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                iwf->p = mov;
                luka_stack_push(luka, stack, iwf);
                mov = mov->next;
            }
        } 
        
        //while
        else if (mov->type == LUKA_WHILE) {
            dataID = luka_express_exec(luka, vars, mov->express);
            luka_queue_check(luka);
            if (dataID == luka_true(luka)) {
                //第一次进入循环
                if ((buf = luka_stack_top(luka, stack)) == NULL || buf->p != mov) {
                    iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                    iwf->p = mov;
                    luka_stack_push(luka, stack, iwf);
                }
                mov = mov->next;
            } else {
                if ((buf = luka_stack_top(luka, stack)) != NULL && buf->p == mov) {
                    luka_stack_pop(luka, stack);
                    luka_free(luka, buf);
                }
                mov = mov->jump;
            }
        }

        //for
        else if (mov->type == LUKA_FOR) {
            //第一次进入循环
            if ((buf = luka_stack_top(luka, stack)) == NULL || buf->p != mov) {
                if (mov->a) {
                    dataID = luka_express_exec(luka, vars, mov->a);
                    luka_queue_check(luka);
                }
            }

            if (mov->b) {
                dataID = luka_express_exec(luka, vars, mov->b);
                luka_queue_check(luka);
                if (dataID != luka_true(luka)) {
                    if ((buf = luka_stack_top(luka, stack)) != NULL && buf->p == mov) {
                        luka_stack_pop(luka, stack);
                        luka_free(luka, buf);
                    }
                    mov = mov->jump;
                } else {
                    if ((buf = luka_stack_top(luka, stack)) == NULL || buf->p != mov) {
                        iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                        iwf->p = mov;
                        luka_stack_push(luka, stack, iwf);
                    }
                    mov = mov->next;
                }
            } else {
                if ((buf = luka_stack_top(luka, stack)) == NULL || buf->p != mov) {
                    iwf = (LukaIWF *)luka_malloc(luka, sizeof(LukaIWF));
                    iwf->p = mov;
                    luka_stack_push(luka, stack, iwf);
                }
                mov = mov->next;
            }
        }

        //end
        else if (mov->type == LUKA_END) {
            //while end
            if (mov->jump->type == LUKA_WHILE) {
                if ((buf = luka_stack_top(luka, stack)) != NULL && buf->p == mov->jump) {
                    mov = mov->jump;
                } else {
                    mov = mov->next;
                }
                luka_queue_check(luka);
            }

            //for end
            else if (mov->jump->type == LUKA_FOR) {
                if ((buf = luka_stack_top(luka, stack)) != NULL && buf->p == mov->jump) {
                    mov = mov->jump;

                    //执行for->c
                    if (mov->c) {
                        dataID = luka_express_exec(luka, vars, mov->c);
                        luka_queue_check(luka);
                    }
                } else {
                    mov = mov->next;
                }
            }

            //if/else end
            else {
                mov = mov->next;
                if (mov && mov->type != LUKA_ELSEIF && mov->type != LUKA_ELSE) {
                    if ((buf = luka_stack_top(luka, stack)) != NULL && luka_func_check_if(buf->p, mov->last->jump)) {
                        luka_stack_pop(luka, stack);
                        luka_free(luka, buf);
                    }
                }
            }
        }

        //break
        else if (mov->type == LUKA_BREAK) {
            LukaIWF *wi_mov = luka_stack_top(luka, stack);
            while (wi_mov->p->type != LUKA_FOR && wi_mov->p->type != LUKA_WHILE) {
                luka_free(luka, wi_mov);
                luka_stack_pop(luka, stack);
                wi_mov = luka_stack_top(luka, stack);
            }

            mov = wi_mov->p->jump;
            luka_free(luka, wi_mov);
            luka_stack_pop(luka, stack);
        }

        //continue
        else if (mov->type == LUKA_CONTINUE) {
            LukaIWF *wi_mov = luka_stack_top(luka, stack);
            while (wi_mov->p->type != LUKA_FOR && wi_mov->p->type != LUKA_WHILE) {
                luka_free(luka, wi_mov);
                luka_stack_pop(luka, stack);
                wi_mov = luka_stack_top(luka, stack);
            }

            mov = wi_mov->p->jump;
            luka_free(luka, wi_mov);
        }
    }

    if (strcmp(func_name, LUKA_MAIN) != 0) {
        luka_object_destroy(luka, vars);
    }
    luka_stack_destroy(luka, stack);
    return luka_null(luka);
}

/** 调用C/luka函数 **/
voidp luka_call_func (Luka *luka, const char *func_name, voidp *p, size_t n) {
	voidp ret = NULL;
    voidp (*func_p)(Luka *, voidp *p, size_t n) = NULL;

    func_p = luka_object_get(luka, luka->cs, func_name);
    if (func_p) {
    	ret = func_p(luka, p, n);
    } else {
    	ret = luka_func_call(luka, func_name, p, n);
    }
    return ret;
}

/***********************************************************
  执行部分 
***********************************************************/

static void luka_exec (Luka *luka, int argc, char *argv[]) {
    LukaFunc *main = NULL;
    voidp *func_param = NULL;
    size_t i = 0, func_len = 0;

    luka_func_call(luka, LUKA_MAIN, NULL, 0);

    main = luka_object_get(luka, luka->funcs, LUKA_MAIN2);
    if (!main)
        return;

    if (argc >= 3) {
        func_len = argc - 2;
        func_param = (voidp *)luka_malloc(luka, sizeof(voidp) * func_len);
        for (i = 0; i < func_len; i++) {
            func_param[i] = luka_put_string(luka, luka_strdup(luka, argv[i + 2]));
			luka_data_up(luka, func_param[i]);
        }
    }

    luka_func_call(luka, LUKA_MAIN2, func_param, func_len);
}

/***********************************************************
  主函数 
***********************************************************/

#ifdef CONFIG_MAIN
int main (int argc, char *argv[]) {
#else
int luka_main (int argc, char *argv[]) {
#endif
    Luka luka;

    if (argc == 1) {
        fprintf(stdout, "luka: try 'luka xxx.luka'\n");
        return 0;
    }

    memset(&luka, 0, sizeof(luka));

    //设置错误返回点
    if ((luka.ret = setjmp(luka.jump)) != LUKA_OK) {
        luka_destroy(&luka);
        fprintf(stderr, "luka: error %d\n", luka.ret);
        return -1;
    }

    //准备环境
    luka_create(&luka);

    //解析代码
    luka_parse(&luka, argv[1]);

    //执行代码
    luka_exec(&luka, argc, argv);

    //垃圾回收
    luka_destroy(&luka);
    return 1;
}
