#ifndef C_HTTP_H
#define C_HTTP_H

#define CHTTP_Use_Agent       "User-Agent: chttp/1.0\r\n"
#define CHTTP_Boundary        "--FormBoundaryHATSUNEMIKU20070831"

#ifdef __cplusplus
extern "C" {
#endif

// +-------------------------------------------------------
// | http/https 
// +-------------------------------------------------------

#define CHTTP_OK                        0
#define CHTTP_MEM_ERR                   1           //内存报错
#define CHTTP_URL_ERR                   2           //无法解析的URL
#define CHTTP_HTTPS_NOTSUP              3           //不支持HTTPS
#define CHTTP_DNS_ERR                   4           //dns失败
#define CHTTP_TIMEOUT                   5           //超时
#define CHTTP_SEND_ERR                  6           //发送失败
#define CHTTP_RECV_ERR                  7           //接收失败
#define CHTTP_PROXY_ERR                 8           //无法连接HTTP代理服务器
#define CHTTP_HTTPS_INIT                9           //SSL握手失败
#define CHTTP_NOT_200                  10           //非HTTP200
#define CHTTP_FILE_ERR                 11           //文件错误

typedef enum {
    C_HTTP_GET = 1, C_HTTP_POST = 2
} cHTTPFormMethod;

typedef struct cHTTPFormNetHTTPProxy {
    const char *ip;
    int port;
    char *auth;
} cHTTPFormNetHTTPProxy;

typedef struct cHTTPForm cHTTPForm;

/** 创建chttp表单 **/
extern cHTTPForm *cHTTPForm_Create(const char *url);

extern void cHTTPForm_Free(cHTTPForm **form);

/** 设置GET/POST **/
extern void cHTTPForm_SetMethod(cHTTPForm *form, cHTTPFormMethod method);

/** 设置超时时间 **/
extern void cHTTPForm_SetTimeOut(cHTTPForm *form, size_t timeout);

/** 设置自定义头部 **/
extern void cHTTPForm_SetDiy(cHTTPForm *form, const char *diy);

/** 设置HTTP代理服务器 **/
extern void cHTTPForm_SetHTTPProxy(cHTTPForm *form, const char *ip, int port, const char *user, const char *pwd);

/** 设置回调函数 **/
extern void cHTTPForm_SetCallBack(cHTTPForm *form, void *p, void (*cb)(void *p, unsigned char *data, size_t size));

/** 设置HTTP头回调函数 **/
extern void cHTTPForm_SetHeadCallBack(cHTTPForm *form, void *p, void (*cb)(void *p, const char *temp));

// +-------------------------------------------------------
// | POST数据4种提交方式 
// | 以下四种数据设置方式只能存在一种, 后设置的将覆盖 
// | 前设置的 
// +-------------------------------------------------------

/** 1. Content-Type: text/plain 原始数据 **/
extern void cHTTPForm_SetPostData_RAW(cHTTPForm *form, const char *data);

/** 2. Content-Type: application/x-www-form-urlencoded 非ASCALL进行编码 **/
/** 注： 内部不提供URL编码，如果需要编码请传入编码后的字符串 **/
extern void cHTTPForm_SetPostData_KV(cHTTPForm *form, const char *data);

typedef struct cHTTPFormPostData {
    int type;    // [0=>数据,1=>文件]
    const char *name;

    // 数据
    const char *data;

    // 文件
    const char *fileName;
    FILE *fileHandle;
    const char *contentType;
} cHTTPFormPostData;

/** 3. Content-Type: multipart/form-data 原始数据 + 文件 **/
extern void cHTTPForm_SetPostData_Data(cHTTPForm *form, cHTTPFormPostData *data, size_t size);

/** 4. Content-Type: xxx 原始数据 **/
extern void cHTTPForm_SetPostData_Raw2(cHTTPForm *form, const char *contentType, const char *data);

/** 提交表单 **/
extern int cHTTPForm_Submit(cHTTPForm *form);

// +-------------------------------------------------------
// | function 
// +-------------------------------------------------------

/** 获得网页Text **/
extern char *cHTTP_Get(const char *url);

/** 获得网页Text (使用HTTP代理) **/
extern char *cHTTP_Get_Proxy(const char *url, const cHTTPFormNetHTTPProxy *proxy);

/** 下载网页 **/
extern int cHTTP_Down(const char *url, const char *file);

/** 下载网页 (使用HTTP代理) **/
extern int cHTTP_Down_Proxy(const char *url, const char *file, const cHTTPFormNetHTTPProxy *proxy);

#ifdef __cplusplus
}
#endif

#endif

