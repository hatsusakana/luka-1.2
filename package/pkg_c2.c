/***********************************************************
  pkg_c2.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "../luka.h"
#include "library/md5/md5.h"
#include "library/cJson/cJson.h"
#include "library/chttp/chttp.h"

/***********************************************************
  �ַ��� 
***********************************************************/

/** splite(�ַ����ָ�) **/
static voidp luka_c2_splite (Luka *luka, voidp *p, size_t n) {
    const char *s = NULL, *o = NULL, *mov = NULL;

    LukaArray *sarray = NULL;
    voidp sarray_p = NULL;

    char *bufstr = NULL;
    voidp bufstr_p = NULL;

    if (n < 2 || !luka_is_string(p[0]) || !luka_is_string(p[1]))
        return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    o = luka_get_string(luka, p[1]);
    if (*s == 0 || *o == 0)
        return luka_null(luka);

    sarray_p = luka_put_array(luka);
    sarray = luka_get_array(luka, sarray_p);

    mov = s;
    while ((mov = strstr(mov, o)) != NULL) {
        if (mov == s) {
            bufstr = luka_strdup(luka, "\0");
        } else {
            bufstr = luka_malloc(luka, mov - s + 1);
            memcpy(bufstr, s, mov - s);
        }
        bufstr_p = luka_put_string(luka, bufstr);
        luka_array_push(luka, sarray, bufstr_p);
        luka_data_up(luka, bufstr_p);
        mov += strlen(o);
        s = mov;
    }
    bufstr = luka_strdup(luka, s);
    bufstr_p = luka_put_string(luka, bufstr);
    luka_array_push(luka, sarray, bufstr_p);
    luka_data_up(luka, bufstr_p);
    return sarray_p;
}

/** substr **/
static voidp luka_c2_substr (Luka *luka, voidp *p, size_t n) {
    const char *s = NULL;
    long start = 0, length = 0;

    char *d = NULL;
    size_t dn = 0;

    if (n < 3 || !luka_is_string(p[0]) || !luka_is_int(p[1]) || !luka_is_int(p[2]))
        return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    start = luka_get_int(luka, p[1]);
    length = luka_get_int(luka, p[2]);
    if (start < 0 || length <= 0)
        return luka_null(luka);

    dn = length + 2;
    d = (char *)luka_malloc(luka, dn);
    memcpy(d, s + start, length);
    return luka_put_string(luka, d);
}

/** strrev(��ת�ַ���) **/
static voidp luka_c2_strrev (Luka *luka, voidp *p, size_t n) {
    const char *s = NULL;
    char *d = NULL;
    size_t dn = 0;

    const char *s_mov = NULL;
    char *d_mov = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    if (*s == 0)
        return p[0];

    dn = strlen(s) + 2;
    d = (char *)luka_malloc(luka, dn);
    s_mov = s + strlen(s) - 1;
    d_mov = d;
    while (s_mov >= s) {
        *d_mov++ = *s_mov--;
    }
    *d_mov = 0;
    return luka_put_string(luka, d);
}

/** strupr **/
static voidp luka_c2_strupr (Luka *luka, voidp *p, size_t n) {
	char *s = NULL, *orign = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

	s = luka_strdup(luka, luka_get_string(luka, p[0]));
	for (orign = s; *orign != 0; orign++)
		*orign = toupper(*orign);
	return luka_put_string(luka, s);
}

/** strlowr **/
static voidp luka_c2_strlowr (Luka *luka, voidp *p, size_t n) {
	char *s = NULL, *orign = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

	s = luka_strdup(luka, luka_get_string(luka, p[0]));
	for (orign = s; *orign != 0; orign++)
		*orign = tolower(*orign);
	return luka_put_string(luka, s);
}

/** basename **/
static voidp luka_c2_basename (Luka *luka, voidp *p, size_t n) {
    const char *s = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    if (*s == 0)
		return luka_null(luka);

#ifdef _WIN32
    s = strrchr(s, '\\');
#else
    s = strrchr(s, '/');
#endif

    if (!s)
        return p[0];

    return luka_put_string(luka, luka_strdup(luka, s + 1));
}

/** dirname **/
static voidp luka_c2_dirname (Luka *luka, voidp *p, size_t n) {
    const char *s = NULL;
    char *d = NULL, *dp = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    if (*s == 0)
		return luka_null(luka);

    d = luka_strdup(luka, s);
#ifdef _WIN32
    dp = strrchr(d, '\\');
#else
    dp = strrchr(d, '/');
#endif

    if (!dp) {
        luka_free(luka, d);
        return luka_null(luka);
    }

    *dp = 0;
    return luka_put_string(luka, d);
}

static char *csv_substr (Luka *luka, const char *left, const char *right) {
    size_t size = right - left;
    char *str = NULL;

    str = (char *)luka_malloc(luka, size + 2);
    if (size != 0)
        memcpy(str, left, size);
    return str;
}

/** �޳���β [�ո�,\t,\r,\n,\x0B] **/
static char *_trim (char *s) {
    char *p = NULL;

    if (s == NULL || strlen(s) == 0) return s;

    p = s + strlen(s) - 1;
    while (*p == ' ' || *p == '\t' || *p == '\r' || *p == '\n' || *p == '\x0B')
        *p-- = '\0';

    p = s;
    while (*p == ' ' || *p == '\t' || *p == '\r' || *p == '\n' || *p == '\x0B')
        p++;

    if (p != s) {
        memmove(s, p, strlen(p) + 1);
    }

    return s;
}

/** ����ת�� **/
static void csv_convert (char *p) {
    _trim(p);

    if (*p != '\"')
        return;
    
    memmove(p, p + 1, strlen(p + 1) + 1);
    if (p[strlen(p) - 1] == '\"')
        p[strlen(p) - 1] = 0;

    while (*p != 0) {
        if (*p == '\"') {
            memmove(p, p + 1, strlen(p + 1) + 1);
        }
        p++;
    }
}

/** ������ **/
static voidp csv_decode_column (Luka *luka, const char *line) {
    int convert_flag = 0;
    const char *left = line, *right = line;
    char *strbuf = NULL;
    voidp pbuf = NULL;

    LukaArray *csv_array = NULL;
    voidp csv_array_p = NULL;

    csv_array_p = luka_put_array(luka);
    csv_array = luka_get_array(luka, csv_array_p);

    while (*right != 0) {
        if (convert_flag == 0) {
            if (*right == '"') {
                convert_flag = 1;
            } else if (*right == ',') {
                strbuf = csv_substr(luka, left, right);
                csv_convert(strbuf);
                left = right + 1;
                pbuf = luka_put_string(luka, strbuf);
                luka_array_push(luka, csv_array, pbuf);
                luka_data_up(luka, pbuf);
            }
        } else {
            if (*right == '\"' && *(right + 1) == '\"') {
                right++;
            } else if (*right == '\"') {
                convert_flag = 0;
            }
        }
        right++;
    }

    strbuf = luka_strdup(luka, left);
    csv_convert(strbuf);
    pbuf = luka_put_string(luka, strbuf);
    luka_array_push(luka, csv_array, pbuf);
    luka_data_up(luka, pbuf);
    return csv_array_p;
}

/** ������ **/
static void csv_decode_line (Luka *luka, const char *s, LukaArray *csv_array) {
    int convert_flag = 0;
    const char *left = s, *right = s;
    char *strbuf = NULL;
    voidp pbuf = NULL;

    while (*right != 0) {
        if (convert_flag == 0) {
            if (*right == '"') {
                convert_flag = 1;
            } else if (*right == '\n') {
                strbuf = csv_substr(luka, left, right);
                left = right + 1;
                pbuf = csv_decode_column(luka, strbuf);
                luka_array_push(luka, csv_array, pbuf);
                luka_data_up(luka, pbuf);
            }
        } else {
            if (*right == '\"' && *(right + 1) == '\"') {
                right++;
            } else if (*right == '\"') {
                convert_flag = 0;
            }
        }
        right++;
    }

    strbuf = luka_strdup(luka, left);
    pbuf = csv_decode_column(luka, strbuf);
    luka_array_push(luka, csv_array, pbuf);
    luka_data_up(luka, pbuf);
}

/** csv_decode **/
static voidp luka_c2_csv_decode (Luka *luka, voidp *p, size_t n) {
    LukaArray *csv_array = NULL;
    voidp csv_array_p = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    csv_array_p = luka_put_array(luka);
    csv_array = luka_get_array(luka, csv_array_p);
    csv_decode_line(luka, luka_get_string(luka, p[0]), csv_array);
    return csv_array_p;
}

/***********************************************************
  �ļ�  
***********************************************************/

/** readtxt **/
static voidp luka_c2_readtxt (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *filepath = NULL;
    char *txtdata = NULL;
    long filesize = 0;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    filepath = luka_get_string(luka, p[0]);
    if (*filepath == 0)
		return luka_null(luka);

    fp = fopen(filepath, "r");
    if (!fp)
		return luka_null(luka);

    fseek(fp, 0, SEEK_END);
    filesize = ftell(fp);
    fclose(fp);

    txtdata = (char *)luka_malloc(luka, filesize + 2);
    fp = fopen(filepath, "r");
    if (!fp) {
        luka_free(luka, txtdata);
		return luka_null(luka);
    }
    fread(txtdata, 1, filesize, fp);
    fclose(fp);
    return luka_put_string(luka, txtdata);
}

/** writetxt **/
static voidp luka_c2_writetxt (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *str = NULL, *file = NULL;
    size_t len = 0;

	if (n < 1 || !luka_is_string(p[0])|| !luka_is_string(p[1]))
		return luka_null(luka);

    str = luka_get_string(luka, p[0]);
    file = luka_get_string(luka, p[1]);
    if (*file == 0)
		return luka_null(luka);

    fp = fopen(file, "w+");
    if (!fp)
		return luka_null(luka);

    len = strlen(str);
    if (len != 0) {
        fwrite(str, 1, len, fp);
    }
    
    fclose(fp);
    return luka_true(luka);
}

/** readfile **/
static voidp luka_c2_readfile (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *filepath = NULL;
    unsigned char *filedata = NULL;
    long filesize = 0;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    filepath = luka_get_string(luka, p[0]);
    if (*filepath == 0)
		return luka_null(luka);

    fp = fopen(filepath, "rb");
    if (!fp)
		return luka_null(luka);

    fseek(fp, 0, SEEK_END);
    filesize = ftell(fp);
    fclose(fp);

    if (filesize == 0) {
        return luka_put_byte(luka, NULL, 0);
    }
    
    filedata = (unsigned char *)luka_malloc(luka, filesize);
    fp = fopen(filepath, "rb");
    if (!fp) {
        luka_free(luka, filedata);
		return luka_null(luka);
    }
    fread(filedata, 1, filesize, fp);
    fclose(fp);
    return luka_put_byte(luka, filedata, filesize);
}

/** writefile **/
static voidp luka_c2_writefile (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *file = NULL;
    unsigned char *data = NULL;
    size_t size = 0;

	if (n < 1 || !luka_is_byte(p[0])|| !luka_is_string(p[1]))
		return luka_null(luka);

    data = (unsigned char *)luka_get_byte(luka, p[0], &size);
    file = luka_get_string(luka, p[1]);
    if (*file == 0)
		return luka_null(luka);

    fp = fopen(file, "wb+");
    if (!fp)
		return luka_null(luka);

    if (size != 0) {
        fwrite(data, 1, size, fp);
    }
    
    fclose(fp);
    return luka_true(luka);
}

/***********************************************************
  ����  
***********************************************************/

static const char *ALPHA_BASE64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/** base64_encode **/
static voidp luka_c2_base64_encode (Luka *luka, voidp *p, size_t n) {
    unsigned char *in_data = NULL;
    size_t i = 0, j = 0, in_size = 0;

    char buf[3] = {0}, *dst = NULL;
    size_t size = 0;

	if (n < 1 || (!luka_is_string(p[0]) && !luka_is_byte(p[0])))
		return luka_null(luka);
    
    if (luka_is_string(p[0])) {
        const char *s = luka_get_string(luka, p[0]);
        in_data = (unsigned char *)s;
        in_size = strlen(s);
    } else {
        in_data = luka_get_byte(luka, p[0], &in_size);
    }

    if (in_size == 0)
        return luka_null(luka);

    size = (in_size%3 == 0 ? in_size/3 : in_size/3 + 1) * 4 + 10;
    dst = (char *)luka_malloc(luka, size);

    for (i = 0, j = 0; i < in_size; ) {
        buf[0] = in_data[i++];
        buf[1] = i < in_size ? in_data[i++] : '\0';
        buf[2] = i < in_size ? in_data[i++] : '\0';
        
        dst[j++] = ALPHA_BASE64[(buf[0] >> 2) & 0x3F];
        dst[j++] = ALPHA_BASE64[((buf[0] << 4) | ((buf[1] & 0xFF) >> 4)) & 0x3F];
        dst[j++] = ALPHA_BASE64[((buf[1] << 2) | ((buf[2] & 0xFF) >> 6)) & 0x3F];
        dst[j++] = ALPHA_BASE64[buf[2] & 0x3F];
    }
    
    switch (in_size % 3) {
        case 1: dst[--j] = '=';
        case 2: dst[--j] = '=';
    }
    return luka_put_string(luka, dst);
}

/** base64_decode **/
static voidp luka_c2_base64_decode (Luka *luka, voidp *p, size_t n) {
    size_t i = 0, j = 0, len = 0;
    char buf[4] = {0};
    int toInt[128] = {-1};

    const char *s = NULL;
    unsigned char *dst = NULL;
    size_t size = 0;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    s = luka_get_string(luka, p[0]);
    if (*s == 0)
		return luka_null(luka);

    len = strlen(s);
    size = len/4*3 + 1;
    dst = (unsigned char *)luka_malloc(luka, size);

    for (i = 0; i < 64; i++)
        toInt[ALPHA_BASE64[i]] = i;

    for (i = 0, j = 0; i < len; i += 4) {
        buf[0] = toInt[s[i]];
        buf[1] = toInt[s[i + 1]];
        dst[j++] = (((buf[0] << 2) | (buf[1] >> 4)) & 0xff);
        buf[2] = toInt[s[i + 2]];
        dst[j++] = (((buf[1] << 4) | (buf[2] >> 2)) & 0xff);
        buf[3] = toInt[s[i + 3]];
        dst[j++] = (((buf[2] << 6) | buf[3]) & 0xff);
    }
    return luka_put_byte(luka, dst, size);
}

/** xor_encryp(���ӽ���) **/
static voidp luka_c2_xor_encryp (Luka *luka, voidp *p, size_t n) {
    unsigned char *in_data = NULL, *out_data = NULL;
    size_t i = 0, in_size = 0;

    const char *secert = NULL;
    size_t secert_len = 0;

	if (n < 2 || (!luka_is_string(p[0]) && !luka_is_byte(p[0])) || !luka_is_string(p[1]))
		return luka_null(luka);

    secert = luka_get_string(luka, p[1]);
    if (*secert == 0)
        return luka_null(luka);
    secert_len = strlen(secert);

    if (luka_is_string(p[0])) {
        const char *s = luka_get_string(luka, p[0]);
        if (*s == 0)
            return luka_null(luka);

        in_data = (unsigned char *)s;
        in_size = strlen(s);
    } else {
        in_data = luka_get_byte(luka, p[0], &in_size);
        if (in_size == 0)
            return luka_null(luka);
    }

    out_data = (unsigned char *)luka_malloc(luka, in_size);
    for (i = 0; i < in_size; i++) {
        out_data[i] = in_data[i] ^ secert[i % secert_len];
    }
    out_data[i] = 0;
    return luka_put_byte(luka, out_data, in_size);
}

/** md5 **/
static voidp luka_c2_md5 (Luka *luka, voidp *p, size_t n) {
    unsigned char *in_data = NULL;
    size_t i = 0, j = 0, in_size = 0;

    char buf[3] = {0}, *dst = NULL;
    size_t size = 0;

    MD5_CTX md5 = {0};
    unsigned char decrypt[16] = {0};
	char result[128] = {0};

	if (n < 1 || (!luka_is_string(p[0]) && !luka_is_byte(p[0])))
		return luka_null(luka);
    
    if (luka_is_string(p[0])) {
        const char *s = luka_get_string(luka, p[0]);
        in_data = (unsigned char *)s;
        in_size = strlen(s);
    } else {
        in_data = luka_get_byte(luka, p[0], &in_size);
    }

    if (in_size == 0)
        return luka_null(luka);

	MD5Init(&md5);
    MD5Update(&md5, in_data, in_size);
    MD5Final(&md5, decrypt);

    for (i = 0; i < 16; i++) {
        memset(buf, 0, sizeof(buf));
        sprintf(buf, "%02x", decrypt[i]);
        strncat(result, buf, sizeof(result) - 1);
    }

	return luka_put_string(luka, luka_strdup(luka, result));
}

/** _StrBuf **/
typedef struct _StrBuf {
	char *s;
	struct _StrBuf *tail;
	struct _StrBuf *next;
} _StrBuf;

static void _sb_add (Luka *luka, _StrBuf **sb, const char *s) {
	if (*sb == NULL) {
		*sb = (_StrBuf *)luka_malloc(luka, sizeof(_StrBuf));
		(*sb)->s = luka_strdup(luka, s);
		(*sb)->tail = *sb;
		(*sb)->next = NULL;
	} else {
		(*sb)->tail->next = (_StrBuf *)luka_malloc(luka, sizeof(_StrBuf));
		(*sb)->tail->next->s = luka_strdup(luka, s);
		(*sb)->tail->next->next = NULL;
		(*sb)->tail = (*sb)->tail->next;
	}
}

static char *_sb_tostring (Luka *luka, _StrBuf *sb) {
	_StrBuf *mov = NULL;
	char *retData = NULL;
	int retLen = 10;

	mov = sb;
	while (mov) {
		retLen += strlen(mov->s);
		mov = mov->next;
	}

	retData = (char *)luka_malloc(luka, retLen);

	mov = sb;
	while (mov) {
		strcat(retData, mov->s);
		mov = mov->next;
	}
	return retData;

}

static void _sb_free (Luka *luka, _StrBuf **sb) {
	_StrBuf *mov = *sb, *buf = NULL;

	if (!sb || !*sb)
		return;

	while (mov) {
		buf = mov;
		mov = mov->next;

		luka_free(luka, buf->s);
		luka_free(luka, buf);
	}

	*sb = NULL;
}
/** _StrBuf Over **/

static void luka_c2_json_encode_ex     (Luka *luka, voidp data, _StrBuf **sb);
static void luka_c2_json_encode_object (Luka *luka, LukaObject *object, _StrBuf **sb);
static void luka_c2_json_encode_array  (Luka *luka, LukaArray *array, _StrBuf **sb);

void luka_c2_json_encode_object_proc (Luka *luka, LukaObject *obj, void *p, const char *key, voidp value) {
	_sb_add(luka, (_StrBuf **)p, key);
}

static void luka_c2_json_encode_object (Luka *luka, LukaObject *object, _StrBuf **sb) {
	_StrBuf *index = NULL, *mov = NULL;
	
	luka_object_each(luka, object, &index, luka_c2_json_encode_object_proc);
	_sb_add(luka, sb, "{");
	if (index != NULL) {
		mov = index;
		while (mov) {
			_sb_add(luka, sb, "\"");
			_sb_add(luka, sb, mov->s);
			_sb_add(luka, sb, "\":");
			luka_c2_json_encode_ex(luka, luka_object_get(luka, object, mov->s), sb);
			mov = mov->next;
			if (mov) _sb_add(luka, sb, ",");
		}
	}
	_sb_add(luka, sb, "}");
	_sb_free(luka, &index);
}

static void luka_c2_json_encode_array (Luka *luka, LukaArray *array, _StrBuf **sb) {
	size_t i = 0;

	_sb_add(luka, sb, "[");
	for (i = 0; i < luka_array_length(luka, array); i++) {
		luka_c2_json_encode_ex(luka, luka_array_get(luka, array, i), sb);
		if (i + 1 < luka_array_length(luka, array)) _sb_add(luka, sb, ",");
	}
	_sb_add(luka, sb, "]");
}

static void luka_c2_json_encode_ex (Luka *luka, voidp data, _StrBuf **sb) {
	char buf[128] = {0};

	if (luka_is_null(data)) {
		_sb_add(luka, sb, "null");
	} else if (luka_is_true(data)) {
		_sb_add(luka, sb, "true");
	} else if (luka_is_false(data)) {
		_sb_add(luka, sb, "false");
	} else if (luka_is_int(data)) {
		sprintf(buf, "%d", luka_get_int(luka, data));
		_sb_add(luka, sb, buf);
	} else if (luka_is_double(data)) {
		sprintf(buf, "%g", luka_get_double(luka, data));
		_sb_add(luka, sb, buf);
	} else if (luka_is_string(data)) {
		sprintf(buf, "%s", luka_get_string(luka, data));
		_sb_add(luka, sb, buf);
	} else if (luka_is_object(data)) {
		luka_c2_json_encode_object(luka, luka_get_object(luka, data), sb);
	} else if (luka_is_array(data)) {
		luka_c2_json_encode_array(luka, luka_get_array(luka, data), sb);
	}
}

/** json_encode **/
static voidp luka_c2_json_encode (Luka *luka, voidp *p, size_t n) {
	_StrBuf *sb = NULL;
	char *sbstr = NULL;
 
	if (n < 1 || (!luka_is_object(p[0]) && !luka_is_array(p[0])))
		return luka_null(luka);

	luka_c2_json_encode_ex(luka, p[0], &sb);

	if (sb == NULL)
		return luka_null(luka);

	sbstr = _sb_tostring(luka, sb);
	_sb_free(luka, &sb);
	return luka_put_string(luka, sbstr);
}

static voidp luka_c2_json_decode_ex (Luka *luka, cJSON *cJson) {
	voidp objP = NULL, arrayP = NULL;
	LukaObject *obj = NULL;
	LukaArray *array = NULL;

	if (cJson->type == cJSON_NULL) {
		return luka_null(luka);
	} else if (cJson->type == cJSON_True) {
		return luka_true(luka);
	} else if (cJson->type == cJSON_False) {
		return luka_false(luka);
	} else if (cJson->type == cJSON_Number) {
		return luka_put_double(luka, cJson->valuedouble);
	} else if (cJson->type == cJSON_String) {
		return luka_put_string(luka, luka_strdup(luka, cJson->valuestring));
	} else if (cJson->type == cJSON_Object) {
		cJSON *mov = NULL;

		objP = luka_put_object(luka);
		obj = luka_get_object(luka, objP);
		mov = cJson->child;
		while (mov) {
			voidp obufp = luka_c2_json_decode_ex(luka, cJSON_GetObjectItem(cJson, mov->string));
			luka_data_up(luka, obufp);
			luka_object_put(luka, obj, mov->string, obufp);
			mov = mov->next;
		}
		return objP;
	} else if (cJson->type == cJSON_Array) {
		int i = 0;

		arrayP = luka_put_array(luka);
		array = luka_get_array(luka, arrayP);
		for (i = 0; i < cJSON_GetArraySize(cJson); i++) {
			voidp abufp = luka_c2_json_decode_ex(luka, cJSON_GetArrayItem(cJson, i));
			luka_data_up(luka, abufp);
			luka_array_push(luka, array, abufp);
		}
		return arrayP;
	}

	return luka_null(luka);
}

/** json_decode **/
static voidp luka_c2_json_decode (Luka *luka, voidp *p, size_t n) {
	const char *jsonStr = NULL;
	cJSON *jsonObj = NULL;
	voidp result = luka_null(luka);
 
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

	jsonStr = luka_get_string(luka, p[0]);
	if (*jsonStr == 0)
		return luka_null(luka);

	jsonObj = cJSON_Parse(jsonStr);
	if (jsonObj == NULL)
		return luka_null(luka);

	if (jsonObj->type != cJSON_Array && jsonObj->type != cJSON_Object) {
		cJSON_Delete(jsonObj);
		return luka_null(luka);
	}

	result = luka_c2_json_decode_ex(luka, jsonObj);

	cJSON_Delete(jsonObj);
	return result;
}

/***********************************************************
  ����  
***********************************************************/

/** http_get **/
static voidp luka_c2_http_get (Luka *luka, voidp *p, size_t n) {
	const char *url = NULL;
	char *context = NULL;
	unsigned char *bData = NULL;
	size_t bSize = 0;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

	url = luka_get_string(luka, p[0]);
	if (*url == 0)
		return luka_null(luka);

	cSOCK_Init();

	context = cHTTP_Get(url);
	if (!context)
		return luka_null(luka);

	if (*context == 0) {
		bData = (unsigned char *)luka_malloc(luka, 1);
		bSize = 0;
	} else {
		bSize = strlen(context);
		bData = (unsigned char *)luka_malloc(luka, bSize);
		memcpy(bData, context, bSize);
		free(context);
	}
	return luka_put_byte(luka, bData, bSize);
}

/***********************************************************
  PackageC2(��װһЩ����) 
***********************************************************/

void luka_c2_regs (Luka *luka) {
    /** �ַ��� **/
    luka_reg(luka, "splite",         luka_c2_splite);
    luka_reg(luka, "substr",         luka_c2_substr);
    luka_reg(luka, "strrev",         luka_c2_strrev);
    luka_reg(luka, "strupr",         luka_c2_strupr);
    luka_reg(luka, "strlowr",        luka_c2_strlowr);
    luka_reg(luka, "basename",       luka_c2_basename);
    luka_reg(luka, "dirname",        luka_c2_dirname);
    luka_reg(luka, "csv_decode",     luka_c2_csv_decode);

    /** �ļ� **/
    luka_reg(luka, "readtxt",        luka_c2_readtxt);
    luka_reg(luka, "writetxt",       luka_c2_writetxt);
    luka_reg(luka, "readfile",       luka_c2_readfile);
    luka_reg(luka, "writefile",      luka_c2_writefile);

    /** ���� **/
    luka_reg(luka, "base64_encode",  luka_c2_base64_encode);
    luka_reg(luka, "base64_decode",  luka_c2_base64_decode);
    luka_reg(luka, "xor_encryp",     luka_c2_xor_encryp);
    luka_reg(luka, "md5",            luka_c2_md5);
    luka_reg(luka, "json_encode",    luka_c2_json_encode);
    luka_reg(luka, "json_decode",    luka_c2_json_decode);

    /** ���� **/
    luka_reg(luka, "http_get",       luka_c2_http_get);
}

void luka_c2_vars (Luka *luka) {
}
