/***********************************************************
  pkg_c.c 

  https://megurineluka.cn/ 
  hatsusakana@gmail.com 
***********************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <unistd.h>

#ifdef _WIN32
#include <windows.h>
#endif

#include "../luka.h"

/***********************************************************
  time.h 
***********************************************************/

/** clock **/
static voidp luka_c_clock (Luka *luka, voidp *p, size_t n) {
    return luka_put_int(luka, clock());
}

/** time **/
static voidp luka_c_time (Luka *luka, voidp *p, size_t n) {
    return luka_put_int(luka, time(NULL));
}

/** difftime **/
static voidp luka_c_difftime (Luka *luka, voidp *p, size_t n) {
    time_t t1 = 0, t2 = 0;

    if (!p || n < 2 || !luka_is_int(p[0]) || !luka_is_int(p[1]))
        return luka_null(luka);

    t1 = luka_get_int(luka, p[0]);
    t2 = luka_get_int(luka, p[1]);
    return luka_put_int(luka, difftime(t1, t2));
}

/** mktime **/
static voidp luka_c_mktime (Luka *luka, voidp *p, size_t n) {
    LukaObject *obj = NULL;
	struct tm tmd = {0};
    voidp tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_isdst;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

	obj = luka_get_object(luka, p[0]);
	if (!obj)
		return luka_null(luka);

    tm_year = luka_object_get(luka, obj, "tm_year");
    tm_mon = luka_object_get(luka, obj, "tm_mon");
    tm_mday = luka_object_get(luka, obj, "tm_mday");
    tm_hour = luka_object_get(luka, obj, "tm_hour");
    tm_min = luka_object_get(luka, obj, "tm_min");
    tm_sec = luka_object_get(luka, obj, "tm_sec");
    tm_isdst = luka_object_get(luka, obj, "tm_isdst");
    if (tm_year) tmd.tm_year = luka_get_int(luka, tm_year);
    if (tm_mon) tmd.tm_mon = luka_get_int(luka, tm_mon);
    if (tm_mday) tmd.tm_mday = luka_get_int(luka, tm_mday);
    if (tm_hour) tmd.tm_hour = luka_get_int(luka, tm_hour);
    if (tm_min) tmd.tm_min = luka_get_int(luka, tm_min);
    if (tm_sec) tmd.tm_sec = luka_get_int(luka, tm_sec);
    if (tm_isdst) tmd.tm_isdst = luka_get_int(luka, tm_isdst);
	return luka_put_int(luka, mktime(&tmd));
}

/** asctime **/
static voidp luka_c_asctime (Luka *luka, voidp *p, size_t n) {
    LukaObject *obj = NULL;
	struct tm tmd = {0};
    voidp tm_year, tm_mon, tm_mday, tm_hour, tm_min, tm_sec, tm_isdst;

	if (n < 1 || !luka_is_object(p[0]))
		return luka_null(luka);

	obj = luka_get_object(luka, p[0]);
	if (!obj)
		return luka_null(luka);

    tm_year = luka_object_get(luka, obj, "tm_year");
    tm_mon = luka_object_get(luka, obj, "tm_mon");
    tm_mday = luka_object_get(luka, obj, "tm_mday");
    tm_hour = luka_object_get(luka, obj, "tm_hour");
    tm_min = luka_object_get(luka, obj, "tm_min");
    tm_sec = luka_object_get(luka, obj, "tm_sec");
    tm_isdst = luka_object_get(luka, obj, "tm_isdst");
    if (tm_year) tmd.tm_year = luka_get_int(luka, tm_year);
    if (tm_mon) tmd.tm_mon = luka_get_int(luka, tm_mon);
    if (tm_mday) tmd.tm_mday = luka_get_int(luka, tm_mday);
    if (tm_hour) tmd.tm_hour = luka_get_int(luka, tm_hour);
    if (tm_min) tmd.tm_min = luka_get_int(luka, tm_min);
    if (tm_sec) tmd.tm_sec = luka_get_int(luka, tm_sec);
    if (tm_isdst) tmd.tm_isdst = luka_get_int(luka, tm_isdst);
	return luka_put_string(luka, luka_strdup(luka, asctime(&tmd)));
}

/** ctime **/
static voidp luka_c_ctime (Luka *luka, voidp *p, size_t n) {
	time_t t = 0;

	if (n < 1 || !luka_is_int(p[0]))
		return luka_null(luka);

	t = luka_get_int(luka, p[0]);
	return luka_put_string(luka, luka_strdup(luka, ctime(&t)));
}

/** gmtime **/
static voidp luka_c_gmtime (Luka *luka, voidp *p, size_t n) {
	time_t t = 0;
	struct tm *tmd = NULL;
	voidp obj = NULL;

	if (n < 1 || !luka_is_int(p[0]))
		return luka_null(luka);

	t = (time_t)luka_get_int(luka, p[0]);
	tmd = gmtime(&t);
	obj = luka_put_object(luka);
	luka_object_put(luka, luka_get_object(luka, obj), "tm_year", luka_put_int(luka, tmd->tm_year));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_mon", luka_put_int(luka, tmd->tm_mon));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_mday", luka_put_int(luka, tmd->tm_mday));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_hour", luka_put_int(luka, tmd->tm_hour));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_min", luka_put_int(luka, tmd->tm_min));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_sec", luka_put_int(luka, tmd->tm_sec));
	luka_object_put(luka, luka_get_object(luka, obj), "tm_isdst", luka_put_int(luka, tmd->tm_isdst));
	return obj;
}

/** strftime **/
static voidp luka_c_strftime (Luka *luka, voidp *p, size_t n) {
	char buf[128] = {0};
	time_t t = time(NULL);
	const char *fmt = "%Y-%m-%d %H:%M:%S";
    struct tm *tmd = NULL;

	if (n >= 1 && luka_is_string( p[0])) {
		fmt = luka_get_string(luka, p[0]);
	}

	if (n >= 2 && luka_is_int(p[1])) {
		t = (time_t)luka_get_int(luka, p[1]);
	}

    tmd = localtime(&t);
    strftime(buf, sizeof(buf) - 1, fmt, tmd);
	return luka_put_string(luka, luka_strdup(luka, buf));
}

#ifdef _WIN32

#define TM_YEAR_BASE 1900
#define ALT_E     0x01
#define ALT_O     0x02
#define LEGAL_ALT(x)    { if (alt_format & ~(x)) return (0); }

static const char *day[7] = {
  "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
  "Friday", "Saturday"
};

static const char *abday[7] = {
  "Sun","Mon","Tue","Wed","Thu","Fri","Sat"
};

static const char *mon[12] = {
  "January", "February", "March", "April", "May", "June", "July",
  "August", "September", "October", "November", "December"
};

static const char *abmon[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

static const char *am_pm[2] = {
  "AM", "PM"
};

static int conv_num(const char **buf, int *dest, int llim, int ulim) {
  int result = 0;
 
  /* The limit also determines the number of valid digits. */
  int rulim = ulim;
 
  if (**buf < '0' || **buf > '9')
    return (0);
 
  do {
    result *= 10;
    result += *(*buf)++ - '0';
    rulim /= 10;
  } while ((result * 10 <= ulim) && rulim && **buf >= '0' && **buf <= '9');
 
  if (result < llim || result > ulim)
    return (0);
 
  *dest = result;
  return (1);
}

static char *strptime(const char *buf, const char *fmt, struct tm *tm) {
  char c;
  const char *bp;
  size_t len = 0;
  int alt_format, i, split_year = 0;
 
  bp = buf;
 
  while ((c = *fmt) != '\0') {
    /* Clear `alternate' modifier prior to new conversion. */
    alt_format = 0;
 
    /* Eat up white-space. */
    if (c <= 32) {
      while (*bp <= 32)
        bp++;
 
      fmt++;
      continue;
    }
        
    if ((c = *fmt++) != '%')
      goto literal;
 
 
again:    switch (c = *fmt++) {
    case '%': /* "%%" is converted to "%". */
literal:
      if (c != *bp++)
        return (0);
      break;
 
    /*
     * "Alternative" modifiers. Just set the appropriate flag
     * and start over again.
     */
    case 'E': /* "%E?" alternative conversion modifier. */
      LEGAL_ALT(0);
      alt_format |= ALT_E;
      goto again;
 
    case 'O': /* "%O?" alternative conversion modifier. */
      LEGAL_ALT(0);
      alt_format |= ALT_O;
      goto again;
      
    /*
     * "Complex" conversion rules, implemented through recursion.
     */
    case 'c': /* Date and time, using the locale's format. */
      LEGAL_ALT(ALT_E);
      if (!(bp = strptime(bp, "%x %X", tm)))
        return (0);
      break;
 
    case 'D': /* The date as "%m/%d/%y". */
      LEGAL_ALT(0);
      if (!(bp = strptime(bp, "%m/%d/%y", tm)))
        return (0);
      break;
 
    case 'R': /* The time as "%H:%M". */
      LEGAL_ALT(0);
      if (!(bp = strptime(bp, "%H:%M", tm)))
        return (0);
      break;
 
    case 'r': /* The time in 12-hour clock representation. */
      LEGAL_ALT(0);
      if (!(bp = strptime(bp, "%I:%M:%S %p", tm)))
        return (0);
      break;
 
    case 'T': /* The time as "%H:%M:%S". */
      LEGAL_ALT(0);
      if (!(bp = strptime(bp, "%H:%M:%S", tm)))
        return (0);
      break;
 
    case 'X': /* The time, using the locale's format. */
      LEGAL_ALT(ALT_E);
      if (!(bp = strptime(bp, "%H:%M:%S", tm)))
        return (0);
      break;
 
    case 'x': /* The date, using the locale's format. */
      LEGAL_ALT(ALT_E);
      if (!(bp = strptime(bp, "%m/%d/%y", tm)))
        return (0);
      break;
 
    /*
     * "Elementary" conversion rules.
     */
    case 'A': /* The day of week, using the locale's form. */
    case 'a':
      LEGAL_ALT(0);
      for (i = 0; i < 7; i++) {
        /* Full name. */
        len = strlen(day[i]);
        if (strncmp(day[i], bp, len) == 0)
          break;
 
        /* Abbreviated name. */
        len = strlen(abday[i]);
        if (strncmp(abday[i], bp, len) == 0)
          break;
      }
 
      /* Nothing matched. */
      if (i == 7)
        return (0);
 
      tm->tm_wday = i;
      bp += len;
      break;
 
    case 'B': /* The month, using the locale's form. */
    case 'b':
    case 'h':
      LEGAL_ALT(0);
      for (i = 0; i < 12; i++) {
        /* Full name. */
        len = strlen(mon[i]);
        if (strncmp(mon[i], bp, len) == 0)
          break;
 
        /* Abbreviated name. */
        len = strlen(abmon[i]);
        if (strncmp(abmon[i], bp, len) == 0)
          break;
      }
 
      /* Nothing matched. */
      if (i == 12)
        return (0);
 
      tm->tm_mon = i;
      bp += len;
      break;
 
    case 'C': /* The century number. */
      LEGAL_ALT(ALT_E);
      if (!(conv_num(&bp, &i, 0, 99)))
        return (0);
 
      if (split_year) {
        tm->tm_year = (tm->tm_year % 100) + (i * 100);
      } else {
        tm->tm_year = i * 100;
        split_year = 1;
      }
      break;
 
    case 'd': /* The day of month. */
    case 'e':
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_mday, 1, 31)))
        return (0);
      break;
 
    case 'k': /* The hour (24-hour clock representation). */
      LEGAL_ALT(0);
      /* FALLTHROUGH */
    case 'H':
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_hour, 0, 23)))
        return (0);
      break;
 
    case 'l': /* The hour (12-hour clock representation). */
      LEGAL_ALT(0);
      /* FALLTHROUGH */
    case 'I':
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_hour, 1, 12)))
        return (0);
      if (tm->tm_hour == 12)
        tm->tm_hour = 0;
      break;
 
    case 'j': /* The day of year. */
      LEGAL_ALT(0);
      if (!(conv_num(&bp, &i, 1, 366)))
        return (0);
      tm->tm_yday = i - 1;
      break;
 
    case 'M': /* The minute. */
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_min, 0, 59)))
        return (0);
      break;
 
    case 'm': /* The month. */
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &i, 1, 12)))
        return (0);
      tm->tm_mon = i - 1;
      break;
 
    case 'p': /* The locale's equivalent of AM/PM. */
      LEGAL_ALT(0);
      /* AM? */
      if (strcmp(am_pm[0], bp) == 0) {
        if (tm->tm_hour > 11)
          return (0);
 
        bp += strlen(am_pm[0]);
        break;
      }
      /* PM? */
      else if (strcmp(am_pm[1], bp) == 0) {
        if (tm->tm_hour > 11)
          return (0);
 
        tm->tm_hour += 12;
        bp += strlen(am_pm[1]);
        break;
      }
 
      /* Nothing matched. */
      return (0);
 
    case 'S': /* The seconds. */
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_sec, 0, 61)))
        return (0);
      break;
 
    case 'U': /* The week of year, beginning on sunday. */
    case 'W': /* The week of year, beginning on monday. */
      LEGAL_ALT(ALT_O);
      /*
       * XXX This is bogus, as we can not assume any valid
       * information present in the tm structure at this
       * point to calculate a real value, so just check the
       * range for now.
       */
       if (!(conv_num(&bp, &i, 0, 53)))
        return (0);
       break;
 
    case 'w': /* The day of week, beginning on sunday. */
      LEGAL_ALT(ALT_O);
      if (!(conv_num(&bp, &tm->tm_wday, 0, 6)))
        return (0);
      break;
 
    case 'Y': /* The year. */
      LEGAL_ALT(ALT_E);
      if (!(conv_num(&bp, &i, 0, 9999)))
        return (0);
 
      tm->tm_year = i - TM_YEAR_BASE;
      break;
 
    case 'y': /* The year within 100 years of the epoch. */
      LEGAL_ALT(ALT_E | ALT_O);
      if (!(conv_num(&bp, &i, 0, 99)))
        return (0);
 
      if (split_year) {
        tm->tm_year = ((tm->tm_year / 100) * 100) + i;
        break;
      }
      split_year = 1;
      if (i <= 68)
        tm->tm_year = i + 2000 - TM_YEAR_BASE;
      else
        tm->tm_year = i + 1900 - TM_YEAR_BASE;
      break;
 
    /*
     * Miscellaneous conversions.
     */
    case 'n': /* Any kind of white-space. */
    case 't':
      LEGAL_ALT(0);
      while (*bp <= 32)
        bp++;
      break;
 
 
    default:  /* Unknown/unsupported conversion. */
      return (0);
    }
 
 
  }
 
  /* LINTED functional specification */
  return ((char *)bp);
}

#endif

/** strtotime **/
static voidp luka_c_strtotime (Luka *luka, voidp *p, size_t n) {
	const char *buf = NULL;
	time_t t = 0;
	const char *fmt = "%Y-%m-%d %H:%M:%S";
    struct tm tmd;

	if (n == 0 || !luka_is_string(p[0]))
		return luka_null(luka);

	if (n > 1 && luka_is_string(p[1])) {
		fmt = luka_get_string(luka, p[1]);
	}

	buf = luka_get_string(luka, p[0]);
    strptime(buf, fmt, &tmd);
	return luka_put_int(luka, mktime(&tmd));
}

/***********************************************************
  stdio.h 
***********************************************************/

/** fopen **/
static voidp luka_c_fopen (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *filename = NULL, *mode = NULL;

	if (n < 2 || !luka_is_string(p[0]) || !luka_is_string(p[1]))
		return luka_null(luka);

    filename = luka_get_string(luka, p[0]);
    mode = luka_get_string(luka, p[1]);
    fp = fopen(filename, mode);
    if (!fp)
		return luka_null(luka);

    return luka_put_voidp(luka, fp);
}

/** fclose **/
static voidp luka_c_fclose (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    fclose((FILE *)luka_get_voidp(luka, p[0]));
    return luka_null(luka);
}

/** ferror **/
static voidp luka_c_ferror (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, ferror((FILE *)luka_get_voidp(luka, p[0])));
}

/** clearerr **/
static voidp luka_c_clearerr (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    clearerr((FILE *)luka_get_voidp(luka, p[0]));
    return luka_null(luka);
}

/** feof **/
static voidp luka_c_feof (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, feof((FILE *)luka_get_voidp(luka, p[0])));
}

/** fflush **/
static voidp luka_c_fflush (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, fflush((FILE *)luka_get_voidp(luka, p[0])));
}

/** fseek **/
static voidp luka_c_fseek (Luka *luka, voidp *p, size_t n) {
	if (n < 3 || !luka_is_voidp(p[0]) || !luka_is_int(p[1]) || !luka_is_int(p[2]))
		return luka_null(luka);

    return luka_put_int(luka, fseek(
        (FILE *)luka_get_voidp(luka, p[0]),
        luka_get_int(luka, p[1]),
        luka_get_int(luka, p[2])
    ));
}

/** ftell **/
static voidp luka_c_ftell (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, ftell((FILE *)luka_get_voidp(luka, p[0])));
}

/** fwrite **/
static voidp luka_c_fwrite (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    size_t ret = 0;

	if (n < 2 || (!luka_is_string(p[0]) && !luka_is_byte(p[0])) || !luka_is_voidp(p[1]))
		return luka_null(luka);

    fp = (FILE *)luka_get_voidp(luka, p[1]);
    if (luka_is_string(p[0])) {
        const char *str = luka_get_string(luka, p[0]);
        ret = fwrite(str, 1, strlen(str), fp);
    } else {
        size_t n = 0;
        unsigned char *data = luka_get_byte(luka, p[0], &n);
        ret = fwrite(data, 1, n, fp);
    }
    return luka_put_int(luka, ret);
}

/** fread **/
static voidp luka_c_fread (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    long size = 0;
    unsigned char *data = NULL;

	if (n < 2 || !luka_is_int(p[0]) || !luka_is_voidp(p[1]))
		return luka_null(luka);

    size = luka_get_int(luka, p[0]);
    fp = (FILE *)luka_get_voidp(luka, p[1]);
    if (size <= 0)
		return luka_null(luka);

    data = (unsigned char *)luka_malloc(luka, size);
    size = fread(data, 1, size, fp);
    if (size > 0)
        return luka_put_byte(luka, data, size);
    else {
        luka_free(luka, data);
        return luka_null(luka);
    }
}

/** fputc **/
static voidp luka_c_fputc (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    int c = 0;

	if (n < 2 || !luka_is_int(p[0]) || !luka_is_voidp(p[1]))
		return luka_null(luka);

    c = (int)luka_get_int(luka, p[0]);
    fp = (FILE *)luka_get_voidp(luka, p[1]);
    return luka_put_int(luka, fputc(c, fp));
}

/** fgetc **/
static voidp luka_c_fgetc (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, fgetc((FILE *)luka_get_voidp(luka, p[0])));
}

/** fputs **/
static voidp luka_c_fputs (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    const char *str = NULL;

	if (n < 2 || !luka_is_string(p[0]) || !luka_is_voidp(p[1]))
		return luka_null(luka);

    str = luka_get_string(luka, p[0]);
    fp = (FILE *)luka_get_voidp(luka, p[1]);
    return luka_put_int(luka, fputs(str, fp));
}

/** fgets **/
static voidp luka_c_fgets (Luka *luka, voidp *p, size_t n) {
    FILE *fp = NULL;
    long size = 0;
    char *s = NULL;

	if (n < 2 || !luka_is_int(p[0]) || !luka_is_voidp(p[1]))
		return luka_null(luka);

    size = (long)luka_get_int(luka, p[0]);
    fp = (FILE *)luka_get_voidp(luka, p[1]);
    s = (char *)luka_malloc(luka, size + 2);
    if (fgets(s, size, fp) == NULL) {
        luka_free(luka, s);
        return luka_null(luka);
    }
    return luka_put_string(luka, s);
}

/** remove **/
static voidp luka_c_remove (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, remove(luka_get_string(luka, p[0])));
}

/** tmpfile **/
static voidp luka_c_tmpfile (Luka *luka, voidp *p, size_t n) {
    FILE *fp = tmpfile();
    if (!fp)
        return luka_null(luka);

    return luka_put_voidp(luka, tmpfile());
}

/** getchar **/
static voidp luka_c_getchar (Luka *luka, voidp *p, size_t n) {
    return luka_put_int(luka, getchar());
}

/** putchar **/
static voidp luka_c_putchar (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_int(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, putchar(luka_get_int(luka, p[0])));
}

/** gets **/
static voidp luka_c_gets (Luka *luka, voidp *p, size_t n) {
    char buf[256] = {0};
    scanf("%255s", buf);
    return luka_put_string(luka, luka_strdup(luka, buf));
}

/** puts **/
static voidp luka_c_puts (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    puts(luka_get_string(luka, p[0]));
    return luka_null(luka);
}

/** perror **/
static voidp luka_c_perror (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    perror(luka_get_string(luka, p[0]));
    return luka_null(luka);
}

/** popen **/
static voidp luka_c_popen (Luka *luka, voidp *p, size_t n) {
	FILE *fp = NULL;

	if (n < 2 || !luka_is_string(p[0]) || !luka_is_string(p[1]))
		return luka_null(luka);

	fp = popen(luka_get_string(luka, p[0]), luka_get_string(luka, p[1]));
	if (!fp)
		return luka_null(luka);

	return luka_put_voidp(luka, fp);
}

/** pclose **/
static voidp luka_c_pclose (Luka *luka, voidp *p, size_t n) {
	FILE *fp = NULL;

	if (n < 1 || !luka_is_voidp(p[0]))
		return luka_null(luka);

	fp = (FILE *)luka_get_voidp(luka, p[0]);
	if (fp) pclose(fp);
	return luka_true(luka);
}

/***********************************************************
  stdlib.h 
***********************************************************/

/** atof **/
static voidp luka_c_atof (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    return luka_put_double(luka, atof(luka_get_string(luka, p[0])));
}

/** atoi **/
static voidp luka_c_atoi (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, atol(luka_get_string(luka, p[0])));
}

/** exit **/
static voidp luka_c_exit (Luka *luka, voidp *p, size_t n) {
    int status = 0;
    if (n != 0 && luka_is_int(p[0])) {
        status = luka_get_int(luka, p[0]);
    }
    exit(status);
    return luka_null(luka);
}

/** getenv **/
static voidp luka_c_getenv (Luka *luka, voidp *p, size_t n) {
    const char *buf = NULL;

	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    buf = luka_get_string(luka, p[0]);
    if (*buf == 0)
		return luka_null(luka);

    buf = getenv(buf);
    if (!buf)
		return luka_null(luka);

    return luka_put_string(luka, luka_strdup(luka, buf));
}

/** system **/
static voidp luka_c_system (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    system(luka_get_string(luka, p[0]));
    return luka_null(luka);
}

/** abs **/
static voidp luka_c_abs (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || (!luka_is_int(p[0]) && !luka_is_double(p[0])))
		return luka_null(luka);

    return luka_put_int(luka, labs(luka_get_int(luka, p[0])));
}

/** rand **/
static voidp luka_c_rand (Luka *luka, voidp *p, size_t n) {
    return luka_put_int(luka, rand());
}

/** srand **/
static voidp luka_c_srand (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_int(p[0]))
		return luka_null(luka);

    srand((size_t)luka_get_int(luka, p[0]));
    return luka_true(luka);
}

/** sleep **/
static voidp luka_c_sleep (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_int(p[0]) || luka_get_int(luka, p[0]) * 1000 < 0)
		return luka_null(luka);

#ifdef _WIN32
    Sleep(luka_get_int(luka, p[0]) * 1000);
#else
    sleep(luka_get_int(luka, p[0]));
#endif
    return luka_true(luka);
}

/***********************************************************
  string.h 
***********************************************************/

/** strstr **/
static voidp luka_c_strstr (Luka *luka, voidp *p, size_t n) {
    const char *s1 = NULL, *s2 = NULL;
    char *point = NULL;

	if (n < 1 || !luka_is_string(p[0]) || !luka_is_string(p[1]))
		return luka_null(luka);

    s1 = luka_get_string(luka, p[0]);
    s2 = luka_get_string(luka, p[1]);

    if (*s1 == 0 || *s2 == 0)
		return luka_null(luka);

    point = strstr(s1, s2);
    return point == NULL ? luka_null(luka) : luka_put_int(luka, point - s1);
}

/** strlen **/
static voidp luka_c_strlen (Luka *luka, voidp *p, size_t n) {
	if (n < 1 || !luka_is_string(p[0]))
		return luka_null(luka);

    return luka_put_int(luka, strlen(luka_get_string(luka, p[0])));
}

/***********************************************************
  PackageC(C���Ժ���) 
***********************************************************/

void luka_c_regs (Luka *luka) {
    /** time.h **/
    luka_reg(luka, "clock",     luka_c_clock);
    luka_reg(luka, "time",      luka_c_time);
    luka_reg(luka, "difftime",  luka_c_difftime);
    luka_reg(luka, "mktime",    luka_c_mktime);
    luka_reg(luka, "asctime",   luka_c_asctime);
    luka_reg(luka, "ctime",     luka_c_ctime);
    luka_reg(luka, "gmtime",    luka_c_gmtime);
    luka_reg(luka, "strftime",  luka_c_strftime);
    luka_reg(luka, "strtotime", luka_c_strtotime);

    /** stdio.h **/
    luka_reg(luka, "fopen",     luka_c_fopen);
    luka_reg(luka, "fclose",    luka_c_fclose);
    luka_reg(luka, "ferror",    luka_c_ferror);
    luka_reg(luka, "clearerr",  luka_c_clearerr);
    luka_reg(luka, "feof",      luka_c_feof);
    luka_reg(luka, "fflush",    luka_c_fflush);
    luka_reg(luka, "fseek",     luka_c_fseek);
    luka_reg(luka, "ftell",     luka_c_ftell);
    luka_reg(luka, "fwrite",    luka_c_fwrite);
    luka_reg(luka, "fread",     luka_c_fread);
    luka_reg(luka, "fputc",     luka_c_fputc);
    luka_reg(luka, "fgetc",     luka_c_fgetc);
    luka_reg(luka, "fputs",     luka_c_fputs);
    luka_reg(luka, "fgets",     luka_c_fgets);
    luka_reg(luka, "remove",    luka_c_remove);
    luka_reg(luka, "tmpfile",   luka_c_tmpfile);
    luka_reg(luka, "getchar",   luka_c_getchar);
    luka_reg(luka, "putchar",   luka_c_putchar);
    luka_reg(luka, "gets",      luka_c_gets);
    luka_reg(luka, "puts",      luka_c_puts);
    luka_reg(luka, "perror",    luka_c_perror);
    luka_reg(luka, "popen",     luka_c_popen);
    luka_reg(luka, "pclose",    luka_c_pclose);

    /** stdlib.h **/
    luka_reg(luka, "atof",      luka_c_atof);
    luka_reg(luka, "atoi",      luka_c_atoi);
    luka_reg(luka, "exit",      luka_c_exit);
    luka_reg(luka, "getenv",    luka_c_getenv);
    luka_reg(luka, "system",    luka_c_system);
    luka_reg(luka, "abs",       luka_c_abs);
    luka_reg(luka, "rand",      luka_c_rand);
    luka_reg(luka, "srand",     luka_c_srand);
    luka_reg(luka, "sleep",     luka_c_sleep);

    /** string.h **/
    luka_reg(luka, "strstr",    luka_c_strstr);
    luka_reg(luka, "strlen",    luka_c_strlen);
}

void luka_c_vars (Luka *luka) {
    luka_reg_var(luka, "SEEK_SET", luka_put_int(luka, 0));
    luka_reg_var(luka, "SEEK_CUR", luka_put_int(luka, 1));
    luka_reg_var(luka, "SEEK_END", luka_put_int(luka, 2));
}

